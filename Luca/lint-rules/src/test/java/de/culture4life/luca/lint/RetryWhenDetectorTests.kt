package de.culture4life.luca.lint

import com.android.tools.lint.checks.infrastructure.TestFiles.kotlin
import com.android.tools.lint.checks.infrastructure.TestLintTask.lint
import org.junit.Test

class RetryWhenDetectorTests {

    private val observableStub = kotlin(
        """
            class Observable {
                
                fun retryWhen(block: (Observable) -> Observable) {
                    // Stub
                }
                
                companion object {
                    fun just(input: String): Observable {
                        return Observable()
                    }
                }
            }
        """
    ).indented()

    @Test
    fun testRetryWhenDetector() {
        lint().files(
            observableStub,
            kotlin(
                """
                    fun test() {
                        Observable
                            .just("Test")
                            .retryWhen { it }
                    }
                """
            ).indented()
        )
            .allowMissingSdk()
            .issues(RetryWhenDetector.ISSUE)
            .run()
            .expect(
                """
                    src/test.kt:2: Warning: Direct usage of retryWhen [RetryWhen]
    Observable
    ^
0 errors, 1 warnings
                """
            )
    }

}