package de.culture4life.luca.ui.payment.children

import androidx.lifecycle.SavedStateHandle
import de.culture4life.luca.LucaUnitTest
import de.culture4life.luca.R
import de.culture4life.luca.network.pojo.payment.OpenPaymentRequestResponseData
import de.culture4life.luca.network.pojo.payment.PaymentLimitsResponseData
import de.culture4life.luca.payment.PaymentAmounts
import de.culture4life.luca.payment.PaymentManager
import de.culture4life.luca.testtools.rxjava.SubscriptionRecorder
import de.culture4life.luca.testtools.samples.LocationSamples
import de.culture4life.luca.testtools.samples.PaymentSamples
import de.culture4life.luca.ui.payment.PaymentFlowViewModel
import de.culture4life.luca.ui.payment.children.PaymentAmountViewModel.Companion.PAYMENT_REQUEST_POLLING_INTERVAL
import de.culture4life.luca.util.TextUtil
import de.culture4life.luca.util.toCurrencyAmountBigDecimal
import de.culture4life.luca.util.toCurrencyAmountNumber
import de.culture4life.luca.util.toCurrencyAmountString
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.kotlin.*
import java.util.concurrent.TimeUnit

class PaymentAmountViewModelTest : LucaUnitTest() {

    private lateinit var viewModel: PaymentAmountViewModel
    private lateinit var sharedViewModel: PaymentFlowViewModel
    private lateinit var paymentManager: PaymentManager
    private val paymentLimits = PaymentLimitsResponseData(
        minimumInvoiceAmount = 500.toCurrencyAmountBigDecimal(),
        maximumInvoiceAmount = 200000.toCurrencyAmountBigDecimal(),
        maximumTipAmount = 40000.toCurrencyAmountBigDecimal(),
    )

    @Before
    fun before() {
        val applicationSpy = spy(application)

        paymentManager = spy(this.application.paymentManager)
        doReturn(paymentManager).`when`(applicationSpy).paymentManager

        sharedViewModel = spy(PaymentFlowViewModel(applicationSpy, SavedStateHandle()))
        viewModel = spy(PaymentAmountViewModel(applicationSpy))
        viewModel.setupSharedViewModelReference(sharedViewModel)
    }

    @Test
    fun keepDataUpdated_noPaymentRequestAvailable_updatesAmountWithZero() {
        // Given
        givenNoPaymentRequest()
        val subscriptionRecorder = SubscriptionRecorder { Completable.complete() }
        Mockito.doAnswer(subscriptionRecorder).whenever(viewModel).updateAmount(0)

        // When
        viewModel.initialize().test()
        viewModel.keepDataUpdated().test()
        triggerScheduler()

        // Then
        subscriptionRecorder.verifySubscriptions(1)
    }

    @Test
    fun onResume_paymentRequestAvailable_updatesAmountWithInvoice() {
        // Given
        val location = LocationSamples.RestaurantWithTable()
        val payment = PaymentSamples.LocationPayment(location)
        givenPaymentRequest(payment.openPaymentRequestResponseData)
        val subscriptionRecorder = SubscriptionRecorder { Completable.complete() }
        Mockito.doAnswer(subscriptionRecorder).whenever(viewModel).updateAmount(payment.invoiceAmount)
        doAnswer { Single.just(location.locationData) }.whenever(sharedViewModel).getPaymentLocationData()

        // When
        viewModel.initialize().test()
        viewModel.onResume()
        viewModel.keepDataUpdated().test()
        triggerScheduler()

        // Then
        subscriptionRecorder.verifySubscriptions(1)
    }

    @Test
    fun onResume_paymentRequestExpired_triggersCallback() {
        // Given
        val location = LocationSamples.RestaurantWithTable()
        val payment = PaymentSamples.LocationPayment(location)
        givenPaymentRequest(payment.openPaymentRequestResponseData)
        doAnswer { Single.just(location.locationData) }.whenever(sharedViewModel).getPaymentLocationData()

        // Provide a payment request initially
        viewModel.initialize().test()
        viewModel.onResume()
        viewModel.keepDataUpdated().test()
        triggerScheduler()

        // Remove payment request, simulating an expired request
        givenNoPaymentRequest()

        // When
        advanceScheduler(PAYMENT_REQUEST_POLLING_INTERVAL, TimeUnit.MILLISECONDS)

        // Then
        verify(sharedViewModel).onPaymentRequestExpired()
    }

    @Test
    fun onResume_paymentRequestAvailable_updatesPaymentRequestData() {
        // Given
        val location = LocationSamples.RestaurantWithTable()
        val paymentRequestResponse = PaymentSamples.LocationPayment(location).openPaymentRequestResponseData
        givenPaymentRequest(paymentRequestResponse)
        val subscriptionRecorder = SubscriptionRecorder { Completable.complete() }
        Mockito.doAnswer(subscriptionRecorder).whenever(viewModel).updatePaymentRequestData(paymentRequestResponse)
        doAnswer { Single.just(location.locationData) }.whenever(sharedViewModel).getPaymentLocationData()

        // When
        viewModel.initialize().test()
        viewModel.onResume()
        viewModel.keepDataUpdated().test()
        triggerScheduler()

        // Then
        subscriptionRecorder.verifySubscriptions(1)
    }

    @Test
    fun updatePaymentRequestData_paymentRequestAvailable_updatesPaymentRequestId() {
        // Given
        val location = LocationSamples.RestaurantWithTable()
        val payment = PaymentSamples.LocationPayment(location)
        val paymentRequestResponse = payment.openPaymentRequestResponseData
        givenPaymentRequest(paymentRequestResponse)

        // When
        viewModel.updatePaymentRequestData(paymentRequestResponse).test()

        // Then
        assertEquals(payment.id, sharedViewModel.paymentRequestId.value!!)
    }

    @Test
    fun updateAmount_amountIsZero_consumerInitiatedPayment() {
        // Given
        val amount = 0

        // When
        viewModel.updateAmount(amount).test()

        // Then
        assertFalse(viewModel.isFixedAmount.value!!)
        assertTrue(viewModel.showKeyboard.value!!)
        assertEquals(PaymentAmounts(amount), viewModel.paymentAmounts.value!!)
    }

    @Test
    fun updateAmount_amountIsNotZero_operatorInitiatedPayment() {
        // Given
        val amount = 1234

        // When
        viewModel.updateAmount(amount).test()

        // Then
        assertTrue(viewModel.isFixedAmount.value!!)
        assertFalse(viewModel.showKeyboard.value!!)
        assertEquals(PaymentAmounts(invoiceAmount = amount, maximumDiscountAmount = 0), viewModel.paymentAmounts.value!!)
    }

    @Test
    fun getAmountErrorIfAvailable_amountBelowMinimum_returnsMinimumError() {
        // Given
        givenPaymentLimits(paymentLimits)
        val minimumAmount = paymentLimits.minimumInvoiceAmount.toCurrencyAmountNumber()
        val expectedError = TextUtil.getPlaceholderString(
            application,
            R.string.pay_min_amount,
            "minAmount" to minimumAmount.toCurrencyAmountString()
        )

        // When
        val actualError = viewModel.getAmountErrorIfAvailable(minimumAmount - 1)

        // Then
        assertEquals(expectedError, actualError)
    }

    @Test
    fun getAmountErrorIfAvailable_amountAboveMaximum_returnsMaximumError() {
        // Given
        givenPaymentLimits(paymentLimits)
        val maximumAmount = paymentLimits.maximumInvoiceAmount.toCurrencyAmountNumber()
        val expectedError = TextUtil.getPlaceholderString(
            application,
            R.string.pay_max_amount,
            "maxAmount" to maximumAmount.toCurrencyAmountString()
        )

        // When
        val actualError = viewModel.getAmountErrorIfAvailable(maximumAmount + 1)

        // Then
        assertEquals(expectedError, actualError)
    }

    @Test
    fun getAmountErrorIfAvailable_validAmount_returnsNull() {
        // Given
        givenPaymentLimits(paymentLimits)
        val minimumAmount = paymentLimits.minimumInvoiceAmount.toCurrencyAmountNumber()
        val maximumAmount = paymentLimits.maximumInvoiceAmount.toCurrencyAmountNumber()

        // When
        val minimumError = viewModel.getAmountErrorIfAvailable(minimumAmount)
        val maximumError = viewModel.getAmountErrorIfAvailable(maximumAmount)

        // Then
        assertEquals(null, minimumError)
        assertEquals(null, maximumError)
    }

    private fun givenNoPaymentRequest() {
        doReturn(Maybe.empty<OpenPaymentRequestResponseData>()).`when`(paymentManager).fetchOpenPaymentRequest(any(), any())
    }

    private fun givenPaymentRequest(paymentRequest: OpenPaymentRequestResponseData) {
        doReturn(Maybe.just(paymentRequest)).`when`(paymentManager).fetchOpenPaymentRequest(any(), any())
    }

    private fun givenPaymentLimits(paymentLimits: PaymentLimitsResponseData) {
        doReturn(Single.just(paymentLimits)).`when`(paymentManager).fetchPaymentLimits()
        viewModel.updatePaymentLimits().blockingAwait()
    }
}
