package de.culture4life.luca.network.pojo

import de.culture4life.luca.LucaUnitTest
import de.culture4life.luca.crypto.KeyIssuerData
import de.culture4life.luca.testtools.samples.DailyKeySamples
import org.junit.Assert.assertEquals
import org.junit.Test

class KeyIssuerResponseDataTest : LucaUnitTest() {

    private val keyIssuerResponseData = DailyKeySamples.WithoutExpiration().keyIssuerResponseData

    private val encryptionKeyData = KeyIssuerData(
        id = "d229e28b-f881-4945-b0d8-09a413b04e00",
        name = "Gesundheitsamt Dev",
        type = "publicHDEKP",
        creationTimestamp = 1637228952000,
        encodedPublicKey = "BAWJrjvso+I1mrOHrlaGjxFDTY+boydG2l7DdfKxqbBAzxQK2QV9sdABsAth3EUe2kiTQxV09aZzlwLZcnhkSaM=",
        issuerId = "6766ea7e43226228d5a8ecb095b6e43f265a826d"
    )

    private val signingKeyData = KeyIssuerData(
        id = "d229e28b-f881-4945-b0d8-09a413b04e00",
        name = "Gesundheitsamt Dev",
        type = "publicHDSKP",
        creationTimestamp = 1637228951000,
        encodedPublicKey = "BMWmMRLSidXLrUwFQr9VwYB+3zrAmnUxOLTX9y9lNGmGO31SrBXAI9wNfIa75bq9V5TIkUUwmq8MN4oGD0oyGnI=",
        issuerId = "6766ea7e43226228d5a8ecb095b6e43f265a826d"
    )

    @Test
    fun parseCertificate_validResponse_parsesCertificate() {
        val parsedData = keyIssuerResponseData.certificate
        assertEquals("CN=luca Dev Cluster Intermediate CA, O=luca Dev, L=Berlin, ST=Berlin, C=DE", parsedData.issuerDN.name)
    }

    @Test
    fun parseEncryptionKeyJwt_validResponse_parsesKey() {
        val parsedData = keyIssuerResponseData.encryptionKeyData
        assertEquals(encryptionKeyData, parsedData)
        assertEquals(keyIssuerResponseData.encryptionKeyJwt, parsedData.signedJwt)
    }

    @Test
    fun parseSigningKeyJwt_validResponse_parsesKey() {
        val parsedData = keyIssuerResponseData.signingKeyData
        assertEquals(signingKeyData, parsedData)
        assertEquals(keyIssuerResponseData.signingKeyJwt, parsedData.signedJwt)
    }
}
