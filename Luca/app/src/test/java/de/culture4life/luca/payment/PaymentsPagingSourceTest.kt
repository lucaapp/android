package de.culture4life.luca.payment

import androidx.paging.PagingSource
import de.culture4life.luca.LucaUnitTest
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class PaymentsPagingSourceTest : LucaUnitTest() {

    @Mock
    private lateinit var paymentManager: PaymentManager

    @Before
    fun before() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun `Loading first page returns correct page`() {
        // Given
        val payments = listOf<PaymentData>(mock(), mock(), mock())
        whenever(paymentManager.fetchPaymentHistory(null)).thenReturn(Single.just(PagedPaymentHistory(CURSOR_PAGE_1, payments)))
        val pagingSource = PaymentsPagingSource(paymentManager)
        val expectedPage = PagingSource.LoadResult.Page(
            data = payments,
            prevKey = null,
            nextKey = CURSOR_PAGE_1
        )

        // When
        val observer = pagingSource.loadSingle(PagingSource.LoadParams.Refresh(key = null, loadSize = 20, placeholdersEnabled = false))
            .test()
        triggerScheduler()

        // Then
        observer.await().assertValue { it == expectedPage }
    }

    @Test
    fun `Loading consecutive page returns correct page`() {
        // Given
        val payments = listOf<PaymentData>(mock(), mock(), mock())
        whenever(paymentManager.fetchPaymentHistory(CURSOR_PAGE_1)).thenReturn(Single.just(PagedPaymentHistory(CURSOR_PAGE_2, payments)))
        val pagingSource = PaymentsPagingSource(paymentManager)
        val expectedPage = PagingSource.LoadResult.Page(
            data = payments,
            prevKey = null,
            nextKey = CURSOR_PAGE_2
        )

        // When
        val observer = pagingSource.loadSingle(PagingSource.LoadParams.Refresh(key = CURSOR_PAGE_1, loadSize = 20, placeholdersEnabled = false))
            .test()
        triggerScheduler()

        // Then
        observer.await().assertValue { it == expectedPage }
    }

    companion object {
        private const val CURSOR_PAGE_1 = "cursor1"
        private const val CURSOR_PAGE_2 = "cursor2"
    }
}
