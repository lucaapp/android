package de.culture4life.luca.network.pojo

import de.culture4life.luca.LucaUnitTest
import de.culture4life.luca.crypto.DailyPublicKeyData
import de.culture4life.luca.testtools.rules.FixedTimeRule
import de.culture4life.luca.testtools.samples.DailyKeySamples
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import java.math.BigInteger
import java.security.spec.ECPoint

class DailyPublicKeysResponseDataTest : LucaUnitTest() {

    @get:Rule
    val fixedTimeRule = FixedTimeRule()

    private val dailyPublicKeyResponseDataWithoutExpiration = DailyKeySamples.WithoutExpiration().dailyPublicKeyResponseData
    private val dailyKeyDataWithoutExpiration = DailyKeySamples.WithoutExpiration().dailyPublicKeyData

    private val dailyPublicKeyResponseDataWithExpiration = DailyKeySamples.WithExpiration().dailyPublicKeyResponseData
    private val dailyKeyDataWithExpiration = DailyKeySamples.WithExpiration().dailyPublicKeyData

    @Test
    fun parseDailyKeyJwt_validResponseWithoutExpiration_parsesKey() {
        val parsedData = dailyPublicKeyResponseDataWithoutExpiration.dailyPublicKeyData
        assertEquals(dailyKeyDataWithoutExpiration, parsedData)
        assertEquals(dailyPublicKeyResponseDataWithoutExpiration.dailyKeyJwt, parsedData.signedJwt)
    }

    @Test
    fun parseDailyKeyJwt_validResponseWithExpiration_parsesKey() {
        val parsedData = dailyPublicKeyResponseDataWithExpiration.dailyPublicKeyData
        assertEquals(dailyKeyDataWithExpiration, parsedData)
        assertEquals(dailyPublicKeyResponseDataWithExpiration.dailyKeyJwt, parsedData.signedJwt)
    }

    @Test
    fun expirationTimestampOrDefault_withExpiration_usesExpiration() {
        assertEquals(dailyKeyDataWithExpiration.expirationTimestamp, dailyKeyDataWithExpiration.expirationTimestampOrDefault)
    }

    @Test
    fun expirationTimestampOrDefault_withoutExpiration_usesDefault() {
        assertEquals(
            dailyKeyDataWithoutExpiration.creationTimestamp + DailyPublicKeyData.EXPIRATION_DURATION_DEFAULT,
            dailyKeyDataWithoutExpiration.expirationTimestampOrDefault
        )
    }

    @Test
    fun getPublicKey_validEncodedPublicKey_decodesKey() {
        val publicKey = dailyKeyDataWithoutExpiration.publicKey
        val expectedW = ECPoint(
            BigInteger("debc528d3089e1fc7063ac729b16928ae10fe44800c66919c27e367a217d6170", 16),
            BigInteger("fa2112639a1bba2773395562879dbdf34368d529f7fc83597cba1b74d70211bf", 16)
        )
        assertEquals(expectedW, publicKey.w)
    }
}
