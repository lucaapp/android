package de.culture4life.luca.util

import de.culture4life.luca.LucaUnitTest
import de.culture4life.luca.R
import io.github.kakaocup.kakao.common.utilities.getResourceString
import org.assertj.core.api.Assertions.assertThat
import org.junit.Assert.*
import org.junit.Test

class TextUtilTest : LucaUnitTest() {

    @Test
    fun `placeholder string`() {
        val resourceWithPlaceholder = R.string.pay_details_discount_hint
        val placeholder = "discountPercentage"
        assertThat(getResourceString(resourceWithPlaceholder)).containsPattern("\\[$placeholder]")

        val replacement = "testReplacement"
        val result = TextUtil.getPlaceholderString(application, resourceWithPlaceholder, placeholder to replacement)
        assertThat(result).contains(replacement)
    }

    @Test
    fun `placeholder string with old style`() {
        val resourceWithPlaceholder = R.string.pay_min_amount
        val placeholder = "minAmount"
        assertThat(getResourceString(resourceWithPlaceholder)).containsPattern("<$placeholder>")

        val replacement = "testReplacement"
        val result = TextUtil.getPlaceholderString(application, resourceWithPlaceholder, placeholder to replacement)
        assertThat(result).contains(replacement)
    }

    @Test
    fun `placeholder string fail when placeholder not found`() {
        val resourceWithPlaceholder = R.string.pay_min_amount
        val placeholder = "wrong"
        assertThat(getResourceString(resourceWithPlaceholder)).doesNotContain(placeholder)

        assertThrows(IllegalArgumentException::class.java) {
            val replacement = "testReplacement"
            TextUtil.getPlaceholderString(application, resourceWithPlaceholder, placeholder to replacement)
        }
    }
}
