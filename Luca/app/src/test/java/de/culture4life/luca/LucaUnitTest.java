package de.culture4life.luca;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.After;
import org.junit.Rule;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import de.culture4life.luca.testtools.rules.FixNestedSpiesMemoryLeakRule;
import de.culture4life.luca.testtools.rules.LoggingRule;
import de.culture4life.luca.testtools.rules.MemoryUsageRule;
import de.culture4life.luca.testtools.rules.ReplaceRxJavaSchedulersRule;
import de.culture4life.luca.ui.BaseViewModel;

@RunWith(AndroidJUnit4.class)
@Config(qualifiers = "de")
public abstract class LucaUnitTest {

    public ReplaceRxJavaSchedulersRule.TestSchedulersRule rxSchedulersRule = ReplaceRxJavaSchedulersRule.Companion.manualExecution();

    @Rule
    public RuleChain rules = RuleChain.emptyRuleChain()
            .around(new MemoryUsageRule())
            .around(new FixNestedSpiesMemoryLeakRule())
            .around(new LoggingRule())
            .around(new InstantTaskExecutorRule())
            .around(rxSchedulersRule);

    protected LucaApplication application;
    private final ArrayList<Object> spies = new ArrayList<>();

    public LucaUnitTest() {
        this.application = ApplicationProvider.getApplicationContext();
    }

    @After
    public void cleanupUnitTest() {
        disposeSpyInstances();
    }

    protected <ManagerType extends Manager> ManagerType getInitializedManager(ManagerType manager) {
        initializeManager(manager);
        return manager;
    }

    protected <ManagerType extends Manager> void initializeManager(ManagerType manager) {
        manager.initialize(application).blockingAwait();
    }

    protected <ManagerType extends Manager> void initializeManagers(ManagerType... managers) {
        for (ManagerType manager : managers) {
            initializeManager(manager);
        }
    }

    protected void advanceScheduler(long delayTime, TimeUnit unit) {
        rxSchedulersRule.getTestScheduler().advanceTimeBy(delayTime, unit);
    }

    protected void triggerScheduler() {
        rxSchedulersRule.getTestScheduler().triggerActions();
    }

    /**
     * Wrapper for Mockito.spy
     *
     * Collect all spy instances to ensure that they are disposed at the end of each test.
     * Necessary because in combination with mockito-inline the spy creates a new instance instead of spying the current.
     */
    public <T> T spy(T value) {
        T spy = Mockito.spy(value);
        spies.add(spy);
        return spy;
    }

    private void disposeSpyInstances() {
        // cleanup disposable leaks
        //  Through mockito-inline every spy creates a new object. So it's no part of LucaApplication anymore.
        //  That's why it doesn't becomes disposed after each test and we have to do it.
        for (Object spy : spies) {
            if (spy instanceof Manager) {
                ((Manager) spy).dispose();
            } else if (spy instanceof BaseViewModel) {
                ((BaseViewModel) spy).dispose();
            }
        }
    }
}
