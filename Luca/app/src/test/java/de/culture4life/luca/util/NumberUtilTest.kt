package de.culture4life.luca.util

import de.culture4life.luca.LucaUnitTest
import org.junit.Assert.assertEquals
import org.junit.Test

class NumberUtilTest : LucaUnitTest() {

    @Test
    fun toCurrencyAmountNumber() {
        assertEquals(0, "".toCurrencyAmountNumber())
        assertEquals(0, "0".toCurrencyAmountNumber())
        assertEquals(12345, "12345".toCurrencyAmountNumber())
        assertEquals(12345, "123 45\n".toCurrencyAmountNumber())
    }

    @Test
    fun toCurrencyAmountString() {
        assertEquals("0,00", 0.toCurrencyAmountString())
        assertEquals("123,45", 12345.toCurrencyAmountString())
    }
}
