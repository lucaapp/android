package de.culture4life.luca.payment

import org.junit.Assert.*
import org.junit.Test

class PaymentAmountsTest {

    private val amounts = PaymentAmounts(
        invoiceAmount = 899,
        tipPercentage = 15,
        discountPercentage = 25
    )

    @Test
    fun `Tip amount is based on tip percentage`() {
        assertEquals(135, amounts.tipAmount)

        val amountsWithoutTip = amounts.copy(tipPercentage = 0)
        assertEquals(0, amountsWithoutTip.tipAmount)
    }

    @Test
    fun `Discounted amount is based on discount percentage`() {
        assertEquals(225, amounts.discountedInvoiceAmount)
    }

    @Test
    fun `Discounted amount considers maximum discount amount`() {
        val amountsWithDiscountLimitSubceeded = amounts.copy(maximumDiscountAmount = 300)
        assertEquals(225, amountsWithDiscountLimitSubceeded.discountedInvoiceAmount)

        val amountsWithDiscountLimitExceeded = amounts.copy(maximumDiscountAmount = 100)
        assertEquals(amountsWithDiscountLimitExceeded.maximumDiscountAmount, amountsWithDiscountLimitExceeded.discountedInvoiceAmount)
    }

    @Test
    fun `Remaining invoice amount considers discount`() {
        assertEquals(674, amounts.remainingInvoiceAmount)

        val amountsWithoutDiscount = amounts.copy(discountPercentage = 0)
        assertEquals(amounts.invoiceAmount, amountsWithoutDiscount.remainingInvoiceAmount)
    }

    @Test
    fun `Total amount considers tip and discount`() {
        assertEquals(809, amounts.totalAmount)

        val amountsWithoutDiscount = amounts.copy(discountPercentage = 0)
        assertEquals(amounts.invoiceAmount + amounts.tipAmount, amountsWithoutDiscount.totalAmount)

        val amountsWithoutTip = amounts.copy(tipPercentage = 0)
        assertEquals(amounts.invoiceAmount - amounts.discountedInvoiceAmount, amountsWithoutTip.totalAmount)
    }

}
