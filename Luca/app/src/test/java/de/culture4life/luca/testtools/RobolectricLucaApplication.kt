package de.culture4life.luca.testtools

import de.culture4life.luca.LucaApplication
import de.culture4life.luca.testtools.rules.RxMissingDisposeRule
import org.robolectric.TestLifecycleApplication
import java.lang.reflect.Method

// The TestLifecycleApplication give us the possibility to hook in
//  - before Application.onCreate
//  - after Application.onTerminate
//  The default @Before and @After are between onCreate and onTerminate.
class RobolectricLucaApplication : LucaApplication(), TestLifecycleApplication {

    private val missingDisposeRule = RxMissingDisposeRule()

    override fun beforeTest(method: Method?) {
        missingDisposeRule.beforeTest()
    }

    override fun prepareTest(test: Any?) {}

    override fun afterTest(method: Method?) {
        missingDisposeRule.afterTest()
    }
}
