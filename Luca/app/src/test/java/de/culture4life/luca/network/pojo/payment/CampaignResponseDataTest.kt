package de.culture4life.luca.network.pojo.payment

import de.culture4life.luca.testtools.samples.LucaPaySamples
import de.culture4life.luca.util.TimeUtil
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import java.util.concurrent.TimeUnit

class CampaignResponseDataTest {

    @Test
    fun `Not yet started campaign is not active`() {
        var campaign = LucaPaySamples.InActiveCampaign().copy(
            startTimestamp = TimeUtil.getCurrentMillis() + TimeUnit.DAYS.toMillis(1),
            endTimestamp = TimeUtil.getCurrentMillis() + TimeUnit.DAYS.toMillis(2)
        )
        assertFalse(campaign.responseData.paymentCampaign.isCurrentlyActive)

        campaign = campaign.copy(startTimestamp = null)
        assertFalse(campaign.responseData.paymentCampaign.isCurrentlyActive)
    }

    @Test
    fun `Already ended campaign is not active`() {
        val campaign = LucaPaySamples.InActiveCampaign().copy(
            startTimestamp = TimeUtil.getCurrentMillis() - TimeUnit.DAYS.toMillis(2),
            endTimestamp = TimeUtil.getCurrentMillis() - TimeUnit.DAYS.toMillis(1)
        )
        assertFalse(campaign.responseData.paymentCampaign.isCurrentlyActive)
    }

    @Test
    fun `Started and not yet ended campaign is active`() {
        var campaign = LucaPaySamples.ActiveCampaign().copy(
            startTimestamp = TimeUtil.getCurrentMillis() - TimeUnit.DAYS.toMillis(1),
            endTimestamp = TimeUtil.getCurrentMillis() + TimeUnit.DAYS.toMillis(1)
        )
        assertTrue(campaign.responseData.paymentCampaign.isCurrentlyActive)

        campaign = campaign.copy(endTimestamp = null)
        assertTrue(campaign.responseData.paymentCampaign.isCurrentlyActive)
    }
}
