package de.culture4life.luca.ui.checkin

import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.NavGraph
import de.culture4life.luca.LucaUnitTest
import de.culture4life.luca.R
import de.culture4life.luca.checkin.CheckInManager
import de.culture4life.luca.crypto.CryptoManager
import de.culture4life.luca.document.Document
import de.culture4life.luca.document.DocumentManager
import de.culture4life.luca.meeting.MeetingManager
import de.culture4life.luca.network.pojo.LocationResponseData
import de.culture4life.luca.registration.RegistrationManager
import de.culture4life.luca.testtools.samples.CovidDocumentSamples
import de.culture4life.luca.testtools.samples.LocationSamples
import de.culture4life.luca.ui.ViewEvent
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.kotlin.*
import java.security.interfaces.ECPublicKey
import java.util.*

class CheckInViewModelTest : LucaUnitTest() {

    private lateinit var viewModel: CheckInViewModel
    private lateinit var registrationManager: RegistrationManager
    private lateinit var meetingManager: MeetingManager
    private lateinit var checkInManager: CheckInManager
    private lateinit var cryptoManager: CryptoManager
    private lateinit var documentManager: DocumentManager

    @Before
    fun before() {
        val applicationSpy = spy(application)
        registrationManager = spy(application.registrationManager)
        meetingManager = spy(application.meetingManager)
        checkInManager = spy(application.checkInManager)
        cryptoManager = spy(application.cryptoManager)
        documentManager = spy(application.documentManager)
        doReturn(registrationManager).`when`(applicationSpy).registrationManager
        doReturn(meetingManager).`when`(applicationSpy).meetingManager
        doReturn(checkInManager).`when`(applicationSpy).checkInManager
        doReturn(cryptoManager).`when`(applicationSpy).cryptoManager
        doReturn(documentManager).`when`(applicationSpy).documentManager

        doReturn(Maybe.just(UUID.randomUUID())).`when`(registrationManager).getUserIdIfAvailable()
        initializeManager(checkInManager)
        viewModel = spy(CheckInViewModel(applicationSpy))
    }

    @Test
    fun `If no daily public key is available then isDailyPublicKeyAvailable returns false`() {
        // Given
        doReturn(Single.just(false)).`when`(checkInManager).isTracingPossible

        // When
        viewModel.initialize().test()
        rxSchedulersRule.testScheduler.triggerActions()

        // Then
        assertFalse(viewModel.isTracingEnabled.value!!)
    }

    @Test
    fun `If tracing is possible then isTracingEnabled returns true`() {
        // Given
        doReturn(Single.just(true)).`when`(checkInManager).isTracingPossible

        // When
        viewModel.initialize().test()
        rxSchedulersRule.testScheduler.triggerActions()

        // Then
        assertTrue(viewModel.isTracingEnabled.value!!)
    }

    @Test
    fun getScannerIdFromUrl_validCheckInUrl_emitsUuid() {
        val location = LocationSamples.Restaurant()
        CheckInViewModel.getScannerIdFromUrl(location.qrCodeContent)
            .map(UUID::toString)
            .test()
            .assertValue(location.scannerId)
    }

    @Test
    fun getScannerIdFromUrl_validCheckInUrlWithLucaAndCwaData_emitsUuid() {
        val location = LocationSamples.Restaurant()
        CheckInViewModel.getScannerIdFromUrl(location.qrCodeContentWithLucaDataAndCWA)
            .map(UUID::toString)
            .test()
            .assertValue(location.scannerId)
    }

    @Test
    fun `If no contact data is provided then isContactDataMissing returns true`() {
        // Given
        doReturn(Single.just(false)).`when`(registrationManager).hasProvidedRequiredContactData()

        // When
        viewModel.checkIfContactDataMissing()
        rxSchedulersRule.testScheduler.triggerActions()

        // Then
        assertTrue(viewModel.isContactDataMissing.value!!)
    }

    @Test
    fun `If currently hosting a meeting then navigate to meeting fragment when checking`() {
        // Given
        val navController = mock<NavController>()
        val currentGraph = mock<NavGraph>()
        val currentDestination = mock<NavDestination>()
        whenever(currentDestination.id).thenReturn(R.id.checkInFragment)
        whenever(navController.graph).thenReturn(currentGraph)
        whenever(navController.currentDestination).thenReturn(currentDestination)
        viewModel.navigationController = navController
        doReturn(Single.just(true)).`when`(meetingManager).isCurrentlyHostingMeeting

        // When
        viewModel.checkIfHostingMeeting()
        rxSchedulersRule.testScheduler.triggerActions()

        // Then
        verify(navController, times(1)).navigate(eq(R.id.action_checkInFragment_to_meetingFragment), eq(null))
    }

    @Test
    fun `Can process bar code if given url has correct host, path and id`() {
        val meeting = LocationSamples.Meeting()
        assertTrue(viewModel.canProcessBarcode(meeting.qrCodeContent))
    }

    @Test
    fun `Checking in with a valid deeplink completes without errors`() {
        // Given
        val location = LocationSamples.Restaurant()
        doReturn(Completable.error(IllegalStateException())).`when`(checkInManager).assertCheckedInToPrivateMeeting()
        var addedAdditionalCheckInProperties = 0
        doReturn(Completable.fromAction { addedAdditionalCheckInProperties++ }).`when`(checkInManager).addAdditionalCheckInProperties(any(), any())
        doReturn(Single.just(mock<ECPublicKey>())).`when`(checkInManager).getLocationPublicKey(UUID.fromString(location.scannerId))
        val qrCodeData = mock<QrCodeData>()
        var numberOfTimesCheckedIn = 0
        doReturn(Completable.fromAction { numberOfTimesCheckedIn++ }).`when`(checkInManager).checkIn(UUID.fromString(location.scannerId), qrCodeData)
        doReturn(Single.just(qrCodeData)).`when`(viewModel).generateQrCodeData(false, false)

        // When
        val observer = viewModel.handleSelfCheckInDeepLink(location.qrCodeContentWithLucaData, false, false).test()
        rxSchedulersRule.testScheduler.triggerActions()

        // Then
        observer.await().assertNoErrors()
        assertEquals(numberOfTimesCheckedIn, 1)
        assertEquals(addedAdditionalCheckInProperties, 1)
    }

    @Test
    fun `Anonymous Checking in with a valid deeplink completes without errors`() {
        // Given
        val location = LocationSamples.Restaurant()
        doReturn(Completable.error(IllegalStateException())).`when`(checkInManager).assertCheckedInToPrivateMeeting()
        var addedAdditionalCheckInProperties = 0
        doReturn(Completable.fromAction { addedAdditionalCheckInProperties++ }).`when`(checkInManager).addAdditionalCheckInProperties(any(), any())
        doReturn(Single.just(mock<ECPublicKey>())).`when`(checkInManager).getLocationPublicKey(UUID.fromString(location.scannerId))
        val qrCodeData = mock<QrCodeData>()
        var numberOfTimesCheckedIn = 0
        doReturn(Completable.fromAction { numberOfTimesCheckedIn++ }).`when`(checkInManager).checkIn(UUID.fromString(location.scannerId), qrCodeData)
        doReturn(Single.just(qrCodeData)).`when`(viewModel).generateQrCodeData(true, false)

        // When
        val observer = viewModel.handleSelfCheckInDeepLink(location.qrCodeContentWithLucaData, true, false).test()
        rxSchedulersRule.testScheduler.triggerActions()

        // Then
        observer.await().assertNoErrors()
        assertEquals(numberOfTimesCheckedIn, 1)
        assertEquals(addedAdditionalCheckInProperties, 1)
    }

    @Test
    fun `Calling onCheckInMultiConfirmDismissed emits correct event`() {
        // When
        viewModel.onCheckInMultiConfirmDismissed()

        // Then
        assertEquals(ViewEvent(true), viewModel.showCameraPreview.value)
    }

    @Test
    fun `Calling onImportDocumentConfirmationDismissed emits correct event`() {
        // When
        viewModel.onImportDocumentConfirmationDismissed()

        // Then
        assertEquals(ViewEvent(true), viewModel.showCameraPreview.value)
    }

    @Test
    fun `Joining private meeting with valid url completes without errors with anonymous qr code data`() {
        // Given
        val location = LocationSamples.Restaurant()
        doReturn(Completable.complete()).`when`(checkInManager).assertCheckedInToPrivateMeeting()
        var addedAdditionalCheckInProperties = 0
        doReturn(Completable.fromAction { addedAdditionalCheckInProperties++ }).`when`(checkInManager).addAdditionalCheckInProperties(any(), any())
        doReturn(Single.just(mock<ECPublicKey>())).`when`(checkInManager).getLocationPublicKey(UUID.fromString(location.scannerId))
        val qrCodeData = mock<QrCodeData>()
        var numberOfTimesCheckedIn = 0
        doReturn(Completable.fromAction { numberOfTimesCheckedIn++ }).`when`(checkInManager).checkIn(UUID.fromString(location.scannerId), qrCodeData)
        doReturn(Single.just(qrCodeData)).`when`(viewModel).generateQrCodeData(true, false)

        // When
        viewModel.onPrivateMeetingJoinApproved(location.qrCodeContentWithLucaData)
        rxSchedulersRule.testScheduler.triggerActions()

        // Then
        assertTrue(viewModel.errors.value.isNullOrEmpty())
        assertEquals(numberOfTimesCheckedIn, 1)
        assertEquals(addedAdditionalCheckInProperties, 1)
    }

    @Test
    fun `Calling onPrivateMeetingJoinDismissed emits correct event`() {
        // When
        viewModel.onPrivateMeetingJoinDismissed("123")

        // Then
        assertEquals(ViewEvent(true), viewModel.showCameraPreview.value)
    }

    @Test
    fun `When requesting to create a private meeting navigate to correct destination`() {
        // Given
        val navController = mock<NavController>()
        val currentGraph = mock<NavGraph>()
        val currentDestination = mock<NavDestination>()
        whenever(currentDestination.id).thenReturn(R.id.checkInFragment)
        whenever(navController.graph).thenReturn(currentGraph)
        whenever(navController.currentDestination).thenReturn(currentDestination)
        viewModel.navigationController = navController
        doReturn(Completable.complete()).`when`(meetingManager).createPrivateMeeting()

        // When
        viewModel.onPrivateMeetingCreationRequested()
        rxSchedulersRule.testScheduler.triggerActions()

        // Then
        verify(navController, times(1)).navigate(eq(R.id.action_checkInFragment_to_meetingFragment), eq(null))
    }

    @Test
    fun `Calling onPrivateMeetingCreationDismissed emits correct event`() {
        // When
        viewModel.onPrivateMeetingCreationDismissed()

        // Then
        assertEquals(ViewEvent(true), viewModel.showCameraPreview.value)
    }

    @Test
    fun `Calling onContactDataMissingDialogDismissed emits correct event`() {
        // When
        viewModel.onContactDataMissingDialogDismissed()

        // Then
        assertEquals(ViewEvent(true), viewModel.showCameraPreview.value)
    }

    @Test
    fun `Calling processBarcode with location url emits correct event`() {
        // Given
        val location = LocationSamples.Restaurant()
        val locationData = mock<LocationResponseData>()
        doReturn("anyLocationId").`when`(locationData).locationId
        doReturn(Single.just(locationData)).`when`(checkInManager).getLocationDataFromScannerId(location.scannerId)
        doReturn(Single.just(true)).`when`(checkInManager).isTracingPossible
        doReturn(Single.just(true)).`when`(registrationManager).hasProvidedRequiredContactData()

        // When
        val observer = viewModel.processBarcode(location.qrCodeContentWithLucaData).test()
        rxSchedulersRule.testScheduler.triggerActions()

        // Then
        observer.await().assertNoErrors()
        assertEquals(ViewEvent(androidx.core.util.Pair(location.qrCodeContentWithLucaData, locationData)), viewModel.checkInMultiConfirm.value)
    }

    @Test
    fun `Calling processBarcode with document content emits correct event`() {
        // Given
        val document = CovidDocumentSamples.ErikaMustermann.EudccFullyVaccinated()
        val documentQrCodeContent = document.qrCodeContent
        doReturn(Single.just(mock<Document>())).`when`(documentManager).parseAndValidateEncodedDocument(documentQrCodeContent)

        // When
        val observer = viewModel.processBarcode(documentQrCodeContent).test()
        rxSchedulersRule.testScheduler.triggerActions()

        // Then
        observer.await().assertNoErrors()
        assertEquals(ViewEvent(documentQrCodeContent), viewModel.possibleDocumentData.value)
    }
}
