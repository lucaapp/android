package de.culture4life.luca.util

import de.culture4life.luca.LucaUnitTest
import io.reactivex.rxjava3.core.Completable
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.verifyNoMoreInteractions
import org.mockito.kotlin.whenever
import java.util.concurrent.TimeUnit

class CompletableUtilTest : LucaUnitTest() {

    private val mockList = mock(List::class.java)

    private val retryWhenWithDelayTest = Completable.fromCallable {
        mockList.size
    }.retryWhenWithDelay(MAX_DELAY_IN_MS) { it is IllegalStateException }

    @Test
    fun `RxJava chain with retryWhenWithDelay is successful no retry is triggered`() {
        whenever(mockList.size).then { 0 }

        retryWhenWithDelayTest.test()

        val maxInvokeTime = MAX_RETRIES * MAX_DELAY_IN_MS
        val maxInvocations = 1

        advanceScheduler(maxInvokeTime, TimeUnit.MILLISECONDS)
        verify(mockList, times(maxInvocations)).size

        advanceScheduler(FAR_FUTURE_DELAY_IN_MS, TimeUnit.MILLISECONDS)
        verifyNoMoreInteractions(mockList)
    }

    @Test
    fun `retryWhenWithDelay will retry MAX_RETRIES when predicate is true`() {
        whenever(mockList.size).then { throw IllegalStateException() }

        retryWhenWithDelayTest.test()

        val maxInvokeTime = MAX_RETRIES * MAX_DELAY_IN_MS
        val maxInvocations = MAX_RETRIES + 1

        advanceScheduler(maxInvokeTime, TimeUnit.MILLISECONDS)
        verify(mockList, times(maxInvocations)).size

        advanceScheduler(FAR_FUTURE_DELAY_IN_MS, TimeUnit.MILLISECONDS)
        verifyNoMoreInteractions(mockList)
    }

    @Test
    fun `retryWhenWithDelay will not retry when predicate is false`() {
        whenever(mockList.size).then { throw RuntimeException() }

        retryWhenWithDelayTest.test()

        val maxInvokeTime = MAX_RETRIES * MAX_DELAY_IN_MS
        val maxInvocations = 1

        advanceScheduler(maxInvokeTime, TimeUnit.MILLISECONDS)
        verify(mockList, times(maxInvocations)).size

        advanceScheduler(FAR_FUTURE_DELAY_IN_MS, TimeUnit.MILLISECONDS)
        verifyNoMoreInteractions(mockList)
    }

    companion object {
        private const val MAX_RETRIES = 3
        private val MAX_DELAY_IN_MS = TimeUnit.SECONDS.toMillis(30)
        private val FAR_FUTURE_DELAY_IN_MS = TimeUnit.DAYS.toMillis(30)
    }
}
