package de.culture4life.luca.util

import de.culture4life.luca.LucaUnitTest
import io.reactivex.rxjava3.core.Single
import org.junit.Test

class SingleUtilTest : LucaUnitTest() {

    @Test
    fun `Asserting matching predicate completes`() {
        Single.just(2)
            .assertValue({ true })
            .test()
            .assertComplete()
    }

    @Test
    fun `Asserting non-matching predicate emits error`() {
        Single.just(2)
            .assertValue({ false })
            .test()
            .assertError(IllegalStateException::class.java)
    }

    @Test
    fun `Asserting non-matching predicate emits custom error if provided`() {
        Single.just(2)
            .assertValue({ it < 1 }, { IllegalArgumentException("Number must be < 1 but was $it") })
            .test()
            .assertError(IllegalArgumentException::class.java)
    }
}
