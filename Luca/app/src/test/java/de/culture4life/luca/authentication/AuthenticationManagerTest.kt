package de.culture4life.luca.authentication

import de.culture4life.luca.LucaUnitTest
import de.culture4life.luca.testtools.rules.FixedTimeRule
import de.culture4life.luca.util.TimeUtil
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import org.junit.Rule
import org.junit.Test
import timber.log.Timber
import java.util.concurrent.TimeUnit

class AuthenticationManagerTest : LucaUnitTest() {

    @get:Rule
    var fixedTimeRule = FixedTimeRule(testScheduler = rxSchedulersRule.testScheduler)

    private var authenticationManager = spy(getInitializedManager(application.authenticationManager))

    @Test
    fun `Requesting new authentication if previous authentication result not valid anymore`() {
        val firstAuthenticationRequest = AuthenticationRequest(
            id = "test",
            callback = { },
            validityDuration = AuthenticationRequest.VALIDITY_DURATION_NONE // previous result may not be re-used for new requests
        )

        givenAuthenticationRequestResult(firstAuthenticationRequest.id, AuthenticationResult(success = true))
        authenticationManager.requestAuthentication(firstAuthenticationRequest).test()

        fixedTimeRule.advanceBy(AUTHENTICATION_DELAY_DEFAULT, TimeUnit.MILLISECONDS) // not within validity duration

        givenAuthenticationRequestResult(firstAuthenticationRequest.id, AuthenticationResult(success = false))
        val secondAuthenticationRequest = firstAuthenticationRequest.copy(timestamp = TimeUtil.getCurrentMillis())

        val request = authenticationManager.requestAuthenticationIfRequiredAndGetResult(secondAuthenticationRequest)
        assertAuthenticationResult(request, false)
    }

    @Test
    fun `Not requesting authentication if previous authentication result still valid`() {
        val firstAuthenticationRequest = AuthenticationRequest(
            id = "test",
            callback = { },
            validityDuration = TimeUnit.SECONDS.toMillis(10) // previous result may be re-used for new requests
        )

        givenAuthenticationRequestResult(firstAuthenticationRequest.id, AuthenticationResult(success = true))
        authenticationManager.requestAuthentication(firstAuthenticationRequest).test()

        fixedTimeRule.advanceBy(5, TimeUnit.SECONDS)

        val expectedResult = authenticationManager.getAuthenticationResult(firstAuthenticationRequest.id).blockingGet()
        authenticationManager.requestAuthenticationIfRequiredAndGetResult(firstAuthenticationRequest.copy())
            .test()
            .assertValue(expectedResult)
    }

    @Test
    fun `Not requesting authentication if previous authentication still in progress`() {
        val firstAuthenticationRequest = AuthenticationRequest(id = "test", callback = { })
        givenAuthenticationRequestResult(firstAuthenticationRequest.id, AuthenticationResult(success = true))

        val allRequestObserver = authenticationManager.getAuthenticationRequests()
            .test()

        authenticationManager.requestAuthentication(firstAuthenticationRequest)
            .test()
            .assertComplete() // triggers the first request

        authenticationManager.requestAuthenticationIfRequired(firstAuthenticationRequest.copy())
            .test()
            .assertComplete() // should not trigger a new request as the first one is still in progress

        val thirdRequestObserver = authenticationManager.requestAuthenticationIfRequiredAndGetResult(firstAuthenticationRequest.copy())
            .test()
            .assertEmpty() // should be waiting for result of first request

        fixedTimeRule.advanceBy(1, TimeUnit.SECONDS)

        thirdRequestObserver.assertComplete()
        allRequestObserver.assertValue(firstAuthenticationRequest) // contains only the first request because the second and third were not required
    }

    @Test
    fun `Requesting multiple authentications simultaneously triggers emitting of only one authentication request`() {
        val firstAuthenticationRequest = AuthenticationRequest(id = "test", callback = { })
        val requestObserver = authenticationManager.getAuthenticationRequests()
            .test()
            .assertNoValues()

        Single.merge(
            authenticationManager.requestAuthenticationIfRequiredAndGetResult(firstAuthenticationRequest),
            authenticationManager.requestAuthenticationIfRequiredAndGetResult(firstAuthenticationRequest.copy())
        ).test()

        requestObserver.assertValue(firstAuthenticationRequest)
    }

    private fun givenAuthenticationRequestResult(
        id: String,
        authenticationResult: AuthenticationResult,
        delay: Long = AUTHENTICATION_DELAY_DEFAULT
    ) {
        authenticationManager.getAuthenticationRequests()
            .doOnNext { Timber.d("Received authentication request: $it") }
            .filter { it.id == id }
            .firstOrError() // only handle the next request
            .delay(delay, TimeUnit.MILLISECONDS, Schedulers.io()) // simulate some delay
            .flatMapCompletable {
                authenticationManager.processAuthenticationResult(
                    id,
                    authenticationResult.copy(timestamp = it.timestamp + 1) // needs to be updated to be after the request timestamp
                )
            }
            .test()
    }

    private fun assertAuthenticationResult(
        request: Single<AuthenticationResult>,
        expectSuccess: Boolean,
        delay: Long = AUTHENTICATION_DELAY_DEFAULT
    ) {
        val requestObserver = request.map { it.success }.test()

        fixedTimeRule.advanceBy(delay, TimeUnit.MILLISECONDS)

        requestObserver.assertValue(expectSuccess)
    }

    companion object {
        const val AUTHENTICATION_DELAY_DEFAULT = 10L
    }
}
