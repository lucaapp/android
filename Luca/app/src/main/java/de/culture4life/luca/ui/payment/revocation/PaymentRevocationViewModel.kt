package de.culture4life.luca.ui.payment.revocation

import android.app.Application
import androidx.lifecycle.MutableLiveData
import de.culture4life.luca.R
import de.culture4life.luca.ui.BaseViewModel
import de.culture4life.luca.whatisnew.WhatIsNewManager
import io.reactivex.rxjava3.core.Completable

class PaymentRevocationViewModel(application: Application) : BaseViewModel(application) {

    private val paymentManager = this.application.paymentManager
    private val whatIsNewManager = this.application.whatIsNewManager

    val revocationCode = MutableLiveData<String>()

    override fun initialize(): Completable {
        return super.initialize()
            .andThen(
                Completable.mergeArray(
                    paymentManager.initialize(application),
                    whatIsNewManager.initialize(application)
                )
            )
            .andThen(invoke(updateRevocationCodeImmediately()))
            .andThen(invoke(markVerificationSuccessfulMessageAsSeen()))
    }

    private fun updateRevocationCodeImmediately(): Completable {
        return paymentManager.getRevocationCode()
            .onErrorReturnItem(application.getString(R.string.unknown))
            .flatMapCompletable { updateInstantly(revocationCode, it) }
    }

    private fun markVerificationSuccessfulMessageAsSeen() =
        whatIsNewManager.markMessageAsSeen(WhatIsNewManager.ID_LUCA_PAY_REVOCATION_CODE_MESSAGE)
}
