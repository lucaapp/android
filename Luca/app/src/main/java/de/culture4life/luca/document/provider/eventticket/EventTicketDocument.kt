package de.culture4life.luca.document.provider.eventticket

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import de.culture4life.luca.document.Document

data class EventTicketDocument(
    @SerializedName("id")
    override var id: String,
    @SerializedName("importTimestamp")
    override var importTimestamp: Long,
    @SerializedName("encodedData")
    override var encodedData: String,
    @SerializedName("hashableEncodedData")
    override var hashableEncodedData: String,
    @SerializedName("isVerified")
    override var isVerified: Boolean,
    @SerializedName("type")
    override var type: Int,
    @SerializedName("issuer")
    var issuer: String,
    @SerializedName("locationName")
    val locationName: String?,
    @SerializedName("eventName")
    val eventName: String,
    @SerializedName("category")
    val category: Category,
    @SerializedName("startTimestamp")
    val startTimestamp: Long,
    @SerializedName("endTimestamp")
    val endTimestamp: Long,
    @SerializedName("creationTimestamp")
    var creationTimestamp: Long,
    @SerializedName("verificationId")
    val verificationId: String?,
    @SerializedName("firstName")
    val firstName: String,
    @SerializedName("lastName")
    val lastName: String
) : Document {

    override var expirationTimestamp: Long = endTimestamp

    override val deletionTimestamp: Long
        get() = throw NotImplementedError() // TODO

    @Keep
    enum class Category(val value: Int) {
        OTHER(0),
        PARTY(1),
        CLASSIC(2),
        CULTURE(3),
        EXHIBITION(4),
        CONFERENCE(5),
        MOVIE(6),
        FAMILY(7),
        SPORT(8),
        MUSIC(9);

        companion object {
            fun fromInt(value: Int?) = values().firstOrNull { it.value == value } ?: OTHER
        }
    }
}
