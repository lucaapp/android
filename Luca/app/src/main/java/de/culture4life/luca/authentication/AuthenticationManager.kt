package de.culture4life.luca.authentication

import android.content.Context
import androidx.biometric.BiometricManager
import de.culture4life.luca.Manager
import de.culture4life.luca.R
import de.culture4life.luca.util.TimeUtil
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.subjects.BehaviorSubject
import io.reactivex.rxjava3.subjects.PublishSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit

open class AuthenticationManager : Manager() {

    private val authenticationRequestsSubject: PublishSubject<AuthenticationRequest> = PublishSubject.create()
    private val authenticationRequests: MutableMap<String, AuthenticationRequest> = HashMap()
    private val authenticationResultSubjects: MutableMap<String, BehaviorSubject<AuthenticationResult>> = HashMap()

    override fun doInitialize(context: Context): Completable {
        return Completable.complete()
    }

    override fun dispose() {
        authenticationRequests.clear()
        authenticationResultSubjects.clear()
        super.dispose()
    }

    /*
        Convenience
     */

    fun authenticateLucaIdEnrollment(): Completable {
        return Single.fromCallable {
            createDefaultAuthenticationRequest(ID_ENROLL_LUCA_ID).copy(
                promptSubtitle = application.getString(R.string.authentication_luca_id_enrollment_subtitle),
                promptDescription = application.getString(R.string.authentication_luca_id_enrollment_description)
            )
        }.flatMapCompletable(::requestAuthenticationIfRequiredAndAssertAuthenticated)
    }

    fun authenticateLucaIdDisplay(): Completable {
        return Single.fromCallable {
            createDefaultAuthenticationRequest(ID_DISPLAY_LUCA_ID).copy(
                promptSubtitle = application.getString(R.string.authentication_luca_id_ident_subtitle),
                promptDescription = application.getString(R.string.authentication_luca_id_ident_description),
                validityDuration = TimeUnit.SECONDS.toMillis(30)
            )
        }.flatMapCompletable(::requestAuthenticationIfRequiredAndAssertAuthenticated)
    }

    private fun createDefaultAuthenticationRequest(id: String): AuthenticationRequest {
        return AuthenticationRequest(
            id = id,
            callback = createDefaultAuthenticationCallback(id),
            promptTitle = application.getString(R.string.authentication_title)
        )
    }

    private fun createDefaultAuthenticationCallback(id: String): AuthenticationCallback {
        return AuthenticationCallback { invoke(processAuthenticationResult(id, it)).subscribe() }
    }

    fun isAuthenticationPossible(authenticationRequest: AuthenticationRequest): Boolean {
        return when (BiometricManager.from(context).canAuthenticate(authenticationRequest.allowedAuthenticators)) {
            BiometricManager.BIOMETRIC_SUCCESS -> true
            BiometricManager.BIOMETRIC_STATUS_UNKNOWN -> true // docs say "possible fine" you should try
            else -> false
        }
    }

    /*
        Requests
     */

    fun getAuthenticationRequests(): Observable<AuthenticationRequest> {
        return authenticationRequestsSubject
    }

    fun requestAuthenticationIfRequiredAndAssertAuthenticated(request: AuthenticationRequest): Completable {
        return requestAuthenticationIfRequiredAndGetResult(request)
            .flatMapCompletable { assertAuthenticated(request) }
    }

    fun requestAuthenticationIfRequiredAndGetResult(request: AuthenticationRequest): Single<AuthenticationResult> {
        return waitForPreviousAuthenticationRequestResultIfAvailable(request.id)
            .andThen(assertAuthenticated(request))
            .andThen(getAuthenticationResult(request.id))
            .onErrorResumeWith(requestAuthenticationAndGetResult(request))
    }

    fun requestAuthenticationIfRequired(request: AuthenticationRequest): Completable {
        return assertAuthenticated(request)
            .onErrorResumeWith(
                getOngoingAuthenticationRequestIfAvailable(request.id)
                    .isEmpty.filter { it } // only proceed if there's no ongoing authentication request
                    .flatMapCompletable { requestAuthentication(request) }
            )
    }

    fun requestAuthenticationAndGetResult(request: AuthenticationRequest): Single<AuthenticationResult> {
        return Single.fromCallable { TimeUtil.getCurrentMillis() }
            .flatMap { requestTimestamp ->
                requestAuthentication(request)
                    .andThen(getAuthenticationResults(request.id))
                    .filter { it.timestamp >= requestTimestamp }
                    .firstOrError()
            }
    }

    fun requestAuthentication(request: AuthenticationRequest): Completable {
        return Completable.fromAction {
            authenticationRequests[request.id] = request
            authenticationRequestsSubject.onNext(request)
        }.doOnSubscribe { Timber.d("Requesting authentication: $request") }
    }

    private fun getOngoingAuthenticationRequestIfAvailable(id: String): Maybe<AuthenticationRequest> {
        return Maybe.fromCallable {
            val request = getPreviousAuthenticationRequest(id)
            val result = getPreviousAuthenticationResult(id)
            if (request != null && (result == null || result.timestamp < request.timestamp)) {
                request
            } else {
                null
            }
        }
    }

    /*
        Results
     */

    fun processAuthenticationResult(id: String, result: AuthenticationResult): Completable {
        return Completable.fromAction {
            getOrCreateAuthenticationResultSubject(id).onNext(result)
        }.doOnSubscribe { Timber.d("Processing authentication result for $id: $result") }
    }

    fun getAuthenticationResults(id: String): Observable<AuthenticationResult> {
        return Observable.defer { getOrCreateAuthenticationResultSubject(id) }
    }

    fun getAuthenticationResult(id: String): Single<AuthenticationResult> {
        return getAuthenticationResults(id).firstOrError()
    }

    private fun getNextAuthenticationResult(id: String): Single<AuthenticationResult> {
        return Single.fromCallable { TimeUtil.getCurrentMillis() }
            .flatMap { minimumTimestamp ->
                getAuthenticationResults(id)
                    .filter { it.timestamp > minimumTimestamp }
                    .firstOrError()
            }
    }

    private fun getAuthenticationResultIfAvailable(id: String): Maybe<AuthenticationResult> {
        return Maybe.defer {
            val subject = getOrCreateAuthenticationResultSubject(id)
            if (subject.hasValue()) {
                subject.firstElement()
            } else {
                Maybe.empty()
            }
        }
    }

    private fun getOrCreateAuthenticationResultSubject(id: String): BehaviorSubject<AuthenticationResult> {
        var subject = authenticationResultSubjects[id]
        if (subject == null) {
            subject = BehaviorSubject.create()
            authenticationResultSubjects[id] = subject
            Timber.v("Created result subject for $id")
        }
        return subject!!
    }

    /**
     * If an authentication request for the given ID is currently in progress, this will wait
     * until a result is available (without doing anything). If there's no request or the
     * result is already available, this will complete instantly.
     */
    private fun waitForPreviousAuthenticationRequestResultIfAvailable(id: String): Completable {
        return getOngoingAuthenticationRequestIfAvailable(id)
            .flatMapSingle { getNextAuthenticationResult(id) }
            .ignoreElement()
    }

    /*
        Assertions
     */

    fun assertAuthenticated(request: AuthenticationRequest): Completable {
        return assertAuthenticatedUsingPreviousResult(request)
            .onErrorResumeNext { assertAuthenticatedUsingCurrentResult(request) }
    }

    private fun assertAuthenticatedUsingCurrentResult(request: AuthenticationRequest): Completable {
        return getAuthenticationResultIfAvailable(request.id)
            .switchIfEmpty(Maybe.error(AuthenticationException("No authentication result available for ${request.id}")))
            .flatMapCompletable { assertAuthenticatedUsingCurrentResult(request, it) }
    }

    private fun assertAuthenticatedUsingCurrentResult(request: AuthenticationRequest, result: AuthenticationResult): Completable {
        return Completable.defer {
            if (request.isFulfilledByResult(result)) {
                Completable.complete()
            } else {
                Completable.error(AuthenticationException("Authentication result doesn't fulfill request"))
            }
        }.doOnSubscribe { Timber.v("Asserting that user is authenticated for $request with $result") }
    }

    private fun assertAuthenticatedUsingPreviousResult(request: AuthenticationRequest): Completable {
        return getPreviousAuthenticationRequestAndResultIfAvailable(request.id)
            .switchIfEmpty(Maybe.error(AuthenticationException("No previous authentication result available for ${request.id}")))
            .flatMapCompletable { (previousRequest, previousResult) ->
                assertAuthenticatedUsingPreviousResult(
                    request,
                    previousRequest,
                    previousResult
                )
            }
    }

    private fun assertAuthenticatedUsingPreviousResult(
        currentRequest: AuthenticationRequest,
        previousRequest: AuthenticationRequest,
        previousResult: AuthenticationResult
    ): Completable {
        return Completable.defer {
            if (currentRequest.isFulfilledByPreviousRequestResult(previousRequest, previousResult)) {
                Completable.complete()
            } else {
                Completable.error(AuthenticationException("Previous authentication result doesn't fulfill request"))
            }
        }.doOnSubscribe { Timber.v("Asserting that user was previously authenticated for $currentRequest with $previousResult") }
    }

    /*
        Previous requests & results
     */

    private fun getPreviousAuthenticationRequestAndResultIfAvailable(id: String): Maybe<Pair<AuthenticationRequest, AuthenticationResult>> {
        return Maybe.fromCallable {
            val request = getPreviousAuthenticationRequest(id)
            val result = getPreviousAuthenticationResult(id)
            if (request != null && result != null && result.timestamp >= request.timestamp) {
                Pair(request, result)
            } else {
                null
            }
        }
    }

    private fun getPreviousAuthenticationRequest(id: String): AuthenticationRequest? {
        return authenticationRequests[id]
    }

    private fun getPreviousAuthenticationResult(id: String): AuthenticationResult? {
        val subject = getOrCreateAuthenticationResultSubject(id)
        return if (subject.hasValue()) {
            subject.firstElement().blockingGet()
        } else {
            null
        }
    }

    companion object {
        const val ID_ENROLL_LUCA_ID = "enroll_luca_id"
        const val ID_DISPLAY_LUCA_ID = "display_luca_id"
    }
}
