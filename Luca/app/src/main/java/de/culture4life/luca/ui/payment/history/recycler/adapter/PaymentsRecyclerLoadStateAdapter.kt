package de.culture4life.luca.ui.payment.history.recycler.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import de.culture4life.luca.databinding.ItemPaymentsLoadingBinding
import de.culture4life.luca.ui.payment.history.recycler.viewholder.PaymentsLoadingViewHolder

class PaymentsRecyclerLoadStateAdapter(private val retry: () -> Unit) : LoadStateAdapter<PaymentsLoadingViewHolder>() {

    override fun onBindViewHolder(holder: PaymentsLoadingViewHolder, loadState: LoadState) {
        return holder.bind(loadState)
    }

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): PaymentsLoadingViewHolder {
        return PaymentsLoadingViewHolder(
            ItemPaymentsLoadingBinding.inflate(LayoutInflater.from(parent.context), parent, false),
            retry
        )
    }
}
