package de.culture4life.luca.util

import android.content.Context
import android.content.DialogInterface
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import de.culture4life.luca.R
import de.culture4life.luca.ui.ViewError
import de.culture4life.luca.ui.dialog.BaseDialogFragment

object ViewErrorUtil {

    abstract class ErrorDialogCallback {
        open fun onPositiveButton() {}
        open fun onDismiss() {}
    }

    @JvmStatic
    fun showErrorAsDialog(context: Context, error: ViewError, callback: ErrorDialogCallback) {
        val builder = MaterialAlertDialogBuilder(context).setTitle(error.title)
            .setMessage(error.description)
            .apply {
                if (error.isCancelable) {
                    setNegativeButton(R.string.action_cancel) { dialog, _ -> dialog.cancel() }
                } else {
                    setCancelable(false)
                }

                if (error.isResolvable) {
                    setPositiveButton(error.resolveLabel) { _, _ -> callback.onPositiveButton() }
                } else {
                    setPositiveButton(R.string.action_ok) { _, _ -> }
                }
            }

        BaseDialogFragment(builder).apply {
            isCancelable = error.isCancelable
            onDismissListener = DialogInterface.OnDismissListener { callback.onDismiss() }
            show()
        }
    }

}
