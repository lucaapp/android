package de.culture4life.luca.ui.idnow

import androidx.viewbinding.ViewBinding
import de.culture4life.luca.databinding.FragmentLucaIdEnrollmentTokenBinding
import de.culture4life.luca.idnow.IdNowManager
import de.culture4life.luca.ui.BaseFragment

class LucaIdEnrollmentTokenFragment : BaseFragment<LucaIdEnrollmentTokenViewModel>() {

    private lateinit var binding: FragmentLucaIdEnrollmentTokenBinding

    override fun getViewBinding(): ViewBinding {
        binding = FragmentLucaIdEnrollmentTokenBinding.inflate(layoutInflater)
        return binding
    }

    override fun getViewModelClass() = LucaIdEnrollmentTokenViewModel::class.java

    override fun initializeViews() {
        super.initializeViews()
        observe(viewModel.enrollmentToken) {
            binding.copyTokenView.setToken(it)
        }
        binding.actionButton.setOnClickListener {
            startActivity(IdNowManager.createIdNowIntent(requireContext(), viewModel.enrollmentToken.value ?: return@setOnClickListener))
        }
    }
}
