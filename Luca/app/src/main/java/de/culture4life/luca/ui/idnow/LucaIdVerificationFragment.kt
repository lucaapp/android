package de.culture4life.luca.ui.idnow

import androidx.viewbinding.ViewBinding
import de.culture4life.luca.databinding.FragmentLucaIdVerificationBinding
import de.culture4life.luca.ui.BaseFragment

class LucaIdVerificationFragment : BaseFragment<LucaIdVerificationViewModel>() {

    private lateinit var binding: FragmentLucaIdVerificationBinding

    override fun getViewBinding(): ViewBinding {
        binding = FragmentLucaIdVerificationBinding.inflate(layoutInflater)
        return binding
    }

    override fun getViewModelClass() = LucaIdVerificationViewModel::class.java

    override fun initializeViews() {
        super.initializeViews()
        observe(viewModel.revocationCode) {
            binding.copyTokenView.setToken(it)
        }
    }
}
