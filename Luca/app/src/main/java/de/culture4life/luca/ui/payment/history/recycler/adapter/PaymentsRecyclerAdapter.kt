package de.culture4life.luca.ui.payment.history.recycler.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import de.culture4life.luca.databinding.ItemPaymentBinding
import de.culture4life.luca.payment.PaymentData
import de.culture4life.luca.ui.payment.history.recycler.viewholder.PaymentsViewHolder

class PaymentsRecyclerAdapter(
    diffCallback: DiffUtil.ItemCallback<PaymentData>,
    private val onPaymentClicked: (PaymentData) -> Unit
) : PagingDataAdapter<PaymentData, PaymentsViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return PaymentsViewHolder(ItemPaymentBinding.inflate(layoutInflater, parent, false), onPaymentClicked)
    }

    override fun onBindViewHolder(holder: PaymentsViewHolder, position: Int) {
        holder.bind(getItem(position) ?: return)
    }
}
