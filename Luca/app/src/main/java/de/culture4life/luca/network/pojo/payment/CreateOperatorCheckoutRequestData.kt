package de.culture4life.luca.network.pojo.payment

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class CreateOperatorCheckoutRequestData(

    @SerializedName("table")
    val table: String? = null,

    @SerializedName("amount")
    val partialAmount: BigDecimal? = null,

    @SerializedName("tipAmount")
    val tipAmount: BigDecimal,

    @SerializedName("waiter")
    val waiter: String? = null,

    @SerializedName("referer")
    val referer: String = "luca/android"
)
