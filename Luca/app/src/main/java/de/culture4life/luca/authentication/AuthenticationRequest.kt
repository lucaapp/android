package de.culture4life.luca.authentication

import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import de.culture4life.luca.authentication.AuthenticationRequest.Companion.VALIDITY_DURATION_NONE
import de.culture4life.luca.util.TimeUtil

data class AuthenticationRequest(

    val id: String,
    val callback: AuthenticationCallback,
    val promptTitle: String? = null,
    val promptSubtitle: String? = null,
    val promptDescription: String? = null,
    val timestamp: Long = TimeUtil.getCurrentMillis(),

    /**
     * Duration in milliseconds or [VALIDITY_DURATION_NONE] if user authentication must take place every time.
     */
    val validityDuration: Long = VALIDITY_DURATION_NONE,

    /**
     * The non-biometric credential used to secure the device (i.e. PIN, pattern, or password).
     */
    val allowDeviceCredential: Boolean = true,

    /**
     * Any biometric (e.g. fingerprint, iris, or face) on the device that meets or exceeds
     * the requirements for Class 2 (formerly Weak), as defined by the Android CDD.
     */
    val allowWeakBiometrics: Boolean = true,

    val cryptoObject: BiometricPrompt.CryptoObject? = null

) {

    val allowedAuthenticators: Int
        get() {
            var allowedAuthenticators = BiometricManager.Authenticators.BIOMETRIC_STRONG
            if (allowDeviceCredential) {
                allowedAuthenticators = allowedAuthenticators or BiometricManager.Authenticators.DEVICE_CREDENTIAL
            }
            if (allowWeakBiometrics) {
                allowedAuthenticators = allowedAuthenticators or BiometricManager.Authenticators.BIOMETRIC_WEAK
            }
            return allowedAuthenticators
        }

    /**
     * Checks if an authentication result is sufficient to treat the current request as fulfilled.
     */
    fun isFulfilledByResult(result: AuthenticationResult): Boolean {
        return when {
            !result.success -> false
            result.timestamp < timestamp -> false
            // TODO: check used authenticators vs allowed authenticators
            else -> true
        }
    }

    /**
     * Checks if a previously requested authentication result is sufficient to treat the current request as fulfilled.
     */
    fun isFulfilledByPreviousRequestResult(previousRequest: AuthenticationRequest, previousResult: AuthenticationResult): Boolean {
        return when {
            !previousRequest.isFulfilledByResult(previousResult) -> false // previous authentication failed
            previousRequest.validityDuration == VALIDITY_DURATION_NONE -> false // previous authentication should not be re-used
            previousRequest.id != id -> false // user authenticated, but with a different intent
            previousResult.timestamp + previousRequest.validityDuration < timestamp -> false // previous authentication expired
            previousRequest.allowDeviceCredential != allowDeviceCredential -> false // TODO: should be no issue if any biometric has been used
            previousRequest.allowWeakBiometrics != allowWeakBiometrics -> false // TODO: should be no issue if strong biometric has been used
            else -> true
        }
    }

    companion object {
        const val VALIDITY_DURATION_NONE = 0L
    }
}
