package de.culture4life.luca.ui.account.lucapay

import android.app.Application
import androidx.lifecycle.MutableLiveData
import de.culture4life.luca.R
import de.culture4life.luca.consent.MissingConsentException
import de.culture4life.luca.ui.BaseViewModel
import de.culture4life.luca.util.isCause
import io.reactivex.rxjava3.core.Completable
import timber.log.Timber

class LucaPayPreferencesViewModel(application: Application) : BaseViewModel(application) {

    private val paymentManager = this.application.paymentManager

    val lucaPayStatus: MutableLiveData<Boolean> = MutableLiveData(false)

    override fun initialize(): Completable {
        return super.initialize()
            .andThen(paymentManager.initialize(application))
            .andThen(updateInitialStatus())
    }

    private fun updateInitialStatus(): Completable {
        return paymentManager.isSignedUp()
            .flatMapCompletable { updateInstantly(lucaPayStatus, it) }
    }

    fun onSwitchToggled(enabled: Boolean) {
        if (enabled == lucaPayStatus.value) {
            return
        }
        invoke(
            Completable.defer { if (enabled) enable() else disable() }
                .andThen(update(lucaPayStatus, enabled))
                .doOnSubscribe { updateAsSideEffect(isLoading, true) }
                .doFinally { updateAsSideEffect(isLoading, false) }
                .doOnError {
                    updateAsSideEffect(lucaPayStatus, !enabled)
                    Timber.w("Unable to toggle payment: $it")
                    val error = createErrorBuilder(it)
                        .withResolveAction(Completable.fromAction { onSwitchToggled(enabled) })
                        .withResolveLabel(R.string.action_retry)
                        .removeWhenShown()
                    if (!it.isCause(MissingConsentException::class.java)) {
                        error.withTitle(R.string.error_request_failed_title)
                    }
                    addError(error.build())
                }
        ).subscribe()
    }

    private fun enable(): Completable {
        return paymentManager.requestConsentIfRequiredAndSignUp()
    }

    private fun disable(): Completable {
        return paymentManager.deleteConsumer(true)
    }
}
