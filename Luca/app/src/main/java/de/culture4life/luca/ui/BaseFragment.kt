package de.culture4life.luca.ui

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.SpannedString
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts.*
import androidx.annotation.CallSuper
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import androidx.navigation.NavAction
import androidx.navigation.NavController
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.FragmentNavigator
import androidx.viewbinding.ViewBinding
import com.google.android.material.snackbar.Snackbar
import com.tbruyelle.rxpermissions3.Permission
import com.tbruyelle.rxpermissions3.RxPermissions
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.R
import de.culture4life.luca.ui.dialog.BaseDialogFragment
import de.culture4life.luca.util.InitializationIdlingResource
import de.culture4life.luca.util.ViewErrorUtil
import de.culture4life.luca.util.retryWhenWithDelay
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.PublishSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit

abstract class BaseFragment<ViewModelType : BaseViewModel> : Fragment() {

    lateinit var viewModel: ViewModelType
        protected set

    protected val application: LucaApplication
        get() = requireActivity().application as LucaApplication

    protected var initialized = false
    protected var rxPermissions: RxPermissions? = null
    protected lateinit var viewDisposable: CompositeDisposable

    private val TAG_RXPERMISSIONS = RxPermissions::class.java.simpleName
    private val activityResults = PublishSubject.create<ActivityResult>()

    private val isCurrentDestination: Boolean
        get() {
            val currentDestination = requireNavigationController().currentDestination as FragmentNavigator.Destination
            return javaClass.name == currentDestination.className
        }

    private var activityResult = registerForActivityResult(StartActivityForResult()) { emitActivityResult(it) }
    private var navigationController: NavController? = null
    private var errorSnackbar: Snackbar? = null
    private var rxPermissionsFragment: Fragment? = null

    @CallSuper
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        viewDisposable = CompositeDisposable()
        val viewBindingRoot = getViewBinding()?.root
        return when {
            viewBindingRoot != null -> viewBindingRoot
            layoutResource != -1 -> inflater.inflate(layoutResource, container, false)
            else -> throw IllegalStateException("Fragment needs to override either getViewBinding() or getLayoutResource()")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rxPermissions = RxPermissions(this)

        try {
            navigationController = findNavController(view)
        } catch (e: Exception) {
            Timber.w("No navigation controller available")
        }

        initializeViewModel()
            .observeOn(AndroidSchedulers.mainThread())
            .doOnComplete {
                initializeViews()
                initialized = true
            }
            .subscribe(
                { Timber.d("Initialized $this with $viewModel") },
                { Timber.e(it, "Unable to initialize $this with $viewModel: $it") }
            )
    }

    private fun emitActivityResult(activityResult: ActivityResult) {
        viewDisposable.add(
            Completable.fromAction { activityResults.onNext(activityResult) }
                .subscribeOn(Schedulers.io())
                .subscribe()
        )
    }

    @CallSuper
    override fun onStart() {
        super.onStart()
        viewDisposable = CompositeDisposable().apply {
            add(
                waitUntilInitializationCompleted().andThen(viewModel.processArguments(arguments))
                    .andThen(viewModel.keepDataUpdated())
                    .doOnSubscribe { Timber.d("Keeping data updated for $this") }
                    .doOnError { Timber.w(it, "Unable to keep data updated for $this") }
                    .retryWhenWithDelay(TimeUnit.SECONDS.toMillis(1))
                    .doFinally { Timber.d("Stopping to keep data updated for $this") }
                    .subscribe()
            )
        }
    }

    @CallSuper
    override fun onStop() {
        viewDisposable.dispose()
        errorSnackbar?.run { if (isShown) dismiss() }
        super.onStop()
    }

    override fun onDestroyView() {
        rxPermissionsFragment = null
        super.onDestroyView()
    }

    /**
     * Overwrite this to return a ViewBinding instead of a layout resource.
     *
     * @return ViewBinding instance
     */
    protected open fun getViewBinding(): ViewBinding? = null

    protected abstract fun getViewModelClass(): Class<ViewModelType>

    @LayoutRes
    protected val layoutResource: Int = -1

    protected open fun getViewModelStoreOwner(): ViewModelStoreOwner = this

    protected fun waitUntilInitializationCompleted(): Completable {
        return Observable.interval(0, 50, TimeUnit.MILLISECONDS)
            .filter { initialized }
            .firstOrError()
            .ignoreElement()
            .doOnSubscribe { InitializationIdlingResource.increment() }
            .doFinally { InitializationIdlingResource.decrement() }
    }

    @CallSuper
    protected open fun initializeViewModel(): Completable {
        return Single.fromCallable { ViewModelProvider(getViewModelStoreOwner()).get(getViewModelClass()) }
            .doOnSuccess { createdViewModel ->
                viewModel = createdViewModel
                viewModel.navigationController = navigationController
            }
            .observeOn(Schedulers.io())
            .flatMap { preInitializeViewModel(viewModel).andThen(Single.just(viewModel)) }
            .flatMapCompletable { it.initialize() }
    }

    /**
     * After ViewModel is created and before initialize is called.
     */
    @CallSuper
    protected open fun preInitializeViewModel(viewModel: ViewModelType): Completable = Completable.complete()

    @CallSuper
    protected open fun initializeViews() {
        setupBackButton()
        observeErrors()
        observeRequiredPermissions()
        observeDialogRequests()
    }

    private fun setupBackButton() {
        requireView().findViewById<ImageView>(R.id.actionBarBackButtonImageView)?.setOnClickListener {
            requireNavigationController().popBackStack()
        }
    }

    protected open fun onMenuItemClick(item: MenuItem): Boolean {
        Timber.w("Unknown menu item selected: ${item.title}")
        return false
    }

    protected fun <ValueType> observe(liveData: LiveData<ValueType>, observer: Observer<ValueType>) {
        liveData.observe(viewLifecycleOwner, observer)
    }

    private fun observeRequiredPermissions() {
        observe(viewModel.requiredPermissions) { permissionsViewEvent ->
            val permissions = permissionsViewEvent.value
            if (permissionsViewEvent.isHandled || permissions.isEmpty()) {
                return@observe
            }
            permissionsViewEvent.isHandled = true

            // add RxPermissionsFragment back to the backstack before using it in rxPermissions.requestEach
            addRxPermissionsFragmentIfRequired()
            rxPermissions!!.requestEach(*permissions.toTypedArray())
                .doOnSubscribe { Timber.d("Requesting required permissions: $permissions") }
                .doOnError { Timber.e(it, "Unable to request permissions: $permissions") }
                .onErrorComplete()
                .doFinally(this::removeRxPermissionsFragmentIfRequired) // permission request is done, so RxPermissionsFragment can be removed
                .subscribe(this::onPermissionResult)
        }
    }

    protected open fun onPermissionResult(permission: Permission) {
        Timber.v("Permission result: $permission")
        viewModel.onPermissionResult(permission)
    }

    private fun observeErrors() {
        observe(viewModel.errors) { errors ->
            if (errors.isEmpty()) {
                indicateNoErrors()
            } else {
                indicateErrors(errors)
            }
        }
    }

    private fun indicateNoErrors() {
        errorSnackbar?.dismiss()
    }

    private fun indicateErrors(errors: Set<ViewError>) {
        Timber.d("indicateErrors() called on $this with: errors = [$errors]")
        errors.forEach { showErrorAsDialog(it) }
    }

    protected fun showErrorAsToast(error: ViewError) {
        if (context == null) {
            return
        }
        Toast.makeText(context, error.title, Toast.LENGTH_LONG).show()
        viewModel.onErrorShown(error)
        viewModel.onErrorDismissed(error)
    }

    protected fun showErrorAsSnackbar(error: ViewError) {
        if (view == null) {
            return
        }
        errorSnackbar?.dismiss()
        val duration = if (error.isResolvable) Snackbar.LENGTH_INDEFINITE else Snackbar.LENGTH_LONG
        errorSnackbar = Snackbar.make(requireView(), error.title, duration).apply {
            addCallback(object : Snackbar.Callback() {
                override fun onShown(snackbar: Snackbar) {
                    viewModel.onErrorShown(error)
                }

                override fun onDismissed(snackbar: Snackbar, event: Int) {
                    viewModel.onErrorDismissed(error)
                }
            })
            if (error.isResolvable) {
                setAction(error.resolveLabel) {
                    viewDisposable.add(
                        error.resolveAction!!
                            .subscribe(
                                { Timber.d("Error resolved") },
                                { Timber.w("Unable to resolve error: $it") }
                            )
                    )
                }
            }
            show()
        }
    }

    private fun showErrorAsDialog(error: ViewError) {
        if (view == null) {
            return
        }

        ViewErrorUtil.showErrorAsDialog(
            requireContext(),
            error,
            object : ViewErrorUtil.ErrorDialogCallback() {
                override fun onDismiss() {
                    viewModel.onErrorDismissed(error)
                }

                override fun onPositiveButton() {
                    viewDisposable.add(
                        error.resolveAction!!
                            .subscribe(
                                { Timber.d("Error resolved") },
                                { Timber.w("Unable to resolve error: $it") }
                            )
                    )
                }
            }
        )
        viewModel.onErrorShown(error)
    }

    protected fun safeNavigateFromNavController(destination: Uri?) {
        if (isCurrentDestination && requireNavigationController().graph.hasDeepLink(destination!!)) {
            requireNavigationController().navigate(destination)
        }
    }

    @JvmOverloads
    protected fun safeNavigateFromNavController(
        @IdRes
        destination: Int,
        bundle: Bundle? = null
    ) {
        if (isCurrentDestination && getNavAction(destination) != null) {
            requireNavigationController().navigate(destination, bundle)
        }
    }

    private fun getNavAction(
        @IdRes
        destination: Int
    ): NavAction? {
        val actionFromCurrent = requireNavigationController().currentDestination?.getAction(destination)
        return actionFromCurrent ?: requireNavigationController().graph.getAction(destination)
    }

    protected fun getFileExportUri(fileName: String): Single<Uri> {
        return Observable.defer {
            val createFileIntent = CreateDocument().createIntent(requireContext(), fileName).apply {
                addCategory(Intent.CATEGORY_OPENABLE)
                type = "text/plain"
            }
            activityResult.launch(createFileIntent)
            activityResults
        }
            .firstOrError()
            .filter { it.resultCode == Activity.RESULT_OK }
            .map { it.data!!.data!! }
            .switchIfEmpty(Single.error(UserCancelledException()))
    }

    protected fun getFileImportUri(mimeTypes: Array<String?>): Single<Uri> {
        return Observable.defer {
            val createFileIntent = OpenDocument().createIntent(requireContext(), mimeTypes)
            activityResult.launch(createFileIntent)
            activityResults
        }
            .firstOrError()
            .filter { it.resultCode == Activity.RESULT_OK }
            .map { it.data!!.data!! }
            .switchIfEmpty(Single.error(UserCancelledException()))
    }

    fun getFormattedString(
        @StringRes
        id: Int,
        vararg args: Any
    ): CharSequence {
        val mappedArgs = args.map { if (it is String) TextUtils.htmlEncode(it) else it }
            .toTypedArray()
        val html = HtmlCompat.toHtml(SpannedString(getText(id)), HtmlCompat.TO_HTML_PARAGRAPH_LINES_INDIVIDUAL)
        return HtmlCompat.fromHtml(String.format(html, *mappedArgs), HtmlCompat.FROM_HTML_MODE_COMPACT)
    }

    override fun toString(): String = this.javaClass.simpleName

    private fun observeDialogRequests() {
        observe(viewModel.dialogRequest) { dialogRequest ->
            if (dialogRequest.isNotHandled) {
                BaseDialogFragment(requireContext(), dialogRequest.valueAndMarkAsHandled).show()
            }
        }
    }

    protected fun requireNavigationController(): NavController = checkNotNull(navigationController) { "Navigation controller was null!" }

    protected fun addRxPermissionsFragmentIfRequired() {
        // 19.04.22 Workaround to avoid retained fragment LUCA-5526
        if (rxPermissionsFragment != null && childFragmentManager.findFragmentByTag(TAG_RXPERMISSIONS) == null) {
            childFragmentManager.beginTransaction().add(rxPermissionsFragment!!, TAG_RXPERMISSIONS).commitNow()
        }
    }

    protected fun removeRxPermissionsFragmentIfRequired() {
        // 19.04.22 Workaround to avoid retained fragment LUCA-5526
        val rxPermissionsFragment = childFragmentManager.findFragmentByTag(TAG_RXPERMISSIONS)
        if (rxPermissionsFragment != null) {
            this.rxPermissionsFragment = rxPermissionsFragment
            childFragmentManager.beginTransaction().remove(rxPermissionsFragment).commitNow()
        }
    }
}
