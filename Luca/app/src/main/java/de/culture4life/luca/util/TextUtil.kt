package de.culture4life.luca.util

import android.content.Context
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import de.culture4life.luca.ui.BaseViewModel

object TextUtil {

    /**
     * Get string with custom placeholder style.
     *
     * Origin Android style (e.g %1$s) isn't easy to maintain for non developer.
     *
     * Sample:
     *      like = "I like [like]!"
     *      getTemplateString(context, R.string.like, "like" to "android")
     *
     * @param context required to access resources
     * @param id string with placeholder
     * @param replacements "myPlaceholder" to "myValue"
     */
    fun getPlaceholderString(
        context: Context,
        @StringRes
        id: Int,
        vararg replacements: Pair<String, String>
    ): String {
        var string = context.getString(id)
        replacements.forEach {
            val placeholderOldStyle = "<${it.first}>" // TODO remove when all strings are converted to new placeholder style
            val placeholder = "[${it.first}]"
            require(string.contains(placeholder) || string.contains(placeholderOldStyle)) {
                "Missing placeholder $placeholder in > $string"
            }
            string = string.replace(placeholder, it.second)
            string = string.replace(placeholderOldStyle, it.second)
        }
        return string
    }
}

fun Fragment.getPlaceholderString(
    @StringRes
    id: Int,
    vararg replacements: Pair<String, String>
): String {
    return TextUtil.getPlaceholderString(requireContext(), id, *replacements)
}

fun BaseViewModel.getPlaceholderString(
    @StringRes
    id: Int,
    vararg replacements: Pair<String, String>
): String {
    return TextUtil.getPlaceholderString(getApplication(), id, *replacements)
}
