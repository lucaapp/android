package de.culture4life.luca.ui.payment.children

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.MutableLiveData
import de.culture4life.luca.R
import de.culture4life.luca.payment.PaymentAmounts
import de.culture4life.luca.payment.PaymentManager
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowChildViewModel
import de.culture4life.luca.ui.payment.PaymentFlowViewModel
import de.culture4life.luca.util.*
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.schedulers.Schedulers
import timber.log.Timber

class PaymentPartialAmountViewModel(application: Application) : BaseFlowChildViewModel<PaymentFlowViewModel>(application) {

    private val paymentManager: PaymentManager = this.application.paymentManager
    private var minimumAmount = MINIMUM_AMOUNT
    private var maximumAmount = MAXIMUM_AMOUNT

    val locationName: MutableLiveData<String> = MutableLiveData()
    val paymentAmounts: MutableLiveData<PaymentAmounts> by lazy { sharedViewModel!!.paymentAmounts }
    val warningText: MutableLiveData<String?> = MutableLiveData()
    val showKeyboard: MutableLiveData<Boolean> = MutableLiveData()

    override fun initialize(): Completable {
        return super.initialize()
            .andThen(paymentManager.initialize(application))
            .andThen(invoke(updateAmount(INITIAL_AMOUNT)))
            .andThen(invoke(updatePaymentLimits()))
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun updateAmount(amount: Int): Completable {
        return updateIfRequired(paymentAmounts, paymentAmounts.value!!.copy(partialPaymentAmount = amount))

    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun updatePaymentLimits(): Completable {
        return paymentManager.fetchPaymentLimits()
            .doOnSuccess {
                minimumAmount = it.minimumInvoiceAmount.toCurrencyAmountNumber()
                maximumAmount = it.maximumInvoiceAmount.toCurrencyAmountNumber()
            }
            .ignoreElement()
            .retryWhenWithDelay(5, 1, Schedulers.io()) { ThrowableUtil.isNetworkError(it) }
    }

    fun onAmountChanged(amountInput: String) {
        val amount = amountInput.toCurrencyAmountNumber()
        Timber.v("Amount changed: Input: $amountInput, Parsed: $amount")
        updateAsSideEffect(paymentAmounts, paymentAmounts.value!!.copy(partialPaymentAmount = amount))
        if (getAmountErrorIfAvailable(amount) == null) {
            updateAsSideEffectIfRequired(warningText, null)
        }
    }

    fun onAmountConfirmed() {
        val amount = paymentAmounts.value!!
        Timber.i("Confirmed amount: $amount")

        if (amount.isClosingPayment) {
            // amount limits don't apply if it's closing the payment request
            updateAsSideEffectIfRequired(warningText, null)
        } else {
            val error = getAmountErrorIfAvailable(amount.partialPaymentAmount)
            updateAsSideEffectIfRequired(warningText, error)
            if (error != null) {
                updateAsSideEffect(showKeyboard, true)
                return
            }
        }

        updateAsSideEffect(showKeyboard, false)
        updateAsSideEffectIfRequired(sharedViewModel!!.paymentAmounts, amount)

        sharedViewModel!!.navigateToNext()
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun getAmountErrorIfAvailable(amount: Int): String? {
        val openAmount = paymentAmounts.value!!.openAmount!!
        if (amount == openAmount) {
            return null
        }
        return when {
            amount > openAmount -> application.getString(R.string.error_split_exceeds_amount)
            amount < minimumAmount -> getPlaceholderString(
                R.string.pay_min_amount,
                "minAmount" to minimumAmount.toCurrencyAmountString()
            )
            amount > maximumAmount -> getPlaceholderString(
                R.string.pay_max_amount,
                "maxAmount" to maximumAmount.toCurrencyAmountString()
            )
            else -> null
        }
    }

    companion object {
        const val INITIAL_AMOUNT = 0
        const val MINIMUM_AMOUNT = 100 // 1 €
        const val MAXIMUM_AMOUNT = 10000000 // 100k €
    }
}
