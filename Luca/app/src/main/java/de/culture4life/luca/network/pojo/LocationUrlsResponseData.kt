package de.culture4life.luca.network.pojo

import com.google.gson.annotations.SerializedName

data class LocationUrlsResponseData(
    @SerializedName("menu")
    val menu: String? = null,
    @SerializedName("schedule")
    val program: String? = null,
    @SerializedName("map")
    val map: String? = null,
    @SerializedName("website")
    val website: String? = null,
    @SerializedName("general")
    val general: String? = null
)
