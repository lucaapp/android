package de.culture4life.luca.crypto

import android.annotation.SuppressLint
import android.content.Context
import com.nexenio.rxkeystore.RxKeyStore
import com.nexenio.rxkeystore.provider.cipher.asymmetric.ec.EcCipherProvider
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey
import org.bouncycastle.jcajce.provider.asymmetric.util.EC5Util
import org.bouncycastle.jce.ECNamedCurveTable
import org.bouncycastle.jce.ECPointUtil
import org.bouncycastle.jce.provider.BouncyCastleProvider
import java.math.BigInteger
import java.security.KeyFactory
import java.security.interfaces.ECPrivateKey
import java.security.interfaces.ECPublicKey
import java.security.spec.AlgorithmParameterSpec
import java.security.spec.ECGenParameterSpec
import java.security.spec.ECPrivateKeySpec
import java.security.spec.ECPublicKeySpec

/**
 * Provides EC cryptography using the secp256r1 curve.
 *
 *
 * Uses Bouncy Castle due to Android limitations.
 */
// TODO: implement in RxKeyStore library
open class AsymmetricCipherProvider(rxKeyStore: RxKeyStore) : EcCipherProvider(rxKeyStore) {

    override fun getKeyAlgorithmParameterSpec(alias: String, context: Context): Single<AlgorithmParameterSpec> {
        return Single.fromCallable { ECGenParameterSpec(CURVE_NAME) }
    }

    override fun getBlockModes(): Array<String> = arrayOf(RxKeyStore.BLOCK_MODE_ECB)

    override fun getEncryptionPaddings(): Array<String> = arrayOf()

    override fun getSignaturePaddings(): Array<String> = arrayOf()

    override fun getDigests(): Array<String> = arrayOf(RxKeyStore.DIGEST_SHA256)

    override fun getTransformationAlgorithm(): String = "ECIES"

    override fun getSignatureAlgorithm(): String = SIGNATURE_ALGORITHM

    override fun getKeyAgreementAlgorithm(): String = RxKeyStore.KEY_AGREEMENT_ECDH

    fun deleteKeyStoreEntry(key: String): Completable = rxKeyStore.deleteEntry(key)

    protected fun getCurveName(): String = CURVE_NAME

    companion object {
        const val SIGNATURE_ALGORITHM = "SHA256withECDSA"
        private const val CURVE_NAME = "secp256r1"
        private const val KEY_ALGORITHM = "ECDSA"

        @JvmStatic
        fun encode(publicKey: ECPublicKey): Single<ByteArray> = encode(publicKey, false)

        // ECPublicKey is interpreted as a functional interface and therefore suggested to be moved to the end because it could be a Lambda.
        // Does not really make sense in this case
        @JvmStatic
        @SuppressLint("LambdaLast")
        fun encode(publicKey: ECPublicKey, compressed: Boolean): Single<ByteArray> {
            return Single.fromCallable { BCECPublicKey(publicKey, BouncyCastleProvider.CONFIGURATION) }
                .map(BCECPublicKey::getQ)
                .map { ecPoint -> ecPoint.getEncoded(compressed) }
        }

        @JvmStatic
        fun decodePublicKey(encodedKey: ByteArray): Single<ECPublicKey> {
            return Single.fromCallable {
                val keyFactory = KeyFactory.getInstance(KEY_ALGORITHM, RxKeyStore.PROVIDER_BOUNCY_CASTLE)
                val bcParameterSpec = ECNamedCurveTable.getParameterSpec(CURVE_NAME)
                val bcCurve = bcParameterSpec.curve
                val curve = EC5Util.convertCurve(bcCurve, bcParameterSpec.seed)
                val parameterSpec = EC5Util.convertSpec(curve, bcParameterSpec)
                val point = ECPointUtil.decodePoint(curve, encodedKey)
                val keySpec = ECPublicKeySpec(point, parameterSpec)
                keyFactory.generatePublic(keySpec) as ECPublicKey
            }
        }

        @JvmStatic
        fun decodePrivateKey(encodedKey: ByteArray): Single<ECPrivateKey> {
            return Single.fromCallable {
                val keyFactory = KeyFactory.getInstance(KEY_ALGORITHM, RxKeyStore.PROVIDER_BOUNCY_CASTLE)
                val bcParameterSpec = ECNamedCurveTable.getParameterSpec(CURVE_NAME)
                val bcCurve = bcParameterSpec.curve
                val curve = EC5Util.convertCurve(bcCurve, bcParameterSpec.seed)
                val parameterSpec = EC5Util.convertSpec(curve, bcParameterSpec)
                val s = BigInteger(1, encodedKey)
                val privateKeySpec = ECPrivateKeySpec(s, parameterSpec)
                val privateKey = keyFactory.generatePrivate(privateKeySpec)
                privateKey as ECPrivateKey
            }
        }
    }
}
