package de.culture4life.luca.location

class LocationUnavailableException(message: String) : Exception(message)
