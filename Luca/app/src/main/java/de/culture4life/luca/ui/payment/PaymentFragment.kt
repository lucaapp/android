package de.culture4life.luca.ui.payment

import androidx.core.view.isVisible
import de.culture4life.luca.R
import de.culture4life.luca.databinding.FragmentPaymentBinding
import de.culture4life.luca.ui.BaseFragment

class PaymentFragment : BaseFragment<PaymentViewModel>() {

    private lateinit var binding: FragmentPaymentBinding

    override fun getViewBinding() = FragmentPaymentBinding.inflate(layoutInflater).also { binding = it }
    override fun getViewModelClass() = PaymentViewModel::class.java

    override fun initializeViews() {
        super.initializeViews()
        initializeIntroLayout()
        initializeObservers()
    }

    private fun initializeIntroLayout() {
        binding.paymentIntroActionButton.setOnClickListener { viewModel.onPaymentSignUpRequested() }
    }

    private fun initializeObservers() {
        observe(viewModel.isLoading) {
            binding.loadingIndicator.isVisible = it
            binding.paymentIntroActionButton.isEnabled = !it
        }
        observe(viewModel.isSignedUpForPayment) { isSignedUp ->
            binding.paymentIntroLayout.isVisible = !isSignedUp
            if (isSignedUp) {
                safeNavigateFromNavController(R.id.action_paymentFragment_to_paymentHistoryFragment)
            }
        }
    }
}
