package de.culture4life.luca.ui

import io.reactivex.rxjava3.core.Completable

fun interface BaseQrCodeCallback {
    fun processBarcode(barcodeData: String): Completable
}
