package de.culture4life.luca.util

import android.content.Context
import de.culture4life.luca.util.AssetUtil.getBytes
import de.culture4life.luca.util.AssetUtil.getInputStream
import dgca.verifier.app.decoder.fromBase64
import org.bouncycastle.asn1.ASN1ObjectIdentifier
import org.bouncycastle.asn1.x500.X500Name
import org.bouncycastle.asn1.x500.style.IETFUtils
import java.io.InputStream
import java.security.KeyFactory
import java.security.PrivateKey
import java.security.PublicKey
import java.security.cert.*
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec

object CertificateUtil {

    private const val TYPE_X509 = "X509"
    private const val ALGORITHM_RSA = "RSA"
    private const val ALGORITHM_PKIX = "PKIX"

    @JvmStatic
    fun loadPublicPEMKey(fileName: String, context: Context, algorithm: String = ALGORITHM_RSA): PublicKey {
        val pemFile = String(getBytes(fileName, context))
        val key = pemFile.replace("-----BEGIN PUBLIC KEY-----", "")
            .replace("-----END PUBLIC KEY-----", "")
        val spec = X509EncodedKeySpec(key.fromBase64())
        return KeyFactory.getInstance(algorithm).generatePublic(spec)
    }

    @JvmStatic
    fun loadPrivatePEMKey(fileName: String, context: Context, algorithm: String = ALGORITHM_RSA): PrivateKey {
        val pemFile = String(getBytes(fileName, context))
        val key = pemFile.replace("-----BEGIN PRIVATE KEY-----", "")
            .replace("-----END PRIVATE KEY-----", "")
        val spec = PKCS8EncodedKeySpec(key.fromBase64())
        return KeyFactory.getInstance(algorithm).generatePrivate(spec)
    }

    @JvmStatic
    fun loadCertificate(fileName: String, context: Context): X509Certificate {
        return loadCertificate(getInputStream(fileName, context))
    }

    @JvmStatic
    fun loadCertificate(inputStream: InputStream): X509Certificate {
        val certificateFactory = CertificateFactory.getInstance(TYPE_X509)
        return certificateFactory.generateCertificate(inputStream) as X509Certificate
    }

    fun checkCertificateChain(rootCertificate: X509Certificate, certificateChain: List<X509Certificate>) {
        rootCertificate.checkValidity()
        certificateChain.forEach(X509Certificate::checkValidity)

        val certPathValidator = CertPathValidator.getInstance(ALGORITHM_PKIX)
        val certificateFactory = CertificateFactory.getInstance(TYPE_X509)
        val certPath = certificateFactory.generateCertPath(certificateChain)

        // validate the cert path
        val parameters = PKIXParameters(setOf(TrustAnchor(rootCertificate, null)))
        parameters.isRevocationEnabled = false
        certPathValidator.validate(certPath, parameters)
    }
}

fun X509Certificate.getX500Name(): X500Name {
    return X500Name.getInstance(this.subjectX500Principal.encoded)
}

fun X500Name.getRdnAsString(attributeType: ASN1ObjectIdentifier): String {
    val rdn = this.getRDNs(attributeType).first()
    return IETFUtils.valueToString(rdn.first.value)
}
