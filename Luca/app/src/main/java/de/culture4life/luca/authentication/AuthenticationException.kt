package de.culture4life.luca.authentication

class AuthenticationException(message: String) : Exception(message)
