package de.culture4life.luca.network.pojo.payment

import com.google.gson.annotations.SerializedName

data class PaymentActiveResponseData(

    @SerializedName("uuid")
    val locationId: String,

    @SerializedName("paymentActive")
    val isPaymentActive: Boolean

)
