package de.culture4life.luca.ui.payment.children

import android.view.View
import androidx.core.view.isVisible
import de.culture4life.luca.R
import de.culture4life.luca.databinding.FragmentPaymentTipBinding
import de.culture4life.luca.payment.PaymentAmounts
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowChildFragment
import de.culture4life.luca.ui.payment.PaymentFlowViewModel
import de.culture4life.luca.util.AmountUtil
import de.culture4life.luca.util.getPlaceholderString
import de.culture4life.luca.util.toCurrencyAmountString

class PaymentTipFragment : BaseFlowChildFragment<PaymentTipViewModel, PaymentFlowViewModel>() {

    private lateinit var binding: FragmentPaymentTipBinding
    private val percentageLayoutMap by lazy {
        mapOf(
            0 to binding.tipPercentageLayout00,
            10 to binding.tipPercentageLayout10,
            15 to binding.tipPercentageLayout15,
            20 to binding.tipPercentageLayout20,
        )
    }

    override fun getViewModelClass() = PaymentTipViewModel::class.java
    override fun getSharedViewModelClass() = PaymentFlowViewModel::class.java
    override fun getViewBinding() = FragmentPaymentTipBinding.inflate(layoutInflater).also { binding = it }

    override fun initializeViews() {
        super.initializeViews()
        observe(viewModel.paymentAmount) {
            updateTipAmounts(it)
            updateTipSelection(it.tipPercentage)
            updateTotalAmountPreview(it.totalAmount)
            updateDiscountCampaignIfAvailable(it)
        }
        initializeTipChoices()
        binding.primaryActionButton.setOnClickListener { viewModel.onTipConfirmed() }
    }

    private fun updateDiscountCampaignIfAvailable(paymentAmounts: PaymentAmounts) {
        with(binding) {
            if (paymentAmounts.discountPercentage > 0) {
                invoiceLabelTextView.isVisible = true
                invoiceAmountTextView.isVisible = true
                invoiceAmountTextView.text = if (paymentAmounts.isPartialPayment) {
                    getStringEuroAmount(paymentAmounts.remainingPartialPaymentAmount)
                } else {
                    getStringEuroAmount(paymentAmounts.remainingInvoiceAmount)
                }
                originAmountTextView.isVisible = true
                originAmountTextView.text = if (paymentAmounts.isPartialPayment) {
                    getStringEuroAmount(paymentAmounts.partialPaymentAmount)
                } else {
                    getStringEuroAmount(paymentAmounts.invoiceAmount)
                }
                campaignInfoImageView.isVisible = true
                campaignInfoTextView.isVisible = true
                campaignInfoTextView.text = getPlaceholderString(
                    R.string.pay_details_discount_hint,
                    "discountPercentage" to "${paymentAmounts.discountPercentage}",
                    "maximumDiscountAmount" to paymentAmounts.maximumDiscountAmount.toCurrencyAmountString()
                )
            } else {
                invoiceLabelTextView.visibility = View.GONE
                invoiceAmountTextView.visibility = View.GONE
                originAmountTextView.visibility = View.GONE
                campaignInfoImageView.visibility = View.GONE
                campaignInfoTextView.visibility = View.GONE
            }
        }
    }

    private fun updateTotalAmountPreview(amount: Int) {
        with(binding) {
            val amountString = getStringEuroAmount(amount)
            totalAmountTextView.text = amountString
            primaryActionButton.text = getPlaceholderString(R.string.pay_continue_button, "amount" to amountString)
        }
    }

    private fun updateTipAmounts(paymentAmounts: PaymentAmounts) {
        val paymentAmount = if (paymentAmounts.isPartialPayment) {
            paymentAmounts.partialPaymentAmount
        } else {
            paymentAmounts.invoiceAmount
        }
        percentageLayoutMap.forEach { entry ->
            val tipAmount = AmountUtil.calculateTip(amountInCents = paymentAmount, tipPercentage = entry.key)
            entry.value.amountTextView.text = getStringEuroAmount(tipAmount)
        }
    }

    private fun initializeTipChoices() {
        percentageLayoutMap.forEach { entry ->
            entry.value.root.setOnClickListener { viewModel.onTipChanged(entry.key) }
            entry.value.percentageTextView.text = getStringPercentage(entry.key)
        }
    }

    private fun updateTipSelection(selected: Int) {
        percentageLayoutMap.forEach {
            it.value.root.isSelected = it.key == selected
        }
    }

    private fun getStringEuroAmount(amount: Int): String {
        val amountString = amount.toCurrencyAmountString()
        return getString(R.string.euro_amount, amountString)
    }

    private fun getStringPercentage(percentage: Int): String {
        return getString(R.string.common_percentage, percentage)
    }

    companion object {
        fun newInstance() = PaymentTipFragment()
    }
}
