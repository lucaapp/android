package de.culture4life.luca.ui.payment.history

import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import de.culture4life.luca.R
import de.culture4life.luca.databinding.FragmentPaymentHistoryDetailBinding
import de.culture4life.luca.payment.PaymentData
import de.culture4life.luca.ui.BaseFragment
import de.culture4life.luca.util.NumberUtil
import de.culture4life.luca.util.getReadableDate
import de.culture4life.luca.util.getReadableTime

class PaymentHistoryDetailFragment : BaseFragment<PaymentHistoryDetailViewModel>() {

    private lateinit var binding: FragmentPaymentHistoryDetailBinding
    override fun getViewModelClass() = PaymentHistoryDetailViewModel::class.java
    override fun getViewBinding() = FragmentPaymentHistoryDetailBinding.inflate(layoutInflater).also { this.binding = it }

    override fun initializeViews() {
        super.initializeViews()

        observe(viewModel.paymentData) { paymentData ->
            with(binding) {
                titleTextView.text = paymentData.locationName
                dateValueTextView.text = requireContext().getReadableDate(paymentData.timestamp)
                timeValueTextView.text = getString(R.string.history_time, requireContext().getReadableTime(paymentData.timestamp))
                amountValueTextView.text = getAmountInEuro(paymentData.totalAmount)
                if (paymentData.isDiscounted) {
                    originInvoiceAmountValueTextView.text = getAmountInEuro(paymentData.originalInvoiceAmount)
                    originInvoiceAmountValueTextView.isVisible = true
                }
                invoiceAmountValueTextView.text = getAmountInEuro(paymentData.invoiceAmount)
                tipAmountValueTextView.text = getTipOverview(tipAmount = paymentData.tipAmount, tipPercent = paymentData.tipPercent)
                paymentCodeValueTextView.text = paymentData.paymentVerifier
            }
        }

        observe(viewModel.tableName) {
            binding.tableValueTextView.text = it
            binding.tableGroup.isVisible = it != getString(R.string.unknown)
        }

        observe(viewModel.payRaffleParticipation) {
            binding.raffleGroup.isVisible = it
        }

        binding.reportProblemButton.setOnClickListener { viewModel.requestSupportMail() }
    }

    private fun getAmountInEuro(amount: Int) = getString(R.string.euro_amount, NumberUtil.toCurrencyAmountString(amount))

    private fun getTipOverview(tipAmount: Int, tipPercent: Int): String {
        return if (tipAmount == 0) {
            getAmountInEuro(0)
        } else {
            return getString(R.string.pay_details_tip_amount_value, tipPercent, NumberUtil.toCurrencyAmountString(tipAmount))
        }
    }

    companion object {
        const val ARGUMENT_PAYMENT_DATA_KEY = "payment_data"

        fun createArguments(payment: PaymentData) = bundleOf(Pair(ARGUMENT_PAYMENT_DATA_KEY, payment))
    }
}
