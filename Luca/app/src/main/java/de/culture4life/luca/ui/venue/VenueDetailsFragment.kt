package de.culture4life.luca.ui.venue

import android.graphics.Paint
import android.os.Bundle
import androidx.core.content.res.ResourcesCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.viewbinding.ViewBinding
import de.culture4life.luca.R
import de.culture4life.luca.databinding.FragmentVenueDetailsBinding
import de.culture4life.luca.network.pojo.LocationResponseData
import de.culture4life.luca.payment.PaymentAmounts
import de.culture4life.luca.ui.BaseFragment
import de.culture4life.luca.ui.checkin.LocationUrl
import de.culture4life.luca.ui.compound.VenueDetailsInformationItemView
import de.culture4life.luca.ui.payment.PaymentFlowBottomSheetFragment
import de.culture4life.luca.ui.payment.PaymentFlowBottomSheetFragment.Companion.NEW_PAYMENT_REQUEST_KEY
import de.culture4life.luca.ui.payment.PaymentFlowBottomSheetFragment.Companion.NEW_PAYMENT_RESULT_KEY
import de.culture4life.luca.ui.venue.VenueDetailsViewModel.PaymentStatus
import de.culture4life.luca.ui.venue.VenueDetailsViewModel.ViewState.Content
import de.culture4life.luca.ui.venue.VenueDetailsViewModel.ViewState.Empty
import de.culture4life.luca.util.ClipboardUtil
import de.culture4life.luca.util.getPlaceholderString
import de.culture4life.luca.util.toCurrencyAmountNumber
import de.culture4life.luca.util.toCurrencyAmountString
import timber.log.Timber

class VenueDetailsFragment : BaseFragment<VenueDetailsViewModel>() {

    private lateinit var binding: FragmentVenueDetailsBinding
    private lateinit var urlTypeToButtonMap: Map<LocationUrl.UrlType, VenueDetailsInformationItemView>

    override fun getViewBinding(): ViewBinding = FragmentVenueDetailsBinding.inflate(layoutInflater).also { binding = it }
    override fun getViewModelClass() = VenueDetailsViewModel::class.java

    private var paymentFlowFragment: PaymentFlowBottomSheetFragment? = null

    override fun initializeViews() {
        super.initializeViews()
        initializeUrlViews()
        initializeObservers()
        binding.startPaymentButton.setOnClickListener { viewModel.onPaymentStartRequested() }

        childFragmentManager.setFragmentResultListener(NEW_PAYMENT_REQUEST_KEY, viewLifecycleOwner) { _, bundle ->
            if (!bundle.getBoolean(NEW_PAYMENT_RESULT_KEY)) {
                viewModel.paymentCancelled()
            }
        }
    }

    private fun initializeObservers() {
        observe(viewModel.askUrlConsent) {
            if (it.isNotHandled) {
                showOpenUrlConsentBottomSheet(it.valueAndMarkAsHandled)
            }
        }

        observe(viewModel.showStartPaymentButton) { binding.startPaymentButton.isVisible = it }

        observe(viewModel.paymentStatus) {
            when (it) {
                is PaymentStatus.PaymentCancelled -> Timber.d("Payment was cancelled")
                is PaymentStatus.PaymentNotEnabled -> Timber.d("Payment is not enabled!")
                is PaymentStatus.PaymentRequest -> {
                    val openPaymentAmount = it.data
                    if (openPaymentAmount == VenueDetailsViewModel.PAYMENT_AMOUNT_NOT_GIVEN) {
                        binding.paymentGroup.isVisible = false
                        binding.originAmountValueTextView.isVisible = false
                    } else {
                        binding.paymentGroup.isVisible = true
                        binding.amountValueTextView.text = getString(
                            R.string.euro_amount,
                            (openPaymentAmount.remainingOpenAmount ?: openPaymentAmount.remainingInvoiceAmount).toCurrencyAmountString()
                        )
                        updateAmountDiscountState(openPaymentAmount)
                    }
                }
                is PaymentStatus.PaymentStart -> {
                    paymentFlowFragment = PaymentFlowBottomSheetFragment.newInstance(it.data)
                    paymentFlowFragment?.show(childFragmentManager, PaymentFlowBottomSheetFragment.TAG)
                }
                is PaymentStatus.PaymentCompletion -> {
                    val paymentFlowViewModel = paymentFlowFragment?.viewModel
                    if (paymentFlowViewModel != null) {
                        // if we have access to the payment flow, wait until its dismissed first
                        observe(paymentFlowViewModel.bottomSheetDismissed) { checkOut() }
                    } else {
                        checkOut()
                    }
                }
            }
        }
        observe(viewModel.activeCampaign) {
            when (it) {
                is VenueDetailsViewModel.Campaign.DiscountCampaign -> {
                    binding.campaignInfoLayout.isVisible = true
                    binding.campaignInfoTextView.text = getPlaceholderString(
                        R.string.pay_details_discount_hint,
                        "discountPercentage" to "${it.data.discountPercentage}",
                        "maximumDiscountAmount" to it.data.maximumDiscountAmount!!.toCurrencyAmountNumber().toCurrencyAmountString()
                    )
                }
                is VenueDetailsViewModel.Campaign.NoCampaign -> {
                    binding.campaignInfoLayout.isVisible = false
                }
            }
        }
        observe(viewModel.viewState) { viewState ->
            binding.emptyStateGroup.isVisible = viewState is Empty
            binding.loadingIndicator.isVisible = viewState is VenueDetailsViewModel.ViewState.Loading
            binding.contentGroup.isVisible = viewState is Content

            (viewState as? Empty)?.venueDetails?.let { venueDetails ->
                binding.emptyTitleTextView.text = venueDetails.title
            }

            (viewState as? Content)?.let { (venueDetails, tableName) ->
                // Main content
                binding.titleTextView.text = venueDetails.title
                binding.subTitleTextView.text = venueDetails.subtitle
                binding.subTitleTextView.isVisible = !venueDetails.subtitle.isNullOrBlank()
                showUrls(venueDetails.providedUrls)

                // Table
                binding.tableGroup.isVisible = tableName != null
                binding.tableNameValueTextView.text = tableName
            }
        }
    }

    private fun updateAmountDiscountState(openPaymentAmount: PaymentAmounts) {
        if (openPaymentAmount.discountPercentage > 0) {
            binding.amountValueTextView.setTextColor(
                ResourcesCompat.getColor(
                    resources,
                    R.color.payment_promotion,
                    requireContext().theme
                )
            )
            binding.originAmountValueTextView.isVisible = true
            binding.originAmountValueTextView.text = getString(
                R.string.euro_amount,
                openPaymentAmount.invoiceAmount.toCurrencyAmountString()
            )
        } else {
            binding.amountValueTextView.setTextColor(
                ResourcesCompat.getColor(
                    resources,
                    R.color.white,
                    requireContext().theme
                )
            )
            binding.originAmountValueTextView.isVisible = false
        }
    }

    /*
        URLs
     */

    private fun initializeUrlViews() {
        binding.reportMisuseButton.apply {
            paintFlags = paintFlags or Paint.UNDERLINE_TEXT_FLAG
            setOnClickListener { viewModel.reportAbuse(requireActivity()) }
        }
        urlTypeToButtonMap = mapOf(
            LocationUrl.UrlType.MENU to binding.menuButton,
            LocationUrl.UrlType.PROGRAM to binding.programButton,
            LocationUrl.UrlType.MAP to binding.mapButton,
            LocationUrl.UrlType.WEBSITE to binding.websiteButton,
            LocationUrl.UrlType.GENERAL to binding.generalButton
        )
        urlTypeToButtonMap.forEach { (type, itemView) ->
            with(itemView) {
                setOnClickListener { viewModel.openProvidedUrlOrRequestConsent(type) }
                setOnLongClickListener {
                    copyUrlToClipboard(type)
                    true
                }
            }
        }
    }

    private fun showUrls(providedUrls: List<LocationUrl>) {
        urlTypeToButtonMap.forEach { it.value.isVisible = false } // hide all items
        providedUrls.forEach { urlTypeToButtonMap[it.type]?.isVisible = true } // show available items
        binding.reportMisuseButton.isVisible = providedUrls.isNotEmpty()
        binding.informationTextView.isVisible = providedUrls.isNotEmpty()
    }

    private fun copyUrlToClipboard(urlType: LocationUrl.UrlType) {
        val locationName = (viewModel.viewState.value as? Content)?.venueDetails?.locationData?.locationDisplayName
        val readableUrlType = urlTypeToButtonMap[urlType]?.text
        val label = getString(R.string.venue_url_clipboard_label, locationName, readableUrlType)
        val url = viewModel.getProvidedUrl(urlType)
        ClipboardUtil.copy(requireContext(), label, url!!, getString(R.string.venue_url_clipboard_toast))
    }

    private fun showOpenUrlConsentBottomSheet(urlType: LocationUrl.UrlType) {
        val locationName = (viewModel.viewState.value as? Content)?.venueDetails?.locationData?.locationDisplayName
        val readableUrlType = urlTypeToButtonMap[urlType]?.text
        val url = viewModel.getProvidedUrl(urlType)
        VenueUrlConsentBottomSheetFragment.newInstance(locationName, readableUrlType?.toString(), url)
            .show(childFragmentManager, VenueUrlConsentBottomSheetFragment.TAG)
    }

    private fun checkOut() {
        requireNavigationController().popBackStack()
    }

    companion object {
        const val ARGUMENT_LOCATION_DATA_KEY = "location_data"
        const val ARGUMENT_LOCATION_URLS_KEY = "location_urls"
        const val ARGUMENT_ADDITIONAL_DATA_KEY = "additional_data"

        @JvmStatic
        fun createArguments(
            locationData: LocationResponseData,
            locationUrls: List<LocationUrl>,
            additionalData: String
        ): Bundle = bundleOf(
            ARGUMENT_LOCATION_DATA_KEY to locationData,
            ARGUMENT_LOCATION_URLS_KEY to locationUrls,
            ARGUMENT_ADDITIONAL_DATA_KEY to additionalData
        )
    }
}
