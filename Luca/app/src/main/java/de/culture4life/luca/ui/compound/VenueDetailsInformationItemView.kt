package de.culture4life.luca.ui.compound

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.withStyledAttributes
import androidx.core.view.setPadding
import de.culture4life.luca.R
import de.culture4life.luca.databinding.ViewVenueDetailsInformationItemBinding

class VenueDetailsInformationItemView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defaultStyle: Int = 0
) : ConstraintLayout(context, attributeSet, defaultStyle) {

    val binding = ViewVenueDetailsInformationItemBinding.inflate(LayoutInflater.from(context), this)

    var text: String
        get() = binding.textView.text.toString()
        set(value) {
            binding.textView.text = value
        }

    @DrawableRes
    var iconRes: Int = R.drawable.ic_menu_color
        set(value) {
            field = value
            binding.iconImageView.setImageResource(value)
        }

    init {
        isClickable = true
        background = ContextCompat.getDrawable(context, R.drawable.background_account_page_action_item)
        setPadding(context.resources.getDimensionPixelSize(R.dimen.spacing_default))
        context.withStyledAttributes(attributeSet, R.styleable.VenueDetailsInformationItemView) {
            text = getString(R.styleable.VenueDetailsInformationItemView_text).orEmpty()
            iconRes = getResourceId(R.styleable.VenueDetailsInformationItemView_iconSrc, R.drawable.ic_menu_color)
        }
    }
}
