package de.culture4life.luca.util

import android.content.Context
import de.culture4life.luca.LucaApplication
import java.io.InputStream
import java.nio.file.Files
import java.nio.file.Paths

object AssetUtil {
    @JvmStatic
    fun getInputStream(fileName: String, context: Context): InputStream {
        return when {
            LucaApplication.isRunningTests() -> try {
                //noinspection NewApi
                Files.newInputStream(Paths.get("src/sharedTest/assets/$fileName"))
            } catch (throwable: Throwable) {
                context.assets.open(fileName)
            }
            else -> {
                context.assets.open(fileName)
            }
        }
    }

    @JvmStatic
    fun getBytes(fileName: String, context: Context): ByteArray {
        return getInputStream(fileName, context).use { it.readBytes() }
    }
}
