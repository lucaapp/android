package de.culture4life.luca.payment

import de.culture4life.luca.util.AmountUtil
import java.io.Serializable
import kotlin.math.min

data class PaymentAmounts(
    val invoiceAmount: Int,
    val openAmount: Int? = null,
    val tipPercentage: Int = 0,
    val discountPercentage: Int = 0,
    val maximumDiscountAmount: Int = invoiceAmount,
    val partialPaymentAmount: Int = 0
) : Serializable {
    val isPartialPayment = openAmount != null || partialPaymentAmount > 0
    val isClosingPayment = isPartialPayment && (invoiceAmount == openAmount || partialPaymentAmount == openAmount)
    val tipAmount = if (isPartialPayment) {
        AmountUtil.calculateTip(partialPaymentAmount, tipPercentage)
    } else {
        AmountUtil.calculateTip(invoiceAmount, tipPercentage)
    }
    val discountedInvoiceAmount = min(maximumDiscountAmount, AmountUtil.calculateTip(invoiceAmount, discountPercentage))
    val remainingInvoiceAmount = invoiceAmount - discountedInvoiceAmount
    val discountedOpenAmount = if (openAmount != null) min(maximumDiscountAmount, AmountUtil.calculateTip(openAmount, discountPercentage)) else null
    val remainingOpenAmount = if (discountedOpenAmount != null) openAmount?.minus(discountedOpenAmount) else null
    val discountedPartialPaymentAmount = min(maximumDiscountAmount, AmountUtil.calculateTip(partialPaymentAmount, discountPercentage))
    val remainingPartialPaymentAmount = partialPaymentAmount - discountedPartialPaymentAmount
    val totalAmount = if (isPartialPayment) remainingPartialPaymentAmount + tipAmount else remainingInvoiceAmount + tipAmount
}
