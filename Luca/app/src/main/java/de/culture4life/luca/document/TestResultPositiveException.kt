package de.culture4life.luca.document

class TestResultPositiveException : DocumentImportException("Test result is positive")
