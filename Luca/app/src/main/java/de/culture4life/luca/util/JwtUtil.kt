package de.culture4life.luca.util

import io.jsonwebtoken.*
import java.security.Key
import java.security.PublicKey

object JwtUtil {

    @JvmStatic
    fun parseJws(signedJws: String, publicKey: PublicKey): Jws<Claims> {
        return Jwts.parserBuilder()
            .setClock(TimeUtil.jwtClock)
            .setSigningKey(publicKey)
            .build()
            .parseClaimsJws(signedJws)
    }

    @JvmStatic
    fun parseSignedJwt(signedJwt: String): Jwt<Header<*>, Claims> {
        return parseUnsignedJwt(getUnsignedJwt(signedJwt))
    }

    @JvmStatic
    fun parseUnsignedJwt(unsignedJwt: String): Jwt<Header<*>, Claims> {
        return Jwts.parserBuilder()
            .setClock { TimeUtil.getCurrentDate() }
            .build()
            .parseClaimsJwt(unsignedJwt)
    }

    @JvmStatic
    fun getUnsignedJwt(signedJwt: String): String {
        val splitToken = signedJwt.split(".").toTypedArray()
        return splitToken[0] + "." + splitToken[1] + "."
    }

    @JvmStatic
    fun verifyJwt(signedJwt: String, signingKey: Key, pinnedAlgorithm: String? = null) {
        val jws = Jwts.parserBuilder()
            .setClock(TimeUtil.jwtClock)
            .setSigningKey(signingKey)
            .build()
            .parseClaimsJws(signedJwt)
        if (pinnedAlgorithm != null) {
            require(jws.header.algorithm == pinnedAlgorithm) { "Signing algorithm of JWT was not equal to $pinnedAlgorithm" }
        }
    }

    @JvmStatic
    fun getHeaderAndBody(encodedJwt: String): String {
        val parts = encodedJwt.split("\\.".toRegex()).toTypedArray()
        return parts[0] + "." + parts[1]
    }
}

fun String.parseSignedJwt(): Jwt<Header<*>, Claims> {
    return JwtUtil.parseSignedJwt(this)
}

fun String.verifyJwt(signingKey: Key, pinnedAlgorithm: String? = null) {
    return JwtUtil.verifyJwt(this, signingKey, pinnedAlgorithm)
}
