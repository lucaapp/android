package de.culture4life.luca.document

class DocumentExpiredException : DocumentImportException {
    constructor() : super("The document has expired")
    constructor(message: String) : super(message)
}
