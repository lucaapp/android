package de.culture4life.luca.ui.compound

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.withStyledAttributes
import androidx.core.view.isVisible
import de.culture4life.luca.R
import de.culture4life.luca.databinding.ViewCopyTokenHorizontalBinding
import de.culture4life.luca.util.ClipboardUtil

/**
 * View shows a token text accompanied by a copy icon on the right side.
 * The token is shown in a single line and the font size depends on the space available.
 * Supports two alignments:
 * - Center: Token is centered horizontally
 * - Start: Token is aligned with the left side of the parent
 */
class CopyTokenView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : ConstraintLayout(context, attrs, defStyleAttr) {

    val binding = ViewCopyTokenHorizontalBinding.inflate(LayoutInflater.from(context), this)

    init {
        context.withStyledAttributes(attrs, R.styleable.CopyTokenView) {
            setIconContentDescription(getString(R.styleable.CopyTokenView_iconContentDescription))
            setAlignment(Alignment.fromParams(getInt(R.styleable.CopyTokenView_alignment, Alignment.CENTER.id)))
            val labelText = getString(R.styleable.CopyTokenView_copyLabelText)
            if (labelText != null) {
                val successText = getString(R.styleable.CopyTokenView_copySuccessText) ?: context.getString(R.string.clipboard_success)
                setCopyOnClickListener { copyCode(labelText, successText) }
            }
        }
    }

    fun setToken(token: String) {
        binding.tokenTextView.text = token
    }

    fun getToken(): String = binding.tokenTextView.text.toString()

    fun setCopyOnClickListener(listener: OnClickListener) {
        binding.tokenTextView.setOnClickListener { listener.onClick(binding.tokenTextView) }
        binding.copyTokenImageView.setOnClickListener { listener.onClick(binding.copyTokenImageView) }
    }

    private fun copyCode(labelText: String, successText: String) {
        ClipboardUtil.copy(
            context = context,
            successText = successText,
            label = labelText,
            content = getToken()
        )
    }

    private fun setIconContentDescription(description: String?) {
        binding.copyTokenImageView.contentDescription = description
    }

    private fun setAlignment(alignment: Alignment) {
        require(alignment == Alignment.CENTER || alignment == Alignment.START)
        // Alignment.CENTER is default, constraints have to be adjusted if a different alignment is set
        if (alignment == Alignment.START) {
            binding.space.isVisible = false
            ConstraintSet().apply {
                clone(this@CopyTokenView)
                clear(R.id.tokenTextView, ConstraintSet.START)
                connect(R.id.tokenTextView, ConstraintSet.START, this@CopyTokenView.id, ConstraintSet.START, 0)
                setHorizontalChainStyle(R.id.tokenTextView, ConstraintSet.CHAIN_PACKED)
                setHorizontalBias(R.id.tokenTextView, 0.0f)
                applyTo(this@CopyTokenView)
            }
        }
    }

    enum class Alignment(val id: Int) {
        CENTER(0),
        START(1);

        companion object {

            fun fromParams(id: Int): Alignment {
                return when (id) {
                    0 -> CENTER
                    1 -> START
                    else -> throw IllegalAccessException("Unsupported Alignment")
                }
            }
        }
    }
}
