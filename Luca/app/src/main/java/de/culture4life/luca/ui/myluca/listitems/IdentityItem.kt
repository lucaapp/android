package de.culture4life.luca.ui.myluca.listitems

import de.culture4life.luca.R
import de.culture4life.luca.idnow.LucaIdData

class IdentityItem @JvmOverloads constructor(var idData: LucaIdData.DecryptedIdData? = null) :
    MyLucaListItem(),
    ExpandableListItem,
    DeletableListItem {

    override var isExpanded: Boolean = false

    override val deleteButtonText: Int
        get() = R.string.action_delete

    override val timestamp: Long
        get() = idData?.validSinceTimestamp ?: -1L
}
