package de.culture4life.luca.document

class DocumentAlreadyImportedException : DocumentImportException {
    constructor() : super("The document has already been imported")
    constructor(cause: Throwable) : super(cause)
}
