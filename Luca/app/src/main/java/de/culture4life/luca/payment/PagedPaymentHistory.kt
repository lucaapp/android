package de.culture4life.luca.payment

import de.culture4life.luca.network.pojo.payment.ConsumerPaymentsResponseData

data class PagedPaymentHistory(val nextPageCursor: String?, val payments: List<PaymentData>) {

    constructor(response: ConsumerPaymentsResponseData) : this(
        nextPageCursor = response.cursor,
        payments = response.payments.map(PaymentData::fromPayment)
    )
}
