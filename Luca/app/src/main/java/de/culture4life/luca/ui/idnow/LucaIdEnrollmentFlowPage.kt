package de.culture4life.luca.ui.idnow

import android.os.Bundle
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowPage

sealed class LucaIdEnrollmentFlowPage(override val id: Long, override val arguments: Bundle? = null) : BaseFlowPage {
    object ExplanationPage : LucaIdEnrollmentFlowPage(0)
    object ConsentPage : LucaIdEnrollmentFlowPage(1)
    object SuccessPage : LucaIdEnrollmentFlowPage(2)
}
