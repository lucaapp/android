package de.culture4life.luca.document.provider;

import de.culture4life.luca.document.Document;

public abstract class ProvidedDocument<DocumentType extends Document> {

    protected DocumentType document;

    public ProvidedDocument(DocumentType document) {
        this.document = document;
    }

    public DocumentType getDocument() {
        return document;
    }

    public void setDocument(DocumentType document) {
        this.document = document;
    }

}
