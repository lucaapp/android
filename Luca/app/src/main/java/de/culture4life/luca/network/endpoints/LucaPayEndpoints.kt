package de.culture4life.luca.network.endpoints

import de.culture4life.luca.network.pojo.payment.*
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import retrofit2.http.*

interface LucaPayEndpoints {

    /*
        Location
    */

    @GET("locations/{locationId}/paymentActive")
    fun getPaymentActiveStatus(
        @Path("locationId")
        locationId: String
    ): Single<PaymentActiveResponseData>

    @GET("locations/{locationId}/paymentRequests/open")
    fun getOpenPaymentRequest(
        @Path("locationId")
        locationId: String,
        @Query("table")
        table: String?
    ): Single<OpenPaymentRequestResponseData>

    @POST("locations/{locationId}/paymentRequests/{paymentRequestId}/checkout")
    fun createCheckoutForPaymentRequest(
        @Header("X-Auth")
        accessToken: String,
        @Path("locationId")
        locationId: String,
        @Path("paymentRequestId")
        paymentRequestId: String,
        @Body
        data: CreateOperatorCheckoutRequestData
    ): Single<CreateCheckoutResponseData>

    @GET("locations/{locationId}/campaigns")
    fun getCampaigns(
        @Path("locationId")
        locationId: String
    ): Single<List<CampaignResponseData>>

    /*
        Consumer
     */

    @POST("consumers/signup")
    @Headers("Content-Type: application/json")
    fun signUpConsumer(
        @Body
        data: ConsumerSignUpRequestData
    ): Single<ConsumerSignUpResponseData>

    @POST("consumers/signin")
    @Headers("Content-Type: application/json")
    fun signInConsumer(
        @Body
        data: ConsumerSignInRequestData
    ): Single<ConsumerSignInResponseData>

    @GET("consumers/me")
    fun getConsumerInformation(
        @Header("X-Auth")
        accessToken: String
    ): Single<ConsumerInformationResponseData>

    @DELETE("consumers/me")
    fun deleteConsumer(
        @Header("X-Auth")
        accessToken: String
    ): Completable

    @GET("consumers/me/payments")
    fun getConsumerPayments(
        @Header("X-Auth")
        accessToken: String,
        @Query("cursor")
        cursor: String? = null
    ): Single<ConsumerPaymentsResponseData>

    @POST("consumers/payments/{rapydPaymentId}/campaign")
    fun submitCampaignParticipation(
        @Header("X-Auth")
        accessToken: String,
        @Path("rapydPaymentId")
        paymentId: String,
        @Body
        campaignSubmissionRequestData: CampaignSubmissionRequestData
    ): Single<CampaignSubmissionResponseData>

    /*
        Checkout
    */

    @GET("checkouts/{checkoutId}")
    fun getCheckout(
        @Path("checkoutId")
        checkoutId: String
    ): Single<CheckoutResponseData>

    @POST("checkouts")
    fun createCheckout(
        @Header("X-Auth")
        accessToken: String,
        @Body
        data: CreateCustomerCheckoutRequestData
    ): Single<CreateCheckoutResponseData>

    /*
        Config
     */

    @GET("configs/paymentLimits")
    fun getPaymentLimits(): Single<PaymentLimitsResponseData>
}
