package de.culture4life.luca.payment

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Data of the consumer that they are signed in with containing the [accessToken] to use for further authentication..
 *
 * @see [Sign in consumer](https://app.luca-app.de/pay/api/v1/swagger/#/Consumers/post_consumers_signin)
 */
data class SignInData(

    @Expose
    @SerializedName("accessToken")
    val accessToken: String,

    @Expose
    @SerializedName("expiresIn")
    val expirationTimestamp: Long
)
