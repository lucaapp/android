package de.culture4life.luca.ui.venue

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.core.app.ShareCompat
import androidx.lifecycle.*
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.R
import de.culture4life.luca.checkin.AdditionalCheckInData
import de.culture4life.luca.checkin.CheckInManager
import de.culture4life.luca.consent.ConsentManager
import de.culture4life.luca.consent.MissingConsentException
import de.culture4life.luca.network.pojo.LocationResponseData
import de.culture4life.luca.network.pojo.payment.CampaignResponseData
import de.culture4life.luca.network.pojo.payment.OpenPaymentRequestResponseData
import de.culture4life.luca.payment.PaymentAmounts
import de.culture4life.luca.payment.PaymentData
import de.culture4life.luca.payment.PaymentLocationData
import de.culture4life.luca.payment.PaymentManager
import de.culture4life.luca.ui.BaseViewModel
import de.culture4life.luca.ui.ViewEvent
import de.culture4life.luca.ui.checkin.LocationUrl
import de.culture4life.luca.util.TimeUtil
import de.culture4life.luca.util.isCause
import de.culture4life.luca.util.toCurrencyAmountNumber
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import timber.log.Timber
import java.util.concurrent.TimeUnit

class VenueDetailsViewModel(application: Application, private val savedStateHandle: SavedStateHandle) : BaseViewModel(application) {

    private val checkInManager: CheckInManager = this.application.checkInManager
    private val paymentManager: PaymentManager = this.application.paymentManager
    private val consentManager: ConsentManager = this.application.consentManager

    private var additionalData: AdditionalCheckInData? = null
    private val venueDetails = MutableLiveData<VenueDetails>()
    private val tableName = MutableLiveData<String>()
    private val openPaymentAmount = MutableLiveData(OPEN_PAYMENT_AMOUNT_NOT_GIVEN)
    private val checkInTimestamp: Long = savedStateHandle.get<Long>(SAVED_STATE_CHECK_IN_TIMESTAMP) ?: TimeUtil.getCurrentMillis()
    private val paymentCancelled = MutableLiveData<ViewEvent<Unit>>()
    private val paymentCompleted = MutableLiveData<ViewEvent<PaymentData>>()
    private val paymentStarted = MutableLiveData<ViewEvent<PaymentLocationData>>()
    val askUrlConsent = MutableLiveData<ViewEvent<LocationUrl.UrlType>>()
    val activeCampaign = MutableLiveData<Campaign>()

    val paymentStatus = createPaymentStatusLiveData()
    val viewState = createViewStateLiveData()
    val showStartPaymentButton = createShowStartPaymentButtonLiveData()

    override fun initialize(): Completable {
        return super.initialize()
            .andThen(
                Completable.mergeArray(
                    checkInManager.initialize(application),
                    paymentManager.initialize(application),
                    consentManager.initialize(application)
                )
            )
            .doOnComplete {
                if (!savedStateHandle.contains(SAVED_STATE_CHECK_IN_TIMESTAMP)) {
                    savedStateHandle.set(SAVED_STATE_CHECK_IN_TIMESTAMP, checkInTimestamp)
                }
            }
    }

    override fun processArguments(arguments: Bundle?): Completable {
        return super.processArguments(arguments)
            .andThen(updateAdditionalData(arguments))
            .andThen(updateVenueDetails(arguments))
    }

    override fun keepDataUpdated(): Completable {
        return Completable.mergeArray(
            super.keepDataUpdated(),
            keepPaymentRequestDataUpdated(),
            keepPaymentDiscountCampaignUpdated(),
            keepPaymentResultsUpdated()
        )
    }

    private fun updateVenueDetails(arguments: Bundle?): Completable {
        return Single.fromCallable {
            val locationData = arguments?.getSerializable(VenueDetailsFragment.ARGUMENT_LOCATION_DATA_KEY) as? LocationResponseData
            requireNotNull(locationData)
            val locationUrls = arguments.getSerializable(VenueDetailsFragment.ARGUMENT_LOCATION_URLS_KEY) as? List<LocationUrl>
            VenueDetails(
                title = if (locationData.areaName.isNullOrBlank()) locationData.groupName else locationData.areaName,
                subtitle = if (locationData.areaName.isNullOrBlank()) "" else locationData.groupName,
                locationData = locationData,
                providedUrls = locationUrls.orEmpty()
            )
        }
            .observeOn(AndroidSchedulers.mainThread())
            .flatMapCompletable { updateInstantly(venueDetails, it) }
    }

    private fun updateAdditionalData(arguments: Bundle?): Completable {
        return Maybe.fromCallable<String> { arguments?.getString(VenueDetailsFragment.ARGUMENT_ADDITIONAL_DATA_KEY) }
            .flatMapCompletable {
                additionalData = AdditionalCheckInData(it)
                Timber.d("Additional data: $additionalData")
                updateTableName(additionalData)
            }
            .onErrorComplete()
    }

    private fun updateTableName(additionalData: AdditionalCheckInData?): Completable {
        return Maybe.fromCallable<String> { additionalData?.tableId }
            .flatMap { paymentManager.fetchTableName(it) }
            .onErrorComplete()
            .switchIfEmpty(Maybe.fromCallable<String> { additionalData?.table })
            .flatMapCompletable { update(tableName, it) }
    }

    fun paymentCancelled() {
        updateAsSideEffect(paymentCancelled, ViewEvent(Unit))
    }

    private fun keepPaymentRequestDataUpdated(): Completable {
        return Completable.defer {
            val locationId = venueDetails.value!!.locationData.locationId
            val table = additionalData?.table
            pollOpenPaymentRequests(locationId, table)
                .flatMapCompletable { update(openPaymentAmount, it.invoiceAmount.toCurrencyAmountNumber()) }
        }
    }

    private fun pollOpenPaymentRequests(locationId: String, table: String?): Flowable<OpenPaymentRequestResponseData> {
        return Flowable.interval(0, PAYMENT_REQUEST_POLLING_INTERVAL, TimeUnit.MILLISECONDS, Schedulers.io())
            .filter { venueDetails.value!!.locationData.isPaymentActive }
            .flatMapMaybe {
                paymentManager.fetchOpenPaymentRequest(locationId, table)
                    .switchIfEmpty(
                        Maybe.defer {
                            updateAsSideEffectIfRequired(openPaymentAmount, OPEN_PAYMENT_AMOUNT_NOT_GIVEN)
                            Maybe.empty()
                        }
                    )
                    .onErrorResumeWith { Maybe.empty<OpenPaymentRequestResponseData>() }
            }
            .onBackpressureBuffer(3)
            .distinctUntilChanged()
    }

    private fun keepPaymentDiscountCampaignUpdated(): Completable {
        return Completable.defer {
            val locationId = venueDetails.value!!.locationData.locationId
            pollPaymentDiscountCampaign(locationId)
                .flatMapCompletable { update(activeCampaign, it) }
        }
    }

    private fun pollPaymentDiscountCampaign(locationId: String): Flowable<Campaign> {
        return Flowable.interval(0, PAYMENT_CAMPAIGN_POLLING_INTERVAL, TimeUnit.MILLISECONDS, Schedulers.io())
            .filter { venueDetails.value!!.locationData.isPaymentActive }
            .flatMapMaybe {
                paymentManager.getActiveDiscountCampaignIfAvailable(locationId)
                    .map<Campaign> { Campaign.DiscountCampaign(it) }
                    .switchIfEmpty(Maybe.just(Campaign.NoCampaign))
                    .onErrorResumeWith { Maybe.empty<Campaign>() }
            }
            .onBackpressureBuffer(3)
            .distinctUntilChanged()
    }

    private fun keepPaymentResultsUpdated(): Completable {
        return paymentManager.getPaymentResults()
            .filter(PaymentData::isSuccessful)
            .filter { it.locationId == venueDetails.value?.locationData?.locationId }
            .filter { it.timestamp > checkInTimestamp }
            .flatMapCompletable { update(paymentCompleted, ViewEvent(it)) }
    }

    fun reportAbuse(activity: Activity) {
        val locationData = venueDetails.value?.locationData
        val providedUrls = venueDetails.value?.providedUrls
        val locationName = locationData?.locationDisplayName ?: application.getString(R.string.unknown)
        val locationId = locationData?.locationId ?: application.getString(R.string.unknown)
        val locationUrls = providedUrls?.joinToString("\n") { (_, url) -> "- $url" }
            ?: application.getString(R.string.unknown)

        val text = application.getString(R.string.venue_url_report_description, locationName, locationId, locationUrls)

        ShareCompat.IntentBuilder(activity)
            .setType(LucaApplication.INTENT_TYPE_MAIL)
            .addEmailTo(application.getString(R.string.mail_abuse))
            .setSubject(application.getString(R.string.venue_url_report_title))
            .setText(text)
            .startChooser()
    }

    fun openProvidedUrlOrRequestConsent(urlType: LocationUrl.UrlType) {
        invoke(
            consentManager.getConsent(ConsentManager.ID_OPEN_VENUE_URL)
                .doOnSuccess { consent ->
                    if (consent.approved) {
                        getProvidedUrl(urlType)?.also { application.openUrl(it) }
                    } else {
                        updateAsSideEffect(askUrlConsent, ViewEvent(urlType))
                    }
                }
                .ignoreElement()
        ).subscribe()
    }

    fun getProvidedUrl(urlType: LocationUrl.UrlType): String? {
        return venueDetails.value?.providedUrls?.firstOrNull { (type, _) -> type == urlType }?.url
    }

    fun onPaymentStartRequested() {
        invoke(
            paymentManager.requestConsentAndSignUpIfRequired()
                .andThen(startPayment())
                .doOnSubscribe { updateAsSideEffect(isLoading, true) }
                .doFinally { updateAsSideEffect(isLoading, false) }
                .doOnError {
                    Timber.w("Unable to initiate payment: $it")
                    val error = createErrorBuilder(it)
                        .withResolveAction(Completable.fromAction { onPaymentStartRequested() })
                        .withResolveLabel(R.string.action_retry)
                        .removeWhenShown()
                    if (!it.isCause(MissingConsentException::class.java)) {
                        error.withTitle(R.string.error_request_failed_title)
                    }
                    addError(error.build())
                }
        ).subscribe()
    }

    private fun startPayment(): Completable {
        return createPaymentData()
            .flatMapCompletable { update(paymentStarted, ViewEvent(it)) }
    }

    private fun createPaymentData(): Single<PaymentLocationData> {
        return Single.fromCallable {
            val locationData = venueDetails.value!!.locationData
            val locationName = locationData.locationDisplayName ?: application.getString(R.string.unknown)
            val table = additionalData?.table
            PaymentLocationData(locationData.locationId, locationName, table, locationData.isPaymentActive)
        }
    }

    private fun createShowStartPaymentButtonLiveData(): LiveData<Boolean> {
        return MediatorLiveData<Boolean>().apply {
            value = false
            fun getNewState(): Boolean {
                val paymentStatus = paymentStatus.value
                val viewState = viewState.value
                return (viewState is ViewState.Content || viewState is ViewState.Empty) && paymentStatus !is PaymentStatus.PaymentNotEnabled
            }

            addSource(paymentStatus) { value = getNewState() }
            addSource(viewState) { value = getNewState() }
        }.distinctUntilChanged()
    }

    private fun createPaymentStatusLiveData(): LiveData<PaymentStatus> {
        return MediatorLiveData<PaymentStatus>().apply {
            value = PaymentStatus.PaymentNotEnabled
            fun getNewState(): PaymentStatus {
                val venueDetails = venueDetails.value
                val paymentActive = venueDetails?.locationData?.isPaymentActive ?: false

                val paymentStartedValue = paymentStarted.value
                val unhandledPaymentStart = paymentStartedValue?.isNotHandled ?: false

                val paymentCompletedValue = paymentCompleted.value
                val unhandledPaymentCompletion = paymentCompletedValue?.isNotHandled ?: false

                val paymentCancelledValue = paymentCancelled.value
                val unhandledPaymentCancellation = paymentCancelledValue?.isNotHandled ?: false

                return when {
                    !paymentActive -> PaymentStatus.PaymentNotEnabled
                    unhandledPaymentStart -> PaymentStatus.PaymentStart(paymentStartedValue!!.valueAndMarkAsHandled)
                    unhandledPaymentCancellation -> {
                        paymentCancelledValue?.valueAndMarkAsHandled
                        PaymentStatus.PaymentCancelled
                    }
                    unhandledPaymentCompletion -> PaymentStatus.PaymentCompletion(paymentCompletedValue!!.valueAndMarkAsHandled)
                    else -> {
                        val openPaymentAmountValue = openPaymentAmount.value!!
                        val paymentAmounts = if (openPaymentAmountValue == OPEN_PAYMENT_AMOUNT_NOT_GIVEN) {
                            PAYMENT_AMOUNT_NOT_GIVEN
                        } else {
                            val activeCampaign = activeCampaign.value
                            if (activeCampaign is Campaign.DiscountCampaign) {
                                PaymentAmounts(
                                    invoiceAmount = openPaymentAmountValue,
                                    discountPercentage = activeCampaign.data.discountPercentage!!,
                                    maximumDiscountAmount = activeCampaign.data.maximumDiscountAmount!!.toCurrencyAmountNumber()
                                )
                            } else {
                                PaymentAmounts(invoiceAmount = openPaymentAmountValue)
                            }
                        }
                        PaymentStatus.PaymentRequest(paymentAmounts)
                    }
                }
            }
            addSource(venueDetails) { value = getNewState() }
            addSource(openPaymentAmount) { value = getNewState() }
            addSource(paymentStarted) { value = getNewState() }
            addSource(paymentCompleted) { value = getNewState() }
            addSource(paymentCancelled) { value = getNewState() }
            addSource(activeCampaign) { value = getNewState() }
        }.distinctUntilChanged()
    }

    private fun createViewStateLiveData(): LiveData<ViewState> {
        return MediatorLiveData<ViewState>().apply {
            value = ViewState.Initial
            fun getNewState(): ViewState {
                val venueDetails = venueDetails.value
                return when {
                    isLoading.value == true -> ViewState.Loading
                    venueDetails == null -> ViewState.Initial
                    venueDetails.providedUrls.isNullOrEmpty() && !venueDetails.locationData.isPaymentActive -> ViewState.Empty(venueDetails)
                    else -> ViewState.Content(venueDetails, tableName.value)
                }
            }
            addSource(isLoading) { value = getNewState() }
            addSource(venueDetails) { value = getNewState() }
            addSource(tableName) { value = getNewState() }
        }.distinctUntilChanged()
    }

    sealed class PaymentStatus {
        object PaymentNotEnabled : PaymentStatus()
        data class PaymentRequest(val data: PaymentAmounts?) : PaymentStatus()
        data class PaymentStart(val data: PaymentLocationData) : PaymentStatus()
        data class PaymentCompletion(val data: PaymentData) : PaymentStatus()
        object PaymentCancelled : PaymentStatus()
    }

    sealed class ViewState {
        object Loading : ViewState()
        object Initial : ViewState()
        data class Empty(val venueDetails: VenueDetails) : ViewState()
        data class Content(val venueDetails: VenueDetails, val tableName: String?) : ViewState()
    }

    sealed class Campaign {
        object NoCampaign : Campaign()
        data class DiscountCampaign(val data: CampaignResponseData.PaymentCampaign) : Campaign()
    }

    companion object {
        val PAYMENT_REQUEST_POLLING_INTERVAL = if (LucaApplication.isRunningTests()) {
            TimeUnit.MILLISECONDS.toMillis(500)
        } else {
            TimeUnit.SECONDS.toMillis(10)
        }
        val PAYMENT_CAMPAIGN_POLLING_INTERVAL = if (LucaApplication.isRunningTests()) {
            TimeUnit.MILLISECONDS.toMillis(500)
        } else {
            TimeUnit.SECONDS.toMillis(30)
        }
        val PAYMENT_AMOUNT_NOT_GIVEN = null
        private const val OPEN_PAYMENT_AMOUNT_NOT_GIVEN = -1
        private const val SAVED_STATE_CHECK_IN_TIMESTAMP = "check_in_timestamp"
    }
}
