package de.culture4life.luca.ui.payment.history.recycler.viewholder

import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import de.culture4life.luca.databinding.ItemPaymentsLoadingBinding

class PaymentsLoadingViewHolder(
    private val binding: ItemPaymentsLoadingBinding,
    private val retry: () -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(loadState: LoadState) {
        binding.loadingIndicator.isVisible = loadState is LoadState.Loading
        binding.retryButton.isVisible = loadState is LoadState.Error
        binding.errorTextView.isVisible = loadState is LoadState.Error
        binding.root.setOnClickListener { retry() }
    }
}
