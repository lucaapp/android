package de.culture4life.luca.ui.onboarding

import android.app.Application
import androidx.lifecycle.MutableLiveData
import de.culture4life.luca.consent.ConsentManager.Companion.ID_TERMS_OF_SERVICE_LUCA_ID
import de.culture4life.luca.ui.BaseViewModel
import de.culture4life.luca.ui.ViewEvent
import de.culture4life.luca.ui.onboarding.OnboardingActivity.Companion.WELCOME_SCREEN_SEEN_KEY
import de.culture4life.luca.ui.terms.UpdatedTermsUtil
import de.culture4life.luca.util.addTo
import io.reactivex.rxjava3.core.Completable

class OnboardingViewModel(app: Application) : BaseViewModel(app) {

    val termsCheckBoxErrorLiveData: MutableLiveData<ViewEvent<Boolean>> = MutableLiveData()
    val privacyPolicyCheckBoxErrorLiveData: MutableLiveData<ViewEvent<Boolean>> = MutableLiveData()
    val showRegistrationScreenLiveData: MutableLiveData<ViewEvent<Boolean>> = MutableLiveData()

    fun onWelcomeActionButtonClicked(termsChecked: Boolean, privacyPolicyChecked: Boolean) {
        if (termsChecked && privacyPolicyChecked) {
            preferencesManager
                .persist(WELCOME_SCREEN_SEEN_KEY, true)
                .andThen(UpdatedTermsUtil.markTermsAsAccepted(application))
                .andThen(markTermsConsentAsGiven())
                .onErrorComplete()
                .subscribe { updateAsSideEffect(showRegistrationScreenLiveData, ViewEvent(true)) }
                .addTo(modelDisposable)
        } else {
            if (!termsChecked) updateAsSideEffect(termsCheckBoxErrorLiveData, ViewEvent(true))
            if (!privacyPolicyChecked) updateAsSideEffect(privacyPolicyCheckBoxErrorLiveData, ViewEvent(true))
        }
    }

    private fun markTermsConsentAsGiven(): Completable {
        return with(application.consentManager) {
            initialize(application).andThen(processConsentRequestResult(ID_TERMS_OF_SERVICE_LUCA_ID, true))
        }
    }
}
