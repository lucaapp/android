package de.culture4life.luca.ui.venue

import de.culture4life.luca.network.pojo.LocationResponseData
import de.culture4life.luca.ui.checkin.LocationUrl

data class VenueDetails(
    val title: String?,
    val subtitle: String?,
    val locationData: LocationResponseData,
    val providedUrls: List<LocationUrl>
)
