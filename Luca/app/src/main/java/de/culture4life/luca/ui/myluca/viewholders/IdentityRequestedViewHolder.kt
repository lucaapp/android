package de.culture4life.luca.ui.myluca.viewholders

import androidx.recyclerview.widget.RecyclerView
import de.culture4life.luca.databinding.ItemMyLucaIdentityRequestedBinding
import de.culture4life.luca.ui.myluca.listitems.IdentityRequestedItem

class IdentityRequestedViewHolder(val binding: ItemMyLucaIdentityRequestedBinding) : RecyclerView.ViewHolder(binding.root) {

    fun show(item: IdentityRequestedItem) {
        binding.copyTokenView.setToken(item.token)
    }

    fun setClickListener(action: () -> Unit) {
        binding.cardView.setOnClickListener { action.invoke() }
    }

    fun setLongClickListener(action: () -> Boolean) {
        binding.cardView.setOnLongClickListener { action.invoke() }
    }
}
