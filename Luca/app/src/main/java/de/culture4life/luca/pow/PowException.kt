package de.culture4life.luca.pow

class PowException(message: String, cause: Throwable) : Exception(message, cause)
