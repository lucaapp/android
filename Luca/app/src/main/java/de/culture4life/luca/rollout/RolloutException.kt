package de.culture4life.luca.rollout

class RolloutException(id: String) : IllegalStateException("The $id feature is not yet available")
