package de.culture4life.luca.ui.payment.history

import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.RecyclerView
import de.culture4life.luca.R
import de.culture4life.luca.databinding.FragmentPaymentHistoryBinding
import de.culture4life.luca.payment.PaymentData
import de.culture4life.luca.payment.PaymentLocationData
import de.culture4life.luca.ui.BaseFragment
import de.culture4life.luca.ui.payment.PaymentFlowBottomSheetFragment
import de.culture4life.luca.ui.payment.PaymentFlowBottomSheetFragment.Companion.NEW_PAYMENT_REQUEST_KEY
import de.culture4life.luca.ui.payment.PaymentFlowBottomSheetFragment.Companion.NEW_PAYMENT_RESULT_KEY
import de.culture4life.luca.ui.payment.history.recycler.adapter.PaymentsRecyclerAdapter
import de.culture4life.luca.ui.payment.history.recycler.adapter.PaymentsRecyclerLoadStateAdapter
import de.culture4life.luca.ui.payment.history.recycler.diffutil.PaymentsDiffCallback
import de.culture4life.luca.ui.qrcode.standalone.QrCodeScannerBottomSheetFragment
import kotlinx.coroutines.*

class PaymentHistoryFragment : BaseFragment<PaymentHistoryViewModel>() {

    private lateinit var binding: FragmentPaymentHistoryBinding
    private val paymentsAdapter = PaymentsRecyclerAdapter(PaymentsDiffCallback()) { showPaymentDetailsInfo(it) }
    private val concatAdapter by lazy {
        paymentsAdapter.withLoadStateAdapters(
            PaymentsRecyclerLoadStateAdapter(paymentsAdapter::retry),
            PaymentsRecyclerLoadStateAdapter(paymentsAdapter::retry)
        )
    }

    override fun getViewModelClass() = PaymentHistoryViewModel::class.java
    override fun getViewBinding() = FragmentPaymentHistoryBinding.inflate(layoutInflater).also { this.binding = it }

    override fun initializeViews() {
        super.initializeViews()
        initializeList()
        initializeNewPayment()
        initializeObservers()
        binding.helpActionBarMenuImageView.setOnClickListener { showHelpScreen() }
    }

    private fun showHelpScreen() {
        viewModel.requestSupportMail()
    }

    private fun initializeNewPayment() {
        observe(viewModel.paymentLocationData) {
            if (!it.isHandled) {
                startNewPayment(it.valueAndMarkAsHandled)
            }
        }

        childFragmentManager.setFragmentResultListener(NEW_PAYMENT_REQUEST_KEY, this) { _, bundle ->
            if (bundle.getBoolean(NEW_PAYMENT_RESULT_KEY)) {
                scrollToTopAfterAutomaticRefresh()
                paymentsAdapter.refresh()
            }
        }

        childFragmentManager.setFragmentResultListener(QrCodeScannerBottomSheetFragment.REQUEST_KEY, this) { _, bundle ->
            val barcodeData = bundle.getString(QrCodeScannerBottomSheetFragment.BUNDLE_RESULT_KEY)!!
            viewModel.invokeProcessQrCodeScan(barcodeData)
        }

        binding.newPaymentButton.setOnClickListener { openScanner() }
    }

    private fun openScanner() {
        QrCodeScannerBottomSheetFragment.newInstance()
            .show(childFragmentManager, QrCodeScannerBottomSheetFragment.TAG)
    }

    private fun startNewPayment(barcodeData: PaymentLocationData) {
        PaymentFlowBottomSheetFragment.newInstance(barcodeData)
            .show(childFragmentManager, PaymentFlowBottomSheetFragment.TAG)
    }

    private fun showPaymentDetailsInfo(payment: PaymentData) {
        val bundle = PaymentHistoryDetailFragment.createArguments(payment)
        safeNavigateFromNavController(R.id.action_paymentHistoryFragment_to_paymentHistoryDetailFragment, bundle)
    }

    private fun initializeObservers() {
        observe(viewModel.isSignedUpForPayment) { isSignedUp ->
            if (!isSignedUp) {
                safeNavigateFromNavController(R.id.action_paymentHistoryFragment_to_paymentFragment)
            }
        }
        observe(viewModel.updatePayments) {
            if (it.isNotHandled) {
                it.isHandled = true
                scrollToTopAfterAutomaticRefresh()
                paymentsAdapter.refresh()
            }
        }
    }

    /**
     * Scrolls to top once list has been updated
     */
    private fun scrollToTopAfterAutomaticRefresh() {
        object : RecyclerView.AdapterDataObserver() {
            fun changed() {
                binding.paymentsList.scrollToPosition(0)
                paymentsAdapter.unregisterAdapterDataObserver(this)
            }
            override fun onChanged() = changed()
            override fun onItemRangeChanged(positionStart: Int, itemCount: Int) = changed()
            override fun onItemRangeChanged(positionStart: Int, itemCount: Int, payload: Any?) = changed()
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) = changed()
            override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) = changed()
            override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) = changed()
        }.also { paymentsAdapter.registerAdapterDataObserver(it) }
    }

    private fun initializeList() {
        // Observe data changes for list data
        observe(viewModel.pagedPayments) {
            paymentsAdapter.submitData(viewLifecycleOwner.lifecycle, it)
        }

        // Setup list with adapter
        binding.refreshLayout.setOnRefreshListener { paymentsAdapter.refresh() }
        binding.paymentsList.adapter = concatAdapter
        paymentsAdapter.addLoadStateListener { loadState ->
            val isEmpty = paymentsAdapter.itemCount == 0 && loadState.append.endOfPaginationReached && loadState.refresh is LoadState.NotLoading
            binding.refreshLayout.isRefreshing = loadState.refresh is LoadState.Loading
            binding.paymentsList.isVisible = !isEmpty
            binding.emptyGroup.isVisible = isEmpty
        }
    }

    private fun <T : Any, V : RecyclerView.ViewHolder> PagingDataAdapter<T, V>.withLoadStateAdapters(
        header: LoadStateAdapter<*>,
        footer: LoadStateAdapter<*>
    ): ConcatAdapter {
        addLoadStateListener { loadStates ->
            // In header we only want to show errors because swiperefresh layout already functions as loading indicator
            header.loadState = if (loadStates.refresh is LoadState.Error) loadStates.refresh else LoadState.NotLoading(true)
            footer.loadState = loadStates.append
        }
        return ConcatAdapter(header, this, footer)
    }
}
