package de.culture4life.luca.authentication

import androidx.biometric.BiometricPrompt
import androidx.biometric.BiometricPrompt.AUTHENTICATION_RESULT_TYPE_BIOMETRIC
import androidx.biometric.BiometricPrompt.AuthenticationCallback
import androidx.fragment.app.FragmentManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import de.culture4life.luca.BuildConfig
import de.culture4life.luca.R
import de.culture4life.luca.ui.dialog.BaseDialogFragment
import de.culture4life.luca.util.addTo
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import timber.log.Timber

class AuthenticationUiExtension(
    private val fragmentManager: FragmentManager,
    private val authenticationManager: AuthenticationManager,
    private val disposable: CompositeDisposable
) {

    init {
        initializeAuthenticationRequests()
    }

    private fun initializeAuthenticationRequests() {
        authenticationManager.getAuthenticationRequests()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::showAuthenticationPromptIfPossible)
            .addTo(disposable)
    }

    private fun showAuthenticationPromptIfPossible(authenticationRequest: AuthenticationRequest) {
        if (authenticationManager.isAuthenticationPossible(authenticationRequest)) {
            showAuthenticationPrompt(authenticationRequest)
        } else {
            showAuthenticationNotPossibleError(authenticationRequest)
        }
    }

    private fun showAuthenticationPrompt(authenticationRequest: AuthenticationRequest) {
        Timber.d("Showing authentication prompt for ${authenticationRequest.id}")
        val fragment = fragmentManager.fragments.last()
        val promptInfo = createPromptInfo(authenticationRequest)
        val promptCallback = createPromptCallback(authenticationRequest)
        val prompt = BiometricPrompt(fragment, promptCallback)
        if (authenticationRequest.cryptoObject != null) {
            prompt.authenticate(promptInfo, authenticationRequest.cryptoObject)
        } else {
            prompt.authenticate(promptInfo)
        }
    }

    private fun showAuthenticationNotPossibleError(authenticationRequest: AuthenticationRequest) {
        Timber.d("Showing authentication not possible error for ${authenticationRequest.id}")
        val fragment = fragmentManager.fragments.last()
        val dialog = MaterialAlertDialogBuilder(fragment.requireContext())
            .setTitle(R.string.authentication_error_not_activated_title)
            .setMessage(R.string.authentication_error_not_activated_description)
            .setPositiveButton(R.string.action_ok) { _, _ ->
                authenticationRequest.callback.onAuthenticationResult(
                    AuthenticationResult(success = false)
                )
            }

        if (BuildConfig.DEBUG) {
            // Enable to proceed with testing in environments which don't have screen lock configured.
            // Very handy for emulators, test devices and automated tests.
            dialog.setNeutralButton("Mock Success") { _, _ ->
                Timber.w("Mocking a successful authentication result for ${authenticationRequest.id}")
                authenticationRequest.callback.onAuthenticationResult(
                    AuthenticationResult(
                        success = true,
                        authenticationType = AUTHENTICATION_RESULT_TYPE_BIOMETRIC,
                        cryptoObject = authenticationRequest.cryptoObject // will not be authenticated!
                    )
                )
            }
        }

        BaseDialogFragment(dialog).apply {
            isCancelable = false
            show()
        }
    }

    private fun createPromptInfo(authenticationRequest: AuthenticationRequest): BiometricPrompt.PromptInfo {
        with(authenticationRequest) {
            val builder = BiometricPrompt.PromptInfo.Builder()
            promptTitle?.let(builder::setTitle)
            promptSubtitle?.let(builder::setSubtitle)
            promptDescription?.let(builder::setDescription)
            allowedAuthenticators.let(builder::setAllowedAuthenticators)
            return builder.build()
        }
    }

    private fun createPromptCallback(authenticationRequest: AuthenticationRequest): AuthenticationCallback {
        return object : BiometricPrompt.AuthenticationCallback() {
            override fun onAuthenticationSucceeded(promptResult: BiometricPrompt.AuthenticationResult) {
                Timber.d("User authentication succeeded")
                authenticationRequest.callback.onAuthenticationResult(
                    AuthenticationResult(
                        success = true,
                        authenticationType = promptResult.authenticationType,
                        cryptoObject = promptResult.cryptoObject
                    )
                )
            }

            override fun onAuthenticationError(errorCode: Int, errorString: CharSequence) {
                Timber.d("User authentication error, code: $errorCode, message: $errorString")
                authenticationRequest.callback.onAuthenticationResult(
                    AuthenticationResult(success = false)
                )
            }

            override fun onAuthenticationFailed() {
                Timber.d("User authentication failed")
            }
        }
    }
}
