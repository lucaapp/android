package de.culture4life.luca.consent

import android.content.Context
import de.culture4life.luca.Manager
import de.culture4life.luca.preference.PreferencesManager
import de.culture4life.luca.util.TimeUtil
import de.culture4life.luca.util.addTo
import de.culture4life.luca.util.assertTrue
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.BehaviorSubject
import io.reactivex.rxjava3.subjects.PublishSubject
import timber.log.Timber

open class ConsentManager(
    private val preferencesManager: PreferencesManager
) : Manager() {

    private val consentSubjects: MutableMap<String, BehaviorSubject<Consent>> = HashMap()
    private val consentRequestsSubject: PublishSubject<String> = PublishSubject.create()

    override fun doInitialize(context: Context): Completable {
        return preferencesManager.initialize(context)
    }

    override fun dispose() {
        consentSubjects.clear()
        super.dispose()
    }

    fun getConsentRequests(): Observable<String> {
        return consentRequestsSubject
    }

    fun getConsent(id: String): Single<Consent> {
        return getOrCreateConsentSubject(id).firstOrError()
    }

    fun getConsentAndChanges(id: String): Observable<Consent> {
        return Observable.defer { getOrCreateConsentSubject(id) }
    }

    fun persistConsent(consent: Consent): Completable {
        return preferencesManager.persist(getPreferenceKey(consent.id), consent)
            .doOnComplete { getOrCreateConsentSubject(consent.id).onNext(consent) }
            .doOnSubscribe { Timber.v("Persisting consent: $consent") }
    }

    fun assertConsentApproved(id: String): Completable {
        return getConsent(id)
            .flatMapCompletable { assertConsentApproved(it) }
    }

    fun assertConsentApproved(consent: Consent): Completable {
        return Single.fromCallable { consent.approved }
            .assertTrue { MissingConsentException(consent.id) }
            .doOnSubscribe { Timber.v("Asserting that consent is approved: $consent") }
    }

    fun requestConsentIfRequiredAndAssertApproved(id: String): Completable {
        return requestConsentIfRequiredAndGetResult(id)
            .flatMapCompletable { assertConsentApproved(it) }
    }

    fun requestConsentIfRequiredAndGetResult(id: String): Single<Consent> {
        return getConsent(id)
            .flatMap {
                assertConsentApproved(id)
                    .andThen(Single.just(it))
                    .onErrorResumeWith(requestConsentAndGetResult(id))
            }
    }

    /**
     * Requests a series of consents if required and continues within the series when the previous one was approved.
     *
     * @throws MissingConsentException if one of the consents was not approved
     */
    fun requestConsentBundleIfRequiredAndAssertApproved(vararg ids: String): Completable {
        val requests = ids.map { requestConsentIfRequiredAndAssertApproved(it) }
        return Completable.concat(requests)
    }

    fun requestConsentAndGetResult(id: String): Single<Consent> {
        return requestConsent(id)
            .andThen(getConsentAndChanges(id))
            .skip(1)
            .firstOrError()
    }

    fun requestConsentIfRequired(id: String): Completable {
        return assertConsentApproved(id)
            .onErrorResumeWith(requestConsent(id))
    }

    fun requestConsent(id: String): Completable {
        return Completable.fromAction { consentRequestsSubject.onNext(id) }
            .doOnSubscribe { Timber.d("Requesting consent for: $id") }
    }

    fun revokeConsent(id: String): Completable {
        return getConsent(id)
            .map { it.copy(approved = false) }
            .flatMapCompletable(::persistConsent)
            .doOnSubscribe { Timber.d("Revoking consent for: $id") }
    }

    fun processConsentRequestResult(id: String, approved: Boolean): Completable {
        return getConsent(id)
            .map {
                it.copy(
                    approved = approved,
                    lastDisplayTimestamp = TimeUtil.getCurrentMillis()
                )
            }
            .flatMapCompletable(::persistConsent)
            .doOnSubscribe { Timber.d("Processing consent result for: $id: $approved") }
    }

    private fun getPreferenceKey(id: String): String {
        return KEY_CONSENT_PREFIX + id
    }

    private fun getOrCreateConsentSubject(id: String): BehaviorSubject<Consent> {
        var subject = consentSubjects[id]
        if (subject == null) {
            subject = BehaviorSubject.create()
            consentSubjects[id] = subject
            preferencesManager.restoreOrDefault(getPreferenceKey(id), Consent(id))
                .doOnSuccess(subject::onNext)
                .subscribeOn(Schedulers.io())
                .subscribe()
                .addTo(managerDisposable)
        }
        return subject!!
    }

    companion object {
        const val ID_TERMS_OF_SERVICE_INFO = "terms_of_service_info"
        const val ID_TERMS_OF_SERVICE_LUCA_ID = "terms_of_service_luca_id" // March 2022, version code 96
        const val ID_TERMS_OF_SERVICE_LUCA_PAY = ID_TERMS_OF_SERVICE_LUCA_ID // already part of ID terms
        const val ID_ENABLE_CAMERA = "enable_camera"
        const val ID_IMPORT_DOCUMENT = "import_document"
        const val ID_INCLUDE_ENTRY_POLICY = "include_entry_policy"
        const val ID_POSTAL_CODE_MATCHING = "postal_code_matching"
        const val ID_OPEN_VENUE_URL = "open_venue_url"
        const val ID_ACTIVATE_LUCA_PAY = "activate_luca_pay"
        private const val KEY_CONSENT_PREFIX = "consent_"

        // consent id series to request when updating a version where ID_TERMS_OF_SERVICE_LUCA_PAY wasn't part of the ID terms during OnBoarding
        val lucaPayConsentBundle = arrayOf(ID_TERMS_OF_SERVICE_INFO, ID_TERMS_OF_SERVICE_LUCA_PAY, ID_ACTIVATE_LUCA_PAY)
    }
}
