package de.culture4life.luca.ui.checkin.flow

import android.os.Bundle
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowPage

sealed class CheckInFlowPage(override val id: Long, override val arguments: Bundle? = null) : BaseFlowPage {
    data class ConfirmCheckInPage(override val arguments: Bundle) : CheckInFlowPage(0, arguments)
    object VoluntaryCheckInPage : CheckInFlowPage(1)
    object EntryPolicyPage : CheckInFlowPage(2)
}
