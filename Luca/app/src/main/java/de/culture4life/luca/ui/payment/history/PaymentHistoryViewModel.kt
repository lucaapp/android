package de.culture4life.luca.ui.payment.history

import android.app.Application
import android.content.ActivityNotFoundException
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.toLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.rxjava3.cachedIn
import androidx.paging.rxjava3.flowable
import de.culture4life.luca.R
import de.culture4life.luca.payment.PaymentData
import de.culture4life.luca.payment.PaymentLocationData
import de.culture4life.luca.payment.PaymentManager
import de.culture4life.luca.payment.PaymentsPagingSource
import de.culture4life.luca.ui.BaseViewModel
import de.culture4life.luca.ui.ViewError
import de.culture4life.luca.ui.ViewEvent
import io.reactivex.rxjava3.core.Completable
import kotlinx.coroutines.ExperimentalCoroutinesApi

class PaymentHistoryViewModel(application: Application) : BaseViewModel(application) {

    private val paymentManager: PaymentManager = this.application.paymentManager
    val paymentLocationData: MutableLiveData<ViewEvent<PaymentLocationData>> = MutableLiveData()
    val isSignedUpForPayment: MutableLiveData<Boolean> = MutableLiveData()
    val updatePayments: MutableLiveData<ViewEvent<Unit>> = MutableLiveData()

    // Page size does not matter as we ignore it in the PagingSource but it is also used for prefetchDistance
    private val pager = Pager(PagingConfig(pageSize = PREFETCH_DISTANCE)) { PaymentsPagingSource(paymentManager) }

    @OptIn(ExperimentalCoroutinesApi::class)
    val pagedPayments: LiveData<PagingData<PaymentData>> = pager.flowable.cachedIn(viewModelScope).toLiveData()

    override fun initialize(): Completable {
        return super.initialize()
            .andThen(paymentManager.initialize(application))
            .andThen(invoke(updateSignedUpStatus()))
    }

    override fun keepDataUpdated(): Completable {
        return Completable.mergeArray(
            super.keepDataUpdated(),
            keepPaymentHistoryUpdated()
        )
    }

    private fun keepPaymentHistoryUpdated(): Completable {
        return paymentManager.getPaymentResultDeeplinks()
            .distinctUntilChanged()
            .flatMapCompletable {
                update(updatePayments, ViewEvent(Unit))
            }
    }

    fun requestSupportMail() {
        try {
            paymentManager.openLucaPaySupportMailIntent()
        } catch (exception: ActivityNotFoundException) {
            val viewError = createErrorBuilder(exception)
                .withTitle(R.string.menu_support_error_title)
                .withDescription(R.string.menu_support_error_description)
                .removeWhenShown()
                .build()
            addError(viewError)
        }
    }

    fun invokeProcessQrCodeScan(arguments: String) {
        invoke(processQrCodeScan(arguments)).subscribe()
    }

    private fun processQrCodeScan(arguments: String): Completable {
        return paymentManager.parsePaymentUrl(arguments)
            .flatMapCompletable { paymentLocationData ->
                Completable.fromCallable {
                    if (paymentLocationData.isPaymentActive) {
                        updateAsSideEffect(this.paymentLocationData, ViewEvent(paymentLocationData))
                    } else {
                        error("Payment is not active for location ${paymentLocationData.locationName}!")
                    }
                }
            }
            .doOnError {
                val viewError = ViewError.Builder(application)
                    .withTitle(application.getString(R.string.pay_not_available_headline))
                    .withDescription(application.getString(R.string.pay_not_available_text))
                    .removeWhenShown()
                    .isExpected()
                    .build()
                addError(viewError)
            }
    }

    private fun updateSignedUpStatus(): Completable {
        return paymentManager.isSignedUp()
            .flatMapCompletable { update(isSignedUpForPayment, it) }
    }

    companion object {
        private const val PREFETCH_DISTANCE = 10
    }
}
