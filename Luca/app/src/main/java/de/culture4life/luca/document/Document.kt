package de.culture4life.luca.document

import androidx.annotation.IntDef
import com.google.gson.*
import de.culture4life.luca.document.provider.eventticket.EventTicketDocument

interface Document {
    var id: String
    var importTimestamp: Long
    var expirationTimestamp: Long
    var encodedData: String
    var hashableEncodedData: String
    var isVerified: Boolean
    val deletionTimestamp: Long
    var type: Int

    fun deleteWhenExpired() = false

    @IntDef(TYPE_UNKNOWN, TYPE_FAST, TYPE_PCR, TYPE_VACCINATION, TYPE_APPOINTMENT, TYPE_RECOVERY, TYPE_EVENT_TICKET)
    @kotlin.annotation.Retention(AnnotationRetention.SOURCE)
    annotation class Type

    companion object {
        const val TYPE_UNKNOWN = 0
        const val TYPE_FAST = 1
        const val TYPE_PCR = 2
        const val TYPE_VACCINATION = 3
        const val TYPE_APPOINTMENT = 4
        const val TYPE_RECOVERY = 6
        const val TYPE_EVENT_TICKET = 7
    }

    class TypeAdapter : JsonSerializer<Document>, JsonDeserializer<Document> {
        private val gson = Gson()

        override fun serialize(item: Document, typeOfSrc: java.lang.reflect.Type, context: JsonSerializationContext): JsonElement {
            return gson.toJsonTree(item, getItemClass(item.type))
        }

        override fun deserialize(jsonElement: JsonElement, type: java.lang.reflect.Type, context: JsonDeserializationContext): Document {
            val itemType = jsonElement.asJsonObject["type"].asInt
            return gson.fromJson(jsonElement, getItemClass(itemType))
        }

        companion object {
            private fun getItemClass(type: Int): Class<out Document> {
                return when (type) {
                    TYPE_UNKNOWN,
                    TYPE_FAST,
                    TYPE_PCR,
                    TYPE_VACCINATION,
                    TYPE_APPOINTMENT,
                    TYPE_RECOVERY -> CovidDocument::class.java
                    TYPE_EVENT_TICKET -> EventTicketDocument::class.java
                    else -> throw IllegalStateException("Can't serialize unknown type $type")
                }
            }
        }
    }
}
