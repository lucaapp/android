package de.culture4life.luca.ui.myluca.listitems

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import de.culture4life.luca.R
import de.culture4life.luca.document.provider.eventticket.EventTicketDocument

class EventTicketItem(override val document: EventTicketDocument) : MyLucaListItem(), ExpandableListItem, DeletableDocumentListItem {

    override val timestamp: Long
        get() = document.startTimestamp

    override var isExpanded: Boolean = false

    override val deleteButtonText: Int
        get() = R.string.action_delete

    @get:StringRes
    val readableCategory: Int by lazy {
        when (document.category) {
            EventTicketDocument.Category.OTHER -> R.string.ticket_category_others
            EventTicketDocument.Category.PARTY -> R.string.ticket_category_party
            EventTicketDocument.Category.CLASSIC -> R.string.ticket_category_classic
            EventTicketDocument.Category.CULTURE -> R.string.ticket_category_culture
            EventTicketDocument.Category.EXHIBITION -> R.string.ticket_category_exhibition
            EventTicketDocument.Category.CONFERENCE -> R.string.ticket_category_conference
            EventTicketDocument.Category.MOVIE -> R.string.ticket_category_movie
            EventTicketDocument.Category.FAMILY -> R.string.ticket_category_family
            EventTicketDocument.Category.SPORT -> R.string.ticket_category_sport
            EventTicketDocument.Category.MUSIC -> R.string.ticket_category_music
        }
    }

    @get:DrawableRes
    val cardIcon: Int by lazy {
        when (document.category) {
            EventTicketDocument.Category.OTHER -> R.drawable.ic_more_info
            EventTicketDocument.Category.PARTY -> R.drawable.ic_party
            EventTicketDocument.Category.CLASSIC -> R.drawable.ic_museum
            EventTicketDocument.Category.CULTURE -> R.drawable.ic_statue
            EventTicketDocument.Category.EXHIBITION -> R.drawable.ic_eye
            EventTicketDocument.Category.CONFERENCE -> R.drawable.ic_conference
            EventTicketDocument.Category.MOVIE -> R.drawable.ic_movie
            EventTicketDocument.Category.FAMILY -> R.drawable.ic_family
            EventTicketDocument.Category.SPORT -> R.drawable.ic_sport
            EventTicketDocument.Category.MUSIC -> R.drawable.ic_music
        }
    }

    @get:ColorRes
    val cardColor: Int by lazy {
        when (document.category) {
            EventTicketDocument.Category.OTHER -> R.color.event_ticket_card_category_other
            EventTicketDocument.Category.PARTY -> R.color.event_ticket_card_category_party
            EventTicketDocument.Category.CLASSIC -> R.color.event_ticket_card_category_classic
            EventTicketDocument.Category.CULTURE -> R.color.event_ticket_card_category_culture
            EventTicketDocument.Category.EXHIBITION -> R.color.event_ticket_card_category_exhibition
            EventTicketDocument.Category.CONFERENCE -> R.color.event_ticket_card_category_conference
            EventTicketDocument.Category.MOVIE -> R.color.event_ticket_card_category_movie
            EventTicketDocument.Category.FAMILY -> R.color.event_ticket_card_category_family
            EventTicketDocument.Category.SPORT -> R.color.event_ticket_card_category_sport
            EventTicketDocument.Category.MUSIC -> R.color.event_ticket_card_category_music
        }
    }
}
