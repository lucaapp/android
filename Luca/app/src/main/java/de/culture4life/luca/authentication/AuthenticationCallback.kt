package de.culture4life.luca.authentication

fun interface AuthenticationCallback {
    fun onAuthenticationResult(result: AuthenticationResult)
}
