package de.culture4life.luca.history

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CheckOutItem(
    @SerializedName("children")
    @Expose
    var children: List<String>? = null
) : HistoryItem() {
    init {
        setType(TYPE_CHECK_OUT)
    }
}
