package de.culture4life.luca.ui.checkin.flow.children

import android.app.Application
import androidx.lifecycle.MutableLiveData
import de.culture4life.luca.ui.ViewEvent
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowChildViewModel
import de.culture4life.luca.ui.checkin.flow.CheckInFlowViewModel
import io.reactivex.rxjava3.core.Completable

class EntryPolicyViewModel(app: Application) : BaseFlowChildViewModel<CheckInFlowViewModel>(app) {

    val onConsentValueChanged: MutableLiveData<ViewEvent<Boolean>> = MutableLiveData()

    fun onActionButtonClicked(shareEntryPolicyStatus: Boolean, alwaysShare: Boolean) {
        invoke(
            persistAlwaysShareEntryPolicyStatus(alwaysShare)
                .doOnComplete {
                    sharedViewModel!!.let {
                        it.shareEntryPolicyState = shareEntryPolicyStatus
                        it.navigateToNext()
                    }
                }
        ).subscribe()
    }

    fun onEntryPolicySharingConsented() {
        updateAsSideEffect(onConsentValueChanged, ViewEvent(true))
    }

    fun onEntryPolicySharingNotConsented() {
        updateAsSideEffect(onConsentValueChanged, ViewEvent(false))
    }

    private fun persistAlwaysShareEntryPolicyStatus(alwaysShare: Boolean): Completable {
        return preferencesManager.persist(KEY_ALWAYS_SHARE_ENTRY_POLICY_STATUS, alwaysShare)
    }

    companion object {
        const val KEY_ALWAYS_SHARE_ENTRY_POLICY_STATUS = "always_share_entry_policy_status"
    }
}
