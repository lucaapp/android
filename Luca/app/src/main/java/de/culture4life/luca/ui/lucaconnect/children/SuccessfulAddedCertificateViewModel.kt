package de.culture4life.luca.ui.lucaconnect.children

import android.app.Application
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowChildViewModel
import de.culture4life.luca.ui.lucaconnect.LucaConnectBottomSheetViewModel

class SuccessfulAddedCertificateViewModel(app: Application) : BaseFlowChildViewModel<LucaConnectBottomSheetViewModel>(app)
