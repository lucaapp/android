package de.culture4life.luca.ui.myluca.viewholders

import android.graphics.Bitmap
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.google.zxing.EncodeHintType
import de.culture4life.luca.databinding.ItemMyLucaEventTicketBinding
import de.culture4life.luca.ui.myluca.listitems.EventTicketItem
import de.culture4life.luca.util.getReadableDate
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import net.glxn.qrgen.android.QRCode

class EventTicketViewHolder(val binding: ItemMyLucaEventTicketBinding) : RecyclerView.ViewHolder(binding.root) {

    private var generatingQrCodeDisposable: Disposable? = null

    fun show(item: EventTicketItem) {
        val context = binding.root.context
        binding.itemTitleTextView.text = item.document.eventName
        binding.date.text = context.getReadableDate(item.document.startTimestamp)
        binding.location.text = item.document.locationName
        binding.issuer.text = item.document.issuer
        binding.category.text = context.getString(item.readableCategory)
        binding.icon.setImageResource(item.cardIcon)
        binding.ticketId.text = item.document.id
        binding.verificationId.text = item.document.verificationId
        generateQrCodeAndSetToImageView(item.document.encodedData, binding.qrCodeImageView)
        binding.eventTicketCardView.setCardBackgroundColor(ContextCompat.getColor(context, item.cardColor))
        binding.collapseLayout.isVisible = item.isExpanded
        binding.collapseIndicator.rotationX = if (item.isExpanded) 180F else 0F
    }

    fun cleanUp() {
        if (generatingQrCodeDisposable?.isDisposed == false) generatingQrCodeDisposable?.dispose()
    }

    private fun generateQrCodeAndSetToImageView(data: String, imageView: ImageView) {
        cleanUp()
        generatingQrCodeDisposable = generateQrCode(data)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ imageView.setImageBitmap(it) }, {})
    }

    private fun generateQrCode(data: String): Single<Bitmap> {
        return Single.fromCallable {
            QRCode.from(data)
                .withSize(500, 500)
                .withHint(EncodeHintType.MARGIN, 0)
                .bitmap()
        }
    }

    fun setListeners(
        expandClickListener: View.OnClickListener? = null,
        deleteClickListener: View.OnClickListener? = null,
    ) {
        deleteClickListener?.let { binding.deleteItemButton.setOnClickListener(it) }
        expandClickListener?.let { binding.root.setOnClickListener(it) }
    }
}
