package de.culture4life.luca.crypto

class DailyKeyExpiredException(message: String) : DailyKeyUnavailableException(message)
