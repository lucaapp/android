package de.culture4life.luca.ui.payment

import android.app.Application
import android.os.Bundle
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import de.culture4life.luca.R
import de.culture4life.luca.payment.PaymentAmounts
import de.culture4life.luca.payment.PaymentData
import de.culture4life.luca.payment.PaymentLocationData
import de.culture4life.luca.ui.ViewError
import de.culture4life.luca.ui.ViewEvent
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowViewModel
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import java.util.concurrent.TimeUnit

class PaymentFlowViewModel(application: Application, private val savedStateHandle: SavedStateHandle) : BaseFlowViewModel(application) {

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    var paymentLocationData: PaymentLocationData? = null

    val paymentAmounts: MutableLiveData<PaymentAmounts> = savedStateHandle.getLiveData(SAVED_STATE_PAYMENT_AMOUNT, PaymentAmounts(0))
    val paymentRequestId: MutableLiveData<String> = savedStateHandle.getLiveData(SAVED_STATE_PAYMENT_REQUEST_ID)
    val paymentResult: MutableLiveData<PaymentData> = savedStateHandle.getLiveData(SAVED_STATE_PAYMENT_RESULT)
    val activatePartialPaymentPage: MutableLiveData<Boolean> = MutableLiveData(false)

    override fun initialize(): Completable {
        return super.initialize()
            .andThen(updatePages())
    }

    private fun updatePages(): Completable {
        return Completable.fromCallable {
            pages.apply {
                clear()
                add(PaymentFlowPage.AmountPage)
                add(PaymentFlowPage.PartialAmountPage)
                add(PaymentFlowPage.PaymentTipPage)
                add(PaymentFlowPage.WebAppPage)
                add(PaymentFlowPage.PaymentResultPage)
            }
        }
            .doOnComplete {
                updateAsSideEffect(onPagesUpdated, ViewEvent(pages))
            }
    }

    override fun processArguments(arguments: Bundle?): Completable {
        return super.processArguments(arguments)
            .andThen(
                Completable.fromAction {
                    requireNotNull(arguments)
                    paymentLocationData = arguments.getSerializable(ARGUMENT_PAYMENT_LOCATION_DATA_KEY)!! as PaymentLocationData
                }
            )
    }

    fun getPaymentLocationData(): Single<PaymentLocationData> {
        return Single.defer {
            if (paymentLocationData != null) {
                Single.just(paymentLocationData!!)
            } else {
                getPaymentLocationData().delaySubscription(10, TimeUnit.MILLISECONDS)
            }
        }
    }

    fun onPaymentRequestExpired() {
        val error = ViewError.Builder(application)
            .withTitle(R.string.pay_error_expired_title)
            .withDescription(R.string.pay_error_expired_text)
            .removeWhenShown()
            .build()
        application.showError(error)
        onFinishFlow()
    }

    override fun onFinishFlow() {
        dismissBottomSheet()
    }

    companion object {
        private const val SAVED_STATE_PAYMENT_AMOUNT = "payment_amount"
        private const val SAVED_STATE_PAYMENT_RESULT = "payment_result"
        private const val SAVED_STATE_PAYMENT_REQUEST_ID = "payment_request_id"
        const val ARGUMENT_PAYMENT_LOCATION_DATA_KEY = "payment_location_data"
    }
}
