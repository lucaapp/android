package de.culture4life.luca.ui.compound

import android.content.Context
import android.util.AttributeSet
import android.widget.Checkable
import android.widget.CompoundButton
import androidx.core.content.withStyledAttributes
import de.culture4life.luca.R
import de.culture4life.luca.databinding.ViewAccountPageSwitchBinding

class AccountPageActionItemSwitchView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defaultStyle: Int = 0
) : AccountPageActionItemBaseView(context, attributeSet, defaultStyle), Checkable {

    private val binding: ViewAccountPageSwitchBinding = ViewAccountPageSwitchBinding.inflate(inflater, this)

    var text: String
        get() = binding.toggleTextView.text.toString()
        set(value) {
            binding.toggleTextView.text = value
        }

    override fun isChecked() = binding.toggle.isChecked

    override fun setChecked(checked: Boolean) {
        binding.toggle.isChecked = checked
    }

    override fun toggle() {
        binding.toggle.toggle()
    }

    fun setOnCheckedChangeListener(onCheckedChangeListener: CompoundButton.OnCheckedChangeListener) {
        binding.toggle.setOnCheckedChangeListener(onCheckedChangeListener)
    }

    override fun jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState()
        binding.toggle.jumpDrawablesToCurrentState()
    }

    override fun setOnClickListener(l: OnClickListener?) {
        super.setOnClickListener(l)

        // When a click listener for the whole View is set we want to disable the default click/touch
        // behaviour of the toggle, otherwise they would be out of sync if the click listener that has
        // been set for example first shows an alert before changing the toggles state
        binding.toggle.isClickable = false
        binding.toggle.background = null
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        binding.toggle.isEnabled = enabled
    }

    init {
        setOnClickListener { binding.toggle.toggle() }
        context.withStyledAttributes(attributeSet, R.styleable.AccountPageButtonView) {
            text = getString(R.styleable.AccountPageButtonView_text).orEmpty()
        }
    }
}
