package de.culture4life.luca.payment

import androidx.paging.PagingState
import androidx.paging.rxjava3.RxPagingSource
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers

/*
 TODO Check if it should be combined with some local caching, maybe room database or something like that
 @see https://developer.android.com/topic/libraries/architecture/paging/v3-network-db
 */
class PaymentsPagingSource(private val paymentManager: PaymentManager) : RxPagingSource<String, PaymentData>() {

    override fun getRefreshKey(state: PagingState<String, PaymentData>): String? {
        // At the moment refresh starts from beginning again (as the user is on the top of the list anyway when triggering a refresh)
        return null
    }

    override fun loadSingle(params: LoadParams<String>): Single<LoadResult<String, PaymentData>> {
        return paymentManager.fetchPaymentHistory(params.key)
            .subscribeOn(Schedulers.io())
            .map { toLoadResult(it) }
            .onErrorReturn { LoadResult.Error(it) }
    }

    private fun toLoadResult(responseData: PagedPaymentHistory): LoadResult<String, PaymentData> {
        return LoadResult.Page(data = responseData.payments, prevKey = null, nextKey = responseData.nextPageCursor)
    }
}
