package de.culture4life.luca.ui.payment.children

import android.util.TypedValue
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import de.culture4life.luca.R
import de.culture4life.luca.databinding.FragmentPaymentResultBinding
import de.culture4life.luca.payment.PaymentData
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowChildFragment
import de.culture4life.luca.ui.payment.PaymentFlowViewModel
import de.culture4life.luca.ui.payment.raffle.PaymentRaffleBottomSheetFragment
import de.culture4life.luca.ui.payment.raffle.PaymentRaffleBottomSheetFragment.Companion.PAYMENT_RAFFLE_REQUEST_CODE
import de.culture4life.luca.ui.payment.raffle.PaymentRaffleBottomSheetFragment.Companion.PAYMENT_RAFFLE_RESULT_CODE
import de.culture4life.luca.util.NumberUtil

class PaymentResultFragment : BaseFlowChildFragment<PaymentResultViewModel, PaymentFlowViewModel>() {

    private lateinit var binding: FragmentPaymentResultBinding

    override fun getSharedViewModelClass() = PaymentFlowViewModel::class.java
    override fun getViewModelClass() = PaymentResultViewModel::class.java
    override fun getViewBinding() = FragmentPaymentResultBinding.inflate(layoutInflater).also { binding = it }

    override fun initializeViews() {
        super.initializeViews()
        initializeObservers()
    }

    private fun initializeObservers() {
        observe(viewModel.paymentResult) { paymentResult ->
            val success = paymentResult.isSuccessful
            binding.successLayout.root.isVisible = success
            binding.errorLayout.root.isVisible = !success
            if (success) {
                initializeSuccessViews(paymentResult)
            } else {
                initializeFailureViews()
            }
        }

        observe(viewModel.raffleCampaignActive) { binding.successLayout.raffleButton.isVisible = it }
    }

    private fun initializeSuccessViews(payment: PaymentData) {
        with(binding.successLayout) {
            nextButton.setOnClickListener { viewModel.onNextButtonClicked() }
            raffleButton.setOnClickListener { showRaffleBottomSheet() }
            locationNameTextView.text = payment.locationName
            codeValueTextView.text = payment.paymentVerifier
            sumTextView.text = getString(R.string.euro_amount, NumberUtil.toCurrencyAmountString(payment.totalAmount))
            tipValueTextView.text = getString(R.string.euro_amount, NumberUtil.toCurrencyAmountString(payment.tipAmount))
            amountValueTextView.text = getString(R.string.euro_amount, NumberUtil.toCurrencyAmountString(payment.invoiceAmount))

            if (payment.isDiscounted) {
                amountValueTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.payment_promotion, application.theme))
                amountValueTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.font_size_default_larger))
                originAmountValueTextView.text = getString(R.string.euro_amount, NumberUtil.toCurrencyAmountString(payment.originalInvoiceAmount))
                originAmountValueTextView.isVisible = true
            } else {
                amountValueTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.white, application.theme))
                amountValueTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.font_size_default))
                originAmountValueTextView.visibility = View.GONE
            }
        }

        childFragmentManager.setFragmentResultListener(PAYMENT_RAFFLE_REQUEST_CODE, this) { _, result ->
            if (result[PAYMENT_RAFFLE_RESULT_CODE] == true) {
                viewModel.onNextButtonClicked()
            }
        }
    }

    private fun initializeFailureViews() {
        binding.errorLayout.retryButton.setOnClickListener { viewModel.onRetryButtonClicked() }
    }

    private fun showRaffleBottomSheet() {
        val paymentId = viewModel.paymentResult.value?.id ?: return
        PaymentRaffleBottomSheetFragment.newInstance(paymentId)
            .show(childFragmentManager, PaymentRaffleBottomSheetFragment.TAG)
    }

    companion object {
        fun newInstance() = PaymentResultFragment()
    }
}
