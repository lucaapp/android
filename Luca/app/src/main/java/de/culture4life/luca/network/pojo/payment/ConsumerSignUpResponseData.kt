package de.culture4life.luca.network.pojo.payment

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Holds the data of the for payment signed up (registered) consumer.
 *
 * @see [Sign up consumer](https://app.luca-app.de/pay/api/v1/swagger/#/Consumers/post_consumers_signup)
 */
data class ConsumerSignUpResponseData(

    @Expose
    @SerializedName("uuid")
    val uuid: String,

    @Expose
    @SerializedName("username")
    val username: String,

    @Expose
    @SerializedName("revocationCode")
    val revocationCode: String

)
