package de.culture4life.luca.network.pojo

import com.google.gson.annotations.SerializedName

/**
 * Holds encrypted and authenticated [TransferData] to be uploaded to the luca server,
 * allowing health departments to reconstruct an infected guest's check-in history.
 *
 * @see [Security Overview: Glossary](https://www.luca-app.de/securityoverview/properties/secrets.html.term-guest-data-transfer-object)
 *
 * @see [Security Overview: Reconstructing the Infected Guest’s Check-In History](https://www.luca-app.de/securityoverview/processes/tracing_access_to_history.html.reconstructing-the-infected-guest-s-check-in-history)
 */
data class DataTransferRequestData(
    @SerializedName("data")
    val encryptedContactData: String,

    @SerializedName("iv")
    val iv: String,

    @SerializedName("publicKey")
    val guestKeyPairPublicKey: String,

    @SerializedName("mac")
    val mac: String,

    @SerializedName("keyId")
    val dailyKeyPairPublicKeyId: Int
)
