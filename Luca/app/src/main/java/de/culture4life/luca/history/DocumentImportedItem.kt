package de.culture4life.luca.history

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import de.culture4life.luca.document.CovidDocument

data class DocumentImportedItem(
    @SerializedName("testResult")
    @Expose
    val document: CovidDocument
) : HistoryItem() {
    init {
        setType(TYPE_DOCUMENT_IMPORTED)
    }
}
