package de.culture4life.luca.ui.payment.children

import android.text.Editable
import android.text.TextWatcher
import de.culture4life.luca.util.removeNonDigits

class PaymentAmountTextWatcher(
    val amountCallback: (amountInput: String) -> Unit
) : TextWatcher {

    private var amountInput = ""

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    override fun afterTextChanged(editable: Editable) {
        if (editable.toString() != amountInput) {
            val digits = editable.toString().removeNonDigits()
            amountInput = digits
            amountCallback(amountInput)
        }
    }
}
