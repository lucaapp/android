package de.culture4life.luca.ui.qrcode.standalone

import androidx.annotation.StringRes
import androidx.annotation.VisibleForTesting
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.setFragmentResult
import de.culture4life.luca.databinding.FragmentQrCodeScannerStandaloneBinding
import de.culture4life.luca.ui.BaseQrCodeFragment
import de.culture4life.luca.ui.base.BaseBottomSheetDialogFragment
import io.reactivex.rxjava3.core.Completable

class QrCodeScannerBottomSheetFragment : BaseBottomSheetDialogFragment<QrCodeScannerViewModel>() {

    private lateinit var binding: FragmentQrCodeScannerStandaloneBinding
    private lateinit var cameraFragment: BaseQrCodeFragment

    override fun getViewBinding() = FragmentQrCodeScannerStandaloneBinding.inflate(layoutInflater).also { binding = it }
    override fun getViewModelClass() = QrCodeScannerViewModel::class.java

    override fun initializeViews() {
        super.initializeViews()
        initializeTextViewContent()
        initializeCameraPreview()
        binding.cancelButton.setOnClickListener { dismiss() }
    }

    private fun initializeTextViewContent() {
        val arguments = requireArguments()
        arguments.getString(BUNDLE_TITLE_KEY).let {
            binding.layoutScanQrCode.scanQrCodeTitleTextView.text = it
            binding.layoutScanQrCode.scanQrCodeTitleTextView.isVisible = !it.isNullOrBlank()
        }
        arguments.getString(BUNDLE_DESCRIPTION_KEY).let {
            binding.layoutScanQrCode.scanQrCodeDescriptionTextView.text = it
            binding.layoutScanQrCode.scanQrCodeDescriptionTextView.isVisible = !it.isNullOrBlank()
        }
    }

    private fun initializeCameraPreview() {
        cameraFragment = binding.layoutScanQrCode.qrCodeScanner.getFragment()
        cameraFragment.setBarcodeResultCallback(::processBarcode)
        observe(viewModel.isLoading) { isLoading ->
            cameraFragment.showLoading(isLoading)
        }
        cameraFragment.requestShowCameraPreview()
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun processBarcode(barcodeData: String): Completable {
        return Completable.fromCallable {
            setFragmentResult(REQUEST_KEY, bundleOf(BUNDLE_RESULT_KEY to barcodeData))
            dismiss()
        }
    }

    companion object {

        const val TAG = "QrCodeScannerBottomSheetFragment"
        const val REQUEST_KEY = "qr_code_scan"
        const val BUNDLE_RESULT_KEY = "qr_code_result"

        private const val BUNDLE_TITLE_KEY = "title"
        private const val BUNDLE_DESCRIPTION_KEY = "description"

        fun newInstance(
            @StringRes
            title: Int? = null,
            @StringRes
            description: Int? = null
        ): QrCodeScannerBottomSheetFragment {
            return QrCodeScannerBottomSheetFragment().apply {
                arguments = bundleOf(
                    BUNDLE_TITLE_KEY to title,
                    BUNDLE_DESCRIPTION_KEY to description,
                )
            }
        }
    }
}
