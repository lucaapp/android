package de.culture4life.luca.util

import de.culture4life.luca.network.NetworkManager.Companion.MAXIMUM_REQUEST_RETRY_COUNT
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableTransformer

object ObservableUtil {

    @JvmStatic
    fun <T : Any> retryWhen(
        throwableClass: Class<out Throwable>,
        maximumRetries: Int = MAXIMUM_REQUEST_RETRY_COUNT
    ): ObservableTransformer<T, T> {
        return ObservableTransformer<T, T> { observable ->
            observable.retryWhen { errors ->
                errors.zipWith(
                    Observable.range(0, maximumRetries + 1),
                    { error, attempt -> Pair(error, attempt) }
                ).flatMap { errorAndAttempt ->
                    val isExpectedError = errorAndAttempt.first.isCause(throwableClass)
                    val hasAttempts = errorAndAttempt.second < maximumRetries
                    if (isExpectedError && hasAttempts) {
                        Observable.just(errorAndAttempt.first) // retry
                    } else {
                        Observable.error(errorAndAttempt.first) // don't retry
                    }
                }
            }
        }
    }
}

fun <T : Any> Observable<T>.retryWhen(
    throwableClass: Class<out Throwable>,
    maximumRetries: Int = MAXIMUM_REQUEST_RETRY_COUNT
): Observable<T> {
    return this.compose(ObservableUtil.retryWhen(throwableClass, maximumRetries))
}
