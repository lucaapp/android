package de.culture4life.luca.payment

import java.io.Serializable

data class PaymentLocationData(
    val locationId: String,
    val locationName: String,
    val table: String? = null,
    val isPaymentActive: Boolean
) : Serializable
