package de.culture4life.luca.ui.payment.history.recycler.diffutil

import androidx.recyclerview.widget.DiffUtil
import de.culture4life.luca.payment.PaymentData

class PaymentsDiffCallback : DiffUtil.ItemCallback<PaymentData>() {
    override fun areContentsTheSame(oldItem: PaymentData, newItem: PaymentData) = oldItem == newItem
    override fun areItemsTheSame(oldItem: PaymentData, newItem: PaymentData) = oldItem.id == newItem.id
}
