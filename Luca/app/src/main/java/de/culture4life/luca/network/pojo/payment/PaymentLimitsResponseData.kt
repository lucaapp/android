package de.culture4life.luca.network.pojo.payment

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class PaymentLimitsResponseData(

    @SerializedName("minInvoiceAmount")
    val minimumInvoiceAmount: BigDecimal,

    @SerializedName("maxInvoiceAmount")
    val maximumInvoiceAmount: BigDecimal,

    @SerializedName("maxTipAmount")
    val maximumTipAmount: BigDecimal

)
