package de.culture4life.luca.ui.myluca.listitems

import androidx.annotation.StringRes
import de.culture4life.luca.document.Document

interface DeletableListItem {
    @get:StringRes
    val deleteButtonText: Int
}

interface DeletableDocumentListItem : DeletableListItem {
    val document: Document
}
