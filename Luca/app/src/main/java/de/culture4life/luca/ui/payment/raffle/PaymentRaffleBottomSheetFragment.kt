package de.culture4life.luca.ui.payment.raffle

import android.view.inputmethod.EditorInfo
import androidx.core.os.bundleOf
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.setFragmentResult
import de.culture4life.luca.R
import de.culture4life.luca.databinding.BottomSheetPayRaffleBinding
import de.culture4life.luca.ui.base.BaseBottomSheetDialogFragment
import de.culture4life.luca.util.hideKeyboard
import de.culture4life.luca.util.setHtmlStringResource

class PaymentRaffleBottomSheetFragment : BaseBottomSheetDialogFragment<PaymentRaffleViewModel>() {

    private lateinit var binding: BottomSheetPayRaffleBinding

    override fun getViewModelClass() = PaymentRaffleViewModel::class.java

    override fun getViewBinding() = BottomSheetPayRaffleBinding.inflate(layoutInflater).also { binding = it }

    override fun initializeViews() {
        super.initializeViews()
        binding.consentTextView.setHtmlStringResource(R.string.pay_raffle_consent_terms_accept_html)
        binding.emailEditText.setOnEditorActionListener { view, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_SEND) {
                view.hideKeyboard()
                binding.emailEditText.clearFocus()
                binding.consentCheckBox.requestFocus()
                true
            } else {
                false
            }
        }
        initializeObservers()
    }

    private fun initializeObservers() {
        observe(viewModel.confirmButtonEnabled) { binding.primaryActionButton.isEnabled = it }
        observe(viewModel.checkboxChecked) { binding.consentCheckBox.isChecked = it }
        observe(viewModel.email) {
            // Only update EditText if something changed to prevent infinite loop
            if (it != binding.emailEditText.text.toString()) {
                binding.emailEditText.setText(it)
            }
        }
        observe(viewModel.success) {
            if (it.isNotHandled) {
                it.isHandled = true
                setFragmentResult(PAYMENT_RAFFLE_REQUEST_CODE, bundleOf(PAYMENT_RAFFLE_RESULT_CODE to true))
                dismiss()
            }
        }

        binding.cancelButton.setOnClickListener { dismiss() }
        binding.consentCheckBox.setOnCheckedChangeListener { _, isChecked -> viewModel.checkboxChecked.value = isChecked }
        binding.emailEditText.doAfterTextChanged { viewModel.email.value = it.toString() }
        binding.primaryActionButton.setOnClickListener { viewModel.onConfirmButtonClicked() }
    }

    companion object {
        const val TAG = "PaymentRaffleBottomSheetFragment"
        const val PAYMENT_RAFFLE_REQUEST_CODE = "payment_raffle_request"
        const val PAYMENT_RAFFLE_RESULT_CODE = "payment_raffle_result"
        const val ARGUMENT_PAYMENT_ID = "payment_id"

        fun newInstance(paymentId: String) = PaymentRaffleBottomSheetFragment().apply {
            arguments = bundleOf(ARGUMENT_PAYMENT_ID to paymentId)
        }
    }
}
