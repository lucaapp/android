package de.culture4life.luca.util

import android.net.Uri
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single
import java.util.*

object LucaUrlUtil {

    /**
     * Checks if given uri string contains correct host and path for luca Uris
     */
    @JvmStatic
    fun isLucaUrl(url: String, startPath: String = "/webapp"): Boolean {
        val parsedUri = try {
            Uri.parse(url)
        } catch (throwable: Throwable) {
            return false
        }

        val isCorrectHost = parsedUri.host.orEmpty().let { host -> host == "luca-app.de" || host.endsWith(".luca-app.de") }
        val isCorrectPath = parsedUri.path.orEmpty().startsWith(startPath)
        val isCorrectScheme = parsedUri.scheme.orEmpty().let { scheme -> scheme == "http" || scheme == "https" || scheme == "luca" }

        return isCorrectHost && isCorrectPath && isCorrectScheme
    }

    @JvmStatic
    fun isCheckInDeeplink(url: String) = isSelfCheckIn(url) || isPrivateMeeting(url)

    @JvmStatic
    fun isMyLucaDeeplink(url: String) = isTestResult(url) || isAppointment(url) || isEventTicket(url)

    @JvmStatic
    fun isSelfCheckIn(url: String): Boolean {
        return isLucaUrl(url) && try {
            val parsedUri = Uri.parse(url)
            UUID.fromString(parsedUri.lastPathSegment)
            true
        } catch (throwable: Throwable) {
            false
        }
    }

    @JvmStatic
    fun isPrivateMeeting(url: String): Boolean {
        return isLucaUrl(url) && url.contains("/meeting")
    }

    @JvmStatic
    fun isAppointment(url: String): Boolean {
        return isLucaUrl(url) && url.contains("/appointment")
    }

    @JvmStatic
    fun isTestResult(url: String): Boolean {
        return isLucaUrl(url) && url.contains("/testresult/#")
    }

    @JvmStatic
    fun getEncodedAdditionalDataFromVenueUrlIfAvailable(url: String): Maybe<String> {
        return Maybe.fromCallable {
            val startIndex = url.indexOf('#') + 1
            if (startIndex < 1 || startIndex >= url.length) {
                return@fromCallable null
            }
            var endIndex = url.length
            if (url.contains("/CWA")) {
                endIndex = url.indexOf("/CWA")
            }
            url.substring(startIndex, endIndex)
        }
    }

    @JvmStatic
    fun getAdditionalDataFromVenueUrlIfAvailable(url: String): Single<String> {
        return getEncodedAdditionalDataFromVenueUrlIfAvailable(url)
            .map { String(it.decodeFromBase64()) }
            .defaultIfEmpty("")
    }

    @JvmStatic
    fun isEventTicket(url: String): Boolean {
        return isLucaUrl(url) && url.contains("/tickets/#")
    }

    @JvmStatic
    fun isWebAppPaymentResult(url: String): Boolean {
        return isLucaUrl(url, "/pay") &&
            containsAnyPaymentStatus(url) &&
            url.contains("?checkoutId=")
    }

    private fun containsAnyPaymentStatus(url: String): Boolean {
        return url.contains("complete_checkout") ||
            url.contains("complete_payment") ||
            url.contains("cancel_checkout") ||
            url.contains("error_payment")
    }

    fun getCheckoutId(url: String): String {
        require(isWebAppPaymentResult(url))
        return Uri.parse(url).getQueryParameter("checkoutId")!!
    }
}
