package de.culture4life.luca.network

class NetworkUnavailableException(message: String) : Exception(message)
