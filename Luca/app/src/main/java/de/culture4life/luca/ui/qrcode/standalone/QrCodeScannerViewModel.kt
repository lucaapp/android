package de.culture4life.luca.ui.qrcode.standalone

import android.app.Application
import de.culture4life.luca.ui.base.BaseBottomSheetViewModel

class QrCodeScannerViewModel(application: Application) : BaseBottomSheetViewModel(application)
