package de.culture4life.luca.ui.payment.children

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.MutableLiveData
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.R
import de.culture4life.luca.network.pojo.payment.CampaignResponseData
import de.culture4life.luca.network.pojo.payment.OpenPaymentRequestResponseData
import de.culture4life.luca.payment.PaymentAmounts
import de.culture4life.luca.payment.PaymentManager
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowChildViewModel
import de.culture4life.luca.ui.payment.PaymentFlowViewModel
import de.culture4life.luca.util.*
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import timber.log.Timber
import java.util.concurrent.TimeUnit

class PaymentAmountViewModel(application: Application) : BaseFlowChildViewModel<PaymentFlowViewModel>(application) {

    private val paymentManager: PaymentManager = this.application.paymentManager
    private var minimumAmount = MINIMUM_AMOUNT
    private var maximumAmount = MAXIMUM_AMOUNT

    private var isViewResumed = true

    val locationName: MutableLiveData<String> = MutableLiveData()
    val paymentAmounts: MutableLiveData<PaymentAmounts> = MutableLiveData(PaymentAmounts(INITIAL_AMOUNT))
    val warningText: MutableLiveData<String?> = MutableLiveData()
    val isFixedAmount: MutableLiveData<Boolean> = MutableLiveData(false)
    val isPartialAmountSupported: MutableLiveData<Boolean> = MutableLiveData(false)
    val showKeyboard: MutableLiveData<Boolean> = MutableLiveData(true)

    override fun initialize(): Completable {
        return super.initialize()
            .andThen(paymentManager.initialize(application))
            .andThen(invoke(updateAmount(INITIAL_AMOUNT)))
            .andThen(invoke(updatePaymentLimits()))
            .andThen(invoke(updateDiscounts()))
    }

    override fun keepDataUpdated(): Completable {
        return Completable.mergeArray(
            super.keepDataUpdated(),
            keepPaymentRequestDataUpdated(),
            invoke(updateLocationName())
        )
    }

    private fun keepPaymentRequestDataUpdated(): Completable {
        return Single.defer { sharedViewModel!!.getPaymentLocationData() }
            .flatMapCompletable { paymentLocationData ->
                val locationId = paymentLocationData.locationId
                val table = paymentLocationData.table
                pollOpenPaymentRequests(locationId, table)
                    .flatMapCompletable(::updatePaymentRequestData)
            }
    }

    private fun pollOpenPaymentRequests(locationId: String, table: String?): Flowable<OpenPaymentRequestResponseData> {
        return Flowable.interval(0, PAYMENT_REQUEST_POLLING_INTERVAL, TimeUnit.MILLISECONDS, Schedulers.io())
            .filter { isViewResumed }
            .flatMapMaybe {
                paymentManager.fetchOpenPaymentRequest(locationId, table)
                    .switchIfEmpty(
                        Maybe.defer {
                            val startedWithPaymentRequest = isFixedAmount.value == true
                            if (startedWithPaymentRequest) {
                                sharedViewModel!!.onPaymentRequestExpired()
                            }
                            Maybe.empty()
                        }
                    )
                    .doOnError { Timber.w("Unable to fetch open payment request for table $table at $locationId: $it") }
                    .onErrorComplete()
            }
            .onBackpressureBuffer(3)
            .distinctUntilChanged()
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun updatePaymentRequestData(paymentRequestData: OpenPaymentRequestResponseData): Completable {
        return Completable.mergeArray(
            updateAmount(
                invoiceAmount = paymentRequestData.invoiceAmount.toCurrencyAmountNumber(),
                openAmount = paymentRequestData.openAmount?.toCurrencyAmountNumber()
            ),
            update(sharedViewModel!!.paymentRequestId, paymentRequestData.id),
            updateIfRequired(isFixedAmount, true),
            updateIfRequired(isPartialAmountSupported, paymentRequestData.partialAmountsSupported)
        )
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun updateAmount(invoiceAmount: Int, openAmount: Int? = null): Completable {
        return Completable.fromAction {
            val amountIsGiven = invoiceAmount != INITIAL_AMOUNT
            updateAsSideEffectIfRequired(isFixedAmount, amountIsGiven)
            updateAsSideEffectIfRequired(showKeyboard, !amountIsGiven)
            updateAsSideEffectIfRequired(paymentAmounts, paymentAmounts.value!!.copy(invoiceAmount = invoiceAmount, openAmount = openAmount))
        }
    }

    private fun updateLocationName(): Completable {
        return Single.defer { sharedViewModel!!.getPaymentLocationData() }
            .map { it.locationName }
            .doOnSuccess { require(it.isNotEmpty()) }
            .onErrorReturnItem(application.getString(R.string.unknown))
            .flatMapCompletable { updateInstantly(locationName, it) }
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun updatePaymentLimits(): Completable {
        return paymentManager.fetchPaymentLimits()
            .doOnSuccess {
                minimumAmount = it.minimumInvoiceAmount.toCurrencyAmountNumber()
                maximumAmount = it.maximumInvoiceAmount.toCurrencyAmountNumber()
            }
            .ignoreElement()
            .retryWhenWithDelay(TimeUnit.SECONDS.toMillis(1)) { ThrowableUtil.isNetworkError(it) }
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun updateDiscounts(): Completable {
        return Single.defer { sharedViewModel!!.getPaymentLocationData() }
            .map { it.locationId }
            .flatMapMaybe { paymentManager.getActiveDiscountCampaignIfAvailable(it) }
            .flatMapCompletable(::applyDiscount)
            .retryWhenWithDelay(TimeUnit.SECONDS.toMillis(1)) { ThrowableUtil.isNetworkError(it) }
    }

    private fun applyDiscount(campaign: CampaignResponseData.PaymentCampaign): Completable {
        return Completable.fromAction {
            Timber.i("Applying discount: $campaign")
            updateAsSideEffect(
                paymentAmounts,
                paymentAmounts.value!!.copy(
                    discountPercentage = campaign.discountPercentage!!,
                    maximumDiscountAmount = campaign.maximumDiscountAmount!!.toCurrencyAmountNumber()
                )
            )
        }
    }

    fun onAmountChanged(amountInput: String) {
        if (isFixedAmount.value == true) {
            Timber.w("Amount is fixed, ignoring change: $amountInput")
            return
        }
        val amount = amountInput.toCurrencyAmountNumber()
        Timber.v("Amount changed: Input: $amountInput, Parsed: $amount")
        updateAsSideEffect(paymentAmounts, paymentAmounts.value!!.copy(invoiceAmount = amount))
        if (getAmountErrorIfAvailable(amount) == null) {
            updateAsSideEffectIfRequired(warningText, null)
        }
    }

    fun onPartialAmount() {
        val amount = paymentAmounts.value!!
        updateAsSideEffectIfRequired(sharedViewModel!!.paymentAmounts, amount)
        sharedViewModel!!.navigateToNext()
    }

    fun onAmountConfirmed() {
        var amount = paymentAmounts.value!!
        if (amount.openAmount != null) {
            amount = amount.copy(partialPaymentAmount = amount.openAmount!!)
        }
        Timber.i("Confirmed amount: $amount")

        if (amount.isClosingPayment) {
            // amount limits don't apply if it's closing the payment request
            updateAsSideEffectIfRequired(warningText, null)
        } else {
            val error = getAmountErrorIfAvailable(amount.openAmount ?: amount.invoiceAmount)
            updateAsSideEffectIfRequired(warningText, error)
            if (error != null) {
                updateAsSideEffectIfRequired(showKeyboard, !isFixedAmount.value!!)
                return
            }
        }

        updateAsSideEffectIfRequired(showKeyboard, false)
        updateAsSideEffectIfRequired(sharedViewModel!!.paymentAmounts, amount)
        updateAsSideEffectIfRequired(sharedViewModel!!.activatePartialPaymentPage, false)
        sharedViewModel!!.navigateToNext()
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun getAmountErrorIfAvailable(amount: Int): String? {
        val openAmount = paymentAmounts.value!!.openAmount
        if (amount == openAmount) {
            return null
        }
        return when {
            openAmount != null && amount > openAmount -> application.getString(R.string.error_split_exceeds_amount)
            amount < minimumAmount -> getPlaceholderString(
                R.string.pay_min_amount,
                "minAmount" to minimumAmount.toCurrencyAmountString()
            )
            amount > maximumAmount -> getPlaceholderString(
                R.string.pay_max_amount,
                "maxAmount" to maximumAmount.toCurrencyAmountString()
            )
            else -> null
        }
    }

    fun onResume() {
        isViewResumed = true
        updateAsSideEffect(sharedViewModel!!.activatePartialPaymentPage, true)
    }

    fun onPause() {
        isViewResumed = false
    }

    companion object {
        const val INITIAL_AMOUNT = 0
        const val MINIMUM_AMOUNT = 100 // 1 €
        const val MAXIMUM_AMOUNT = 10000000 // 100k €

        @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
        val PAYMENT_REQUEST_POLLING_INTERVAL = if (LucaApplication.isRunningTests()) {
            TimeUnit.MILLISECONDS.toMillis(500)
        } else {
            TimeUnit.SECONDS.toMillis(10)
        }
    }
}
