package de.culture4life.luca.network.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LocationTableResponseData(

    @Expose
    @SerializedName("id")
    val id: String,

    @Expose
    @SerializedName("customName")
    val customName: String?,

    @Expose
    @SerializedName("internalNumber")
    val internalNumber: Int?,

    @Expose
    @SerializedName("locationId")
    val locationId: String
)
