package de.culture4life.luca.network.pojo.payment

import com.google.gson.annotations.SerializedName

data class ConsumerInformationResponseData(

    @SerializedName("uuid")
    val uuid: String,

    @SerializedName("username")
    val username: String,

    @SerializedName("tokenizedCards")
    val payCards: List<PayCard>?

) {
    data class PayCard(

        @SerializedName("name")
        val name: String?,

        @SerializedName("type")
        val type: String?,

        @SerializedName("last4digits")
        val last4digits: String?,

        @SerializedName("expirationYear")
        val expirationYear: String?,

        @SerializedName("expirationMonth")
        val expirationMonth: String?

    )
}
