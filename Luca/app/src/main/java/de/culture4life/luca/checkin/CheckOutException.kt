package de.culture4life.luca.checkin

import androidx.annotation.IntDef

class CheckOutException @JvmOverloads constructor(
    message: String,
    @CheckOutErrorCode
    val errorCode: Int,
    cause: Throwable? = null
) : Exception(message, cause) {

    @IntDef(UNKNOWN_ERROR, MISSING_PERMISSION_ERROR, MINIMUM_DURATION_ERROR, MINIMUM_DISTANCE_ERROR, LOCATION_UNAVAILABLE_ERROR)
    @kotlin.annotation.Retention(AnnotationRetention.SOURCE)
    annotation class CheckOutErrorCode

    companion object {
        const val UNKNOWN_ERROR = 0
        const val MISSING_PERMISSION_ERROR = 1
        const val MINIMUM_DURATION_ERROR = 2
        const val MINIMUM_DISTANCE_ERROR = 3
        const val LOCATION_UNAVAILABLE_ERROR = 4
    }
}
