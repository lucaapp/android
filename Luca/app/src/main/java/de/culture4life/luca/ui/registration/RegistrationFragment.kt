package de.culture4life.luca.ui.registration

import android.animation.ObjectAnimator
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.viewbinding.ViewBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import de.culture4life.luca.BuildConfig
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.R
import de.culture4life.luca.databinding.FragmentRegistrationAllBinding
import de.culture4life.luca.network.NetworkManager.Companion.isHttpException
import de.culture4life.luca.ui.BaseFragment
import de.culture4life.luca.ui.DefaultTextWatcher
import de.culture4life.luca.ui.dialog.BaseDialogFragment
import de.culture4life.luca.util.AccessibilityServiceUtil
import de.culture4life.luca.util.TimeUtil.getCurrentMillis
import de.culture4life.luca.util.TimeUtil.getReadableDurationWithPlural
import de.culture4life.luca.util.addTo
import de.culture4life.luca.util.hideKeyboard
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.schedulers.Schedulers
import timber.log.Timber
import java.net.HttpURLConnection
import java.util.concurrent.TimeUnit

class RegistrationFragment : BaseFragment<RegistrationViewModel>() {

    private lateinit var binding: FragmentRegistrationAllBinding
    private var completionObserver: Observer<Boolean>? = null

    private val isNameStepCompleted: Boolean
        get() = areFieldsValidOrEmptyAndNotRequired(listOf(binding.firstNameLayout, binding.lastNameLayout))

    private val isAddressStepCompleted: Boolean
        get() = areFieldsValidOrEmptyAndNotRequired(
            listOf(
                binding.addressLayout.streetLayout,
                binding.addressLayout.houseNumberLayout,
                binding.postalCodeLayout,
                binding.cityNameLayout
            )
        )

    private val areAllStepsCompleted: Boolean
        get() = isNameStepCompleted && isAddressStepCompleted

    private val shouldSkipVerification: Boolean
        get() = !viewModel.isInEditMode ||
            viewModel.isUsingTestingCredentials ||
            RegistrationViewModel.SKIP_PHONE_NUMBER_VERIFICATION ||
            binding.phoneNumberLayout.isEmptyAndNotRequired

    override fun getViewBinding(): ViewBinding = FragmentRegistrationAllBinding.inflate(layoutInflater).also { binding = it }

    override fun getViewModelClass(): Class<RegistrationViewModel> = RegistrationViewModel::class.java

    override fun initializeViews() {
        super.initializeViews()
        initializeSharedViews()
        initializeNameViews()
        initializeContactViews()
        initializeAddressViews()
    }

    override fun onPause() {
        super.onPause()
        if (!viewModel.isInEditMode) {
            viewModel.updateRegistrationDataWithFormValuesAsSideEffect()
        }
    }

    private fun initializeSharedViews() {
        observe(viewModel.progress, ::indicateProgress)

        if (viewModel.isInEditMode) {
            initializeSharedViewsInEditMode()
        } else {
            initializeSharedViewsInRegistrationMode()
        }

        observe(viewModel.completed) { completionObserver?.onChanged(it) }

        observe(viewModel.isLoading) { loading ->
            binding.registrationActionButton.isEnabled = !loading

            // indeterminate state can only be changed while invisible
            // see: https://github.com/material-components/material-components-android/issues/1921
            binding.registrationProgressIndicator.isVisible = false
            binding.registrationProgressIndicator.isIndeterminate = loading
            binding.registrationProgressIndicator.isVisible = viewModel.isInEditMode
        }
    }

    private fun initializeSharedViewsInRegistrationMode() {
        with(binding) {
            registrationProgressIndicator.isVisible = false
            registrationHeading.text = getString(R.string.registration_heading_name)
            nameTextView.isVisible = true
            if (LucaApplication.IS_USING_STAGING_ENVIRONMENT || BuildConfig.DEBUG) {
                registrationHeading.setOnClickListener { viewModel.useDebugRegistrationData().subscribe() }
            }

            binding.registrationActionButton.text = getString(R.string.action_finish)
            binding.registrationActionButton.setOnClickListener {
                Completable.fromAction {
                    if (isNameStepCompleted) {
                        viewModel.updateRegistrationDataWithFormValuesAsSideEffect()
                        completionObserver = Observer { completed ->
                            if (completed) {
                                (requireActivity() as RegistrationActivity).onRegistrationCompleted()
                            }
                        }
                        viewModel.onRegistrationRequested()
                    }
                }
                    .delaySubscription(DELAY_DURATION, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                    .subscribe()
                    .addTo(viewDisposable)
            }
        }
    }

    private fun initializeSharedViewsInEditMode() {
        binding.registrationHeading.text = getString(R.string.navigation_contact_data)
        binding.registrationActionButton.setText(R.string.action_update)
        binding.registrationActionButton.setOnClickListener {
            Completable.mergeArray(
                viewModel.updatePhoneNumberVerificationStatus(),
                viewModel.updateShouldReImportingTestData(),
                viewModel.updateLucaConnectUpdatesRequired()
            )
                .andThen(viewModel.getPhoneNumberVerificationStatus())
                .delaySubscription(DELAY_DURATION, TimeUnit.MILLISECONDS, Schedulers.io())
                .doOnSubscribe { view?.hideKeyboard() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { numberVerified ->
                    when {
                        !numberVerified && !shouldSkipVerification -> showCurrentPhoneNumberVerificationStep()
                        areAllStepsCompleted -> when {
                            viewModel.isLucaConnectNoticeRequired -> showLucaConnectNoticeDialog()
                            viewModel.shouldReImportTestData -> showWillDeleteDocumentsDialog()
                            else -> viewModel.onUserDataUpdateRequested()
                        }
                        else -> showMissingInfoDialog()
                    }
                }
                .addTo(viewDisposable)
        }

        completionObserver = Observer { completed ->
            if (completed) {
                (requireActivity() as RegistrationActivity).onEditingContactDataCompleted()
            }
        }
    }

    private fun initializeNameViews() {
        binding.firstNameLayout.isRequired = true
        binding.lastNameLayout.isRequired = true
        bindToLiveData(binding.firstNameLayout, viewModel.firstName)
        bindToLiveData(binding.lastNameLayout, viewModel.lastName)
        if (!viewModel.isInEditMode) {
            addConfirmationAction(binding.lastNameLayout)
        } else {
            binding.firstNameLayout.editText?.run {
                post { setSelection(text.length) }
            }
        }
    }

    private fun initializeContactViews() {
        if (viewModel.isInEditMode) {
            bindToLiveData(binding.phoneNumberLayout, viewModel.phoneNumber)
            bindToLiveData(binding.emailLayout, viewModel.email)
            binding.phoneNumberLayout.isVisible = true
            binding.emailLayout.isVisible = true
        }
    }

    private fun initializeAddressViews() {
        if (viewModel.isInEditMode) {
            bindToLiveData(binding.addressLayout.streetLayout, viewModel.street)
            bindToLiveData(binding.addressLayout.houseNumberLayout, viewModel.houseNumber)
            bindToLiveData(binding.postalCodeLayout, viewModel.postalCode)
            bindToLiveData(binding.cityNameLayout, viewModel.city)
            addConfirmationAction(binding.cityNameLayout)
            binding.addressLayout.root.isVisible = true
            binding.addressLayout.streetLayout.isVisible = true
            binding.addressLayout.houseNumberLayout.isVisible = true
            binding.postalCodeLayout.isVisible = true
            binding.cityNameLayout.isVisible = true
        }
    }

    private fun addConfirmationAction(textInputLayout: TextInputLayout) {
        textInputLayout.editText!!.imeOptions = EditorInfo.IME_ACTION_DONE
        textInputLayout.editText!!.setOnEditorActionListener { _, _, _ -> binding.registrationActionButton.callOnClick() }
    }

    private fun bindToLiveData(textInputLayout: RegistrationTextInputLayout, textLiveData: LiveData<String>) {
        val editText = textInputLayout.editText!!

        editText.addTextChangedListener(object : DefaultTextWatcher() {
            override fun afterTextChanged(editable: Editable) {
                viewModel.onFormValueChanged(textLiveData, editable.toString())
            }
        })

        editText.onFocusChangeListener = OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                textInputLayout.hideError()
            } else {
                Completable.timer(DELAY_DURATION, TimeUnit.MILLISECONDS, Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .andThen(
                        Completable.fromAction {
                            if (textInputLayout.isEmptyButRequired) {
                                textInputLayout.error = getString(R.string.registration_empty_but_required_field_error)
                            } else if (!textInputLayout.isValid && textInputLayout.isRequired) {
                                textInputLayout.error = getString(R.string.registration_invalid_value_field_error)
                            } else {
                                textInputLayout.hideError()
                            }
                        }
                    )
                    .subscribe()
                    .addTo(viewDisposable)
            }
        }

        observe(textLiveData) { value ->
            if (value.trim() != editText.text.toString().trim()) {
                editText.setText(value)
            }
        }

        observe(viewModel.getValidationStatus(textLiveData)) { textInputLayout.isValid = it }
    }

    private fun indicateProgress(progress: Double) {
        val percent = (progress * 100).toInt().coerceIn(0, 100)
        ObjectAnimator.ofInt(binding.registrationProgressIndicator, "progress", percent)
            .setDuration(250)
            .start()
    }

    private fun showCurrentPhoneNumberVerificationStep() {
        if (viewModel.shouldRequestNewVerificationTan.value!!) {
            val nextPossibleVerificationTanRequestTimestamp = viewModel.nextPossibleTanRequestTimestamp.value!!
            if (nextPossibleVerificationTanRequestTimestamp > getCurrentMillis()) {
                showPhoneNumberRequestTimeoutDialog(nextPossibleVerificationTanRequestTimestamp)
            } else {
                showPhoneNumberVerificationConfirmationDialog()
            }
        } else {
            showPhoneNumberVerificationTanDialog()
        }
    }

    private fun showPhoneNumberVerificationConfirmationDialog() {
        val number = viewModel.getFormattedPhoneNumber()
        val isMobileNumber = viewModel.isMobilePhoneNumber(number)
        val messageResource =
            if (isMobileNumber) R.string.verification_explanation_sms_description else R.string.verification_explanation_landline_description
        BaseDialogFragment(
            MaterialAlertDialogBuilder(requireContext()).setTitle(getString(R.string.verification_explanation_title))
                .setMessage(getString(messageResource, number))
                .setPositiveButton(R.string.action_confirm) { _, _ -> requestPhoneNumberVerificationTan() }
                .setNegativeButton(R.string.action_cancel) { dialog, _ -> dialog.cancel() }
        ).show()
    }

    private fun requestPhoneNumberVerificationTan() {
        viewModel.requestPhoneNumberVerificationTan()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnComplete { showPhoneNumberVerificationTanDialog() }
            .subscribe(
                { Timber.i("Phone number verification TAN sent") },
                { Timber.w("Unable to request phone number verification TAN: $it") }
            )
            .addTo(viewDisposable)
    }

    private fun showPhoneNumberVerificationTanDialog() {
        val viewGroup = requireActivity().findViewById<ViewGroup>(android.R.id.content)
        val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_tan, viewGroup, false)
        val builder = MaterialAlertDialogBuilder(requireContext()).setView(dialogView)
            .setTitle(R.string.verification_enter_tan_title)
            .setPositiveButton(R.string.action_ok) { _, _ -> }
            .setNegativeButton(R.string.action_cancel) { dialog, _ ->
                viewModel.onPhoneNumberVerificationCanceled()
                dialog.cancel()
            }
        val alertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener { verifyTanDialogInput(alertDialog) }
        val tanEditText: TextInputEditText = dialogView.findViewById(R.id.tanInputEditText)
        tanEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                verifyTanDialogInput(alertDialog)
                return@setOnEditorActionListener true
            }
            false
        }
        dialogView.findViewById<View>(R.id.infoImageView).setOnClickListener { showTanNotReceivedDialog() }
        dialogView.findViewById<View>(R.id.infoTextView).setOnClickListener { showTanNotReceivedDialog() }
    }

    private fun showMissingInfoDialog() {
        MaterialAlertDialogBuilder(requireContext()).setTitle(R.string.registration_missing_info)
            .setMessage(R.string.registration_missing_fields_error_text)
            .setPositiveButton(R.string.action_ok) { _, _ -> }
            .create()
            .show()
    }

    private fun showWillDeleteDocumentsDialog() {
        MaterialAlertDialogBuilder(requireContext()).setTitle(R.string.registration_will_delete_tests_title)
            .setMessage(R.string.registration_will_delete_tests_text)
            .setNegativeButton(R.string.action_no) { _, _ -> }
            .setPositiveButton(R.string.action_yes) { _, _ -> viewModel.onUserDataUpdateRequested() }
            .create()
            .show()
    }

    private fun showLucaConnectNoticeDialog() {
        LucaConnectNotice(viewModel, requireContext()).show { viewModel.onUserDataUpdateRequested() }
    }

    private fun verifyTanDialogInput(alertDialog: AlertDialog) {
        view?.hideKeyboard()
        completionObserver = Observer { completed: Boolean ->
            if (completed) {
                (requireActivity() as RegistrationActivity).onRegistrationCompleted()
            }
        }
        val tanInputLayout = alertDialog.findViewById<TextInputLayout>(R.id.tanInputLayout)
        val verificationTan = tanInputLayout!!.editText!!.text.toString()
        if (verificationTan.length != 6) {
            tanInputLayout.error = getString(R.string.verification_enter_tan_error)
            return
        }
        viewModel.verifyTan(verificationTan)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    Timber.i("Phone number verified")
                    alertDialog.dismiss()
                    view?.hideKeyboard()
                    viewModel.onUserDataUpdateRequested()
                }
            ) { throwable: Throwable ->
                Timber.w("Phone number verification failed: %s", throwable.toString())
                val isForbidden = isHttpException(throwable, HttpURLConnection.HTTP_FORBIDDEN)
                val isBadRequest = isHttpException(throwable, HttpURLConnection.HTTP_BAD_REQUEST)
                if (isForbidden || isBadRequest) {
                    // TAN was incorrect
                    tanInputLayout.error = getString(R.string.verification_enter_tan_error)
                }
            }
            .addTo(viewDisposable)
    }

    private fun showTanNotReceivedDialog() {
        BaseDialogFragment(
            MaterialAlertDialogBuilder(requireContext()).setTitle(R.string.verification_tan_delayed_title)
                .setMessage(R.string.verification_tan_delayed_description)
                .setPositiveButton(R.string.action_ok) { dialog, _ -> dialog.cancel() }
        ).show()
    }

    private fun showPhoneNumberRequestTimeoutDialog(nextPossibleTanRequestTimestamp: Long) {
        val duration = nextPossibleTanRequestTimestamp - getCurrentMillis()
        val readableDuration = getReadableDurationWithPlural(duration, requireContext()).blockingGet()
        BaseDialogFragment(
            MaterialAlertDialogBuilder(requireContext())
                .setTitle(R.string.verification_timeout_error_title)
                .setMessage(getString(R.string.verification_timeout_error_description, readableDuration))
                .setPositiveButton(R.string.action_ok) { dialog, _ -> dialog.cancel() }
                .setNeutralButton(R.string.verification_timeout_action_use_last) { _, _ -> showPhoneNumberVerificationTanDialog() }
        ).show()
    }

    private fun areFieldsValidOrEmptyAndNotRequired(fields: List<RegistrationTextInputLayout>): Boolean {
        var completed = true
        for (textLayout in fields) {
            if (textLayout.isValidOrEmptyAndNotRequired) {
                continue
            }
            if (completed) {
                completed = false
                textLayout.requestFocus()
            }
            if (textLayout.isEmptyButRequired) {
                talkbackHintFor(textLayout)
                textLayout.error = getString(R.string.registration_empty_but_required_field_error)
            } else if (!textLayout.isValid) {
                textLayout.error = getString(R.string.registration_invalid_value_field_error)
            }
        }
        return completed
    }

    private fun talkbackHintFor(textLayout: RegistrationTextInputLayout) {
        if (inputTextIdToHint.containsKey(textLayout.id)) {
            AccessibilityServiceUtil.speak(requireContext(), getString(inputTextIdToHint[textLayout.id]!!))
        }
    }

    companion object {
        private val DELAY_DURATION = RegistrationViewModel.DEBOUNCE_DURATION
        private val inputTextIdToHint = hashMapOf(
            R.id.firstNameLayout to R.string.registration_missing_first_name_hint,
            R.id.lastNameLayout to R.string.registration_missing_last_name_hint,
            R.id.phoneNumberLayout to R.string.registration_missing_phone_number_hint,
            R.id.streetLayout to R.string.registration_missing_street_hint,
            R.id.houseNumberLayout to R.string.registration_missing_house_number_hint,
            R.id.postalCodeLayout to R.string.registration_missing_postal_code_hint,
            R.id.cityNameLayout to R.string.registration_missing_city_hint,
        )
    }
}
