package de.culture4life.luca.network.pojo

import com.google.gson.annotations.SerializedName

data class AdditionalCheckInPropertiesRequestData(
    @SerializedName("traceId")
    val traceId: String,

    @SerializedName("data")
    val encryptedProperties: String,

    @SerializedName("iv")
    val iv: String,

    @SerializedName("mac")
    val mac: String,

    @SerializedName("publicKey")
    val scannerPublicKey: String
)
