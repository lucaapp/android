package de.culture4life.luca.ui.qrcode

import android.os.Bundle
import de.culture4life.luca.ui.base.bottomsheetflow.BaseFlowPage

sealed class AddCertificateFlowPage(override val id: Long, override val arguments: Bundle? = null) : BaseFlowPage {
    object SelectInputPage : AddCertificateFlowPage(0)
    object ScanQrCodePage : AddCertificateFlowPage(1)
    object DocumentAddedSuccessPage : AddCertificateFlowPage(2)
}
