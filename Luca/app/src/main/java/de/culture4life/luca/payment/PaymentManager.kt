package de.culture4life.luca.payment

import android.content.ActivityNotFoundException
import android.content.Context
import android.net.Uri
import androidx.annotation.VisibleForTesting
import androidx.core.os.ConfigurationCompat
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.Manager
import de.culture4life.luca.R
import de.culture4life.luca.checkin.AdditionalCheckInData
import de.culture4life.luca.consent.ConsentManager
import de.culture4life.luca.crypto.CryptoManager
import de.culture4life.luca.network.NetworkManager
import de.culture4life.luca.network.endpoints.LucaEndpointsV3
import de.culture4life.luca.network.pojo.LocationResponseData
import de.culture4life.luca.network.pojo.payment.*
import de.culture4life.luca.preference.PreferencesManager
import de.culture4life.luca.registration.RegistrationData
import de.culture4life.luca.util.*
import de.culture4life.luca.util.TextUtil.getPlaceholderString
import de.culture4life.luca.whatisnew.WhatIsNewManager
import de.culture4life.luca.whatisnew.WhatIsNewManager.Companion.ID_LUCA_PAY_REVOCATION_CODE_MESSAGE
import io.reactivex.rxjava3.core.*
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.BehaviorSubject
import timber.log.Timber
import java.net.HttpURLConnection
import java.net.HttpURLConnection.HTTP_NOT_FOUND
import java.net.HttpURLConnection.HTTP_UNAUTHORIZED
import java.util.*
import kotlin.math.max

/**
 * Handles the registration of a consumer using randomly generated credentials (username & password).
 * Loads consumer data (cards, history) and processes payment requests.
 *
 * @see [Consumers](https://app.luca-app.de/pay/api/v1/swagger/#/Consumers)
 */
class PaymentManager(
    private val preferencesManager: PreferencesManager,
    private val networkManager: NetworkManager,
    private val cryptoManager: CryptoManager,
    private val whatIsNewManager: WhatIsNewManager,
    private val consentManager: ConsentManager
) : Manager() {

    private var cachedSignInData: Maybe<SignInData>? = null
    private val paymentResultDeeplinkSubject = BehaviorSubject.create<Uri>()
    private val paymentResultSubject = BehaviorSubject.create<PaymentData>()

    override fun doInitialize(context: Context): Completable {
        return Completable.mergeArray(
            preferencesManager.initialize(context),
            networkManager.initialize(context),
            whatIsNewManager.initialize(context),
            consentManager.initialize(context)
        )
    }

    override fun dispose() {
        cachedSignInData = null
        super.dispose()
    }

    /*
        Consumer sign-up & sign-in
     */

    fun isSignedUp() = getSignInDataIfAvailable().isNotEmpty()

    fun requestConsentAndSignUpIfRequired(): Completable {
        return isSignedUp()
            .filter { !it }
            .flatMapCompletable { requestConsentIfRequiredAndSignUp() }
    }

    fun requestConsentIfRequiredAndSignUp(): Completable {
        return requestConsentIfRequired()
            .andThen(signUp())
    }

    /**
     * Sign up consumer for payment using their full name and randomly generated username and password. Sign in to get the jwt.
     *
     * @see [Consumer Authentication](https://gitlab.com/luca-internal/docs/luca-rfcs/-/blob/master/8/consumer-authentication.md)
     */
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun signUp(): Completable {
        return assertConsentApproved()
            .andThen(
                Completable.defer {
                    createConsumerSignUpRequestData()
                        .doOnSuccess { data -> Timber.d("Sign up consumer data: $data") }
                        .flatMapCompletable { signUpConsumerRequestData ->
                            networkManager.getLucaPayEndpoints()
                                .flatMap { it.signUpConsumer(signUpConsumerRequestData) }
                                .doOnSuccess { Timber.i("Signed up consumer with ID: ${it.uuid}") }
                                .flatMapCompletable { persistRevocationCodeAndShowMessage(it.revocationCode) }
                        }
                        .onErrorResumeNext {
                            Timber.w("Unable to sign up consumer: $it")
                            if (it.isHttpException(HttpURLConnection.HTTP_CONFLICT)) {
                                deleteConsumer().andThen(signUp())
                            } else {
                                Completable.error(it)
                            }
                        }
                }
            )
            .andThen(signIn())
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun signIn(): Completable {
        return createConsumerSignInRequestData()
            .doOnSuccess { data -> Timber.d("Sign in consumer data: $data") }
            .flatMap { signInConsumerRequestData ->
                networkManager.getLucaPayEndpoints().flatMap { it.signInConsumer(signInConsumerRequestData) }
            }
            .doOnSuccess { signInConsumerResponseData -> Timber.i("Signed in consumer with token: ${signInConsumerResponseData.accessToken}") }
            .flatMapCompletable { signInConsumerResponseData ->
                persistSignInData(
                    SignInData(
                        accessToken = signInConsumerResponseData.accessToken,
                        expirationTimestamp = signInConsumerResponseData.expirationTimestamp.fromUnixTimestamp()
                    )
                )
            }
    }

    private fun signInIfUnauthorizedOrError(error: Throwable): Completable {
        return Completable.defer {
            if (error.isHttpException(HTTP_UNAUTHORIZED)) {
                signIn()
            } else {
                Completable.error(error)
            }
        }
    }

    fun fetchConsumerInformation(): Single<ConsumerInformationResponseData> {
        return getSignInData()
            .flatMap { signInData ->
                networkManager.getLucaPayEndpoints()
                    .flatMap { it.getConsumerInformation(signInData.accessToken) }
            }
            .onErrorResumeNext {
                signInIfUnauthorizedOrError(it)
                    .andThen(fetchConsumerInformation())
            }
    }

    fun deleteConsumerIfRequired(): Completable {
        return getSignInDataIfAvailable().flatMapCompletable { deleteConsumer(true) }
    }

    fun deleteConsumer(revokeConsent: Boolean = false): Completable {
        return getSignInData()
            .flatMapCompletable { signInData ->
                networkManager.getLucaPayEndpoints()
                    .flatMapCompletable { it.deleteConsumer(signInData.accessToken) }
            }
            .onErrorResumeNext {
                signInIfUnauthorizedOrError(it)
                    .andThen(deleteConsumer())
                    .onErrorResumeNext { error ->
                        if (error.isHttpException(HTTP_UNAUTHORIZED, HTTP_NOT_FOUND)) {
                            Completable.complete()
                        } else {
                            Completable.error(error)
                        }
                    }
            }
            .andThen(deleteSignInData())
            .andThen(deleteConsumerData())
            .andThen(hideRevocationCodeMessage())
            .andThen(
                Completable.defer {
                    if (revokeConsent) revokeConsent() else Completable.complete()
                }
            )
            .doOnSubscribe { Timber.i("Deleting consumer") }
    }

    /*
        Payment requests
     */

    fun fetchOpenPaymentRequest(locationId: String, table: String?): Maybe<OpenPaymentRequestResponseData> {
        return networkManager.getLucaPayEndpoints()
            .flatMap { it.getOpenPaymentRequest(locationId, table) }
            .toMaybe()
            .onErrorResumeNext {
                if (it.isHttpException(HTTP_NOT_FOUND)) {
                    Maybe.empty() // currently no open payment request
                } else {
                    Maybe.error(it)
                }
            }
    }

    /*
        Checkout
     */

    fun fetchCheckout(checkoutId: String): Single<CheckoutResponseData> {
        return networkManager.getLucaPayEndpoints()
            .flatMap { it.getCheckout(checkoutId) }
    }

    fun createCheckout(
        locationId: String,
        paymentRequestId: String?,
        table: String?,
        invoiceAmount: Int,
        partialAmount: Int? = null,
        tipAmount: Int
    ): Single<CreateCheckoutResponseData> {
        val createCheckout = if (paymentRequestId != null) {
            createOperatorCheckout(
                locationId = locationId,
                paymentRequestId = paymentRequestId,
                table = table,
                partialAmount = partialAmount,
                tipAmount = tipAmount
            )
        } else {
            createCustomerCheckout(
                locationId = locationId,
                table = table,
                invoiceAmount = invoiceAmount,
                tipAmount = tipAmount
            )
        }
        return createCheckout
            .onErrorResumeNext {
                signInIfUnauthorizedOrError(it)
                    .andThen(
                        createCheckout(
                            locationId = locationId,
                            paymentRequestId = paymentRequestId,
                            table = table,
                            invoiceAmount = invoiceAmount,
                            partialAmount = partialAmount,
                            tipAmount = tipAmount
                        )
                    )
            }
            .flatMap {
                persistLastCheckoutId(it.id)
                    .andThen(Single.just(it))
            }
    }

    /**
     * Create checkout for customer-initiated payment.
     *
     * @see [Create a checkout](https://app.luca-app.de/pay/api/v1/swagger/#/Consumers/post_checkouts)
     */
    private fun createCustomerCheckout(
        locationId: String,
        table: String?,
        invoiceAmount: Int,
        tipAmount: Int
    ): Single<CreateCheckoutResponseData> {
        return getSignInData()
            .flatMap { signInData ->
                networkManager.getLucaPayEndpoints()
                    .flatMap {
                        it.createCheckout(
                            accessToken = signInData.accessToken,
                            data = CreateCustomerCheckoutRequestData(
                                locationId = locationId,
                                table = table,
                                invoiceAmount = invoiceAmount.toCurrencyAmountBigDecimal(),
                                tipAmount = tipAmount.toCurrencyAmountBigDecimal()
                            )
                        )
                    }
            }
    }

    /**
     * Create checkout for operator-initiated payment.
     *
     * @see [Create a checkout for a payment request](https://app.luca-app.de/pay/api/v1/swagger/#/PaymentRequests/post_locations__locationId__paymentRequests__paymentRequestId__checkout)
     */
    private fun createOperatorCheckout(
        locationId: String,
        paymentRequestId: String,
        table: String?,
        partialAmount: Int? = null,
        tipAmount: Int
    ): Single<CreateCheckoutResponseData> {
        return getSignInData()
            .flatMap { signInData ->
                networkManager.getLucaPayEndpoints()
                    .flatMap {
                        it.createCheckoutForPaymentRequest(
                            accessToken = signInData.accessToken,
                            locationId = locationId,
                            paymentRequestId = paymentRequestId,
                            data = CreateOperatorCheckoutRequestData(
                                table = table,
                                partialAmount = partialAmount?.toCurrencyAmountBigDecimal(),
                                tipAmount = tipAmount.toCurrencyAmountBigDecimal()
                            )
                        )
                    }
            }
    }

    fun restoreLastCheckoutIdIfAvailable(): Maybe<String> {
        return preferencesManager.restoreIfAvailable(KEY_LAST_CHECKOUT_ID, String::class.java)
    }

    fun persistLastCheckoutId(id: String): Completable {
        return preferencesManager.persist(KEY_LAST_CHECKOUT_ID, id)
    }

    fun deleteLastCheckoutId(): Completable {
        return preferencesManager.delete(KEY_LAST_CHECKOUT_ID)
    }

    /*
        Payment history
     */

    fun fetchPaymentHistory(cursor: String? = null): Single<PagedPaymentHistory> {
        return fetchPaymentHistoryPage(cursor)
            .firstOrError()
            .map(::PagedPaymentHistory)
    }

    fun fetchCompletePaymentHistory(): Observable<PaymentData> {
        return fetchCompletePaymentHistory(null)
            .concatMap { response -> Observable.fromIterable(response.payments) }
            .map(PaymentData::fromPayment)
    }

    private fun fetchCompletePaymentHistory(cursor: String?): Observable<ConsumerPaymentsResponseData> {
        return fetchPaymentHistoryPage(cursor)
            .concatMap { response ->
                if (response.cursor == null) {
                    Observable.just(response)
                } else {
                    Observable.just(response)
                        .concatWith(fetchCompletePaymentHistory(response.cursor))
                }
            }
    }

    private fun fetchPaymentHistoryPage(cursor: String?): Observable<ConsumerPaymentsResponseData> {
        return getSignInData()
            .flatMap { signInData ->
                networkManager.getLucaPayEndpoints()
                    .flatMap { endpoint -> endpoint.getConsumerPayments(signInData.accessToken, cursor) }
            }
            .toObservable()
            .onErrorResumeNext {
                signInIfUnauthorizedOrError(it)
                    .andThen(fetchPaymentHistoryPage(cursor))
            }
    }

    /*
        Payment web-app
     */

    fun openPaymentWebApp(url: String) {
        application.openUrl(url, true)
    }

    fun onPaymentResultReceived(deeplink: Uri) {
        paymentResultDeeplinkSubject.onNext(deeplink)
    }

    fun getPaymentResultDeeplinks(): Observable<Uri> {
        return paymentResultDeeplinkSubject
    }

    /*
        Consent
     */

    private fun requestConsentIfRequired(): Completable {
        return consentManager.getConsent(ConsentManager.ID_TERMS_OF_SERVICE_LUCA_PAY)
            .flatMapCompletable { consent ->
                if (consent.approved) {
                    consentManager.requestConsentIfRequiredAndAssertApproved(ConsentManager.ID_ACTIVATE_LUCA_PAY)
                } else {
                    // this case occurs when updating a version where ID_TERMS_OF_SERVICE_LUCA_PAY wasn't part of the ID terms during OnBoarding
                    consentManager.requestConsentBundleIfRequiredAndAssertApproved(*ConsentManager.lucaPayConsentBundle)
                }
            }
    }

    private fun assertConsentApproved(): Completable {
        return consentManager.assertConsentApproved(ConsentManager.ID_TERMS_OF_SERVICE_LUCA_PAY)
            .andThen(consentManager.assertConsentApproved(ConsentManager.ID_ACTIVATE_LUCA_PAY))
    }

    private fun revokeConsent(): Completable {
        return consentManager.revokeConsent(ConsentManager.ID_ACTIVATE_LUCA_PAY)
    }

    /*
        Consumer data
     */

    /**
     * Create data to sign up with using the created and persisted credentials.
     *
     * @see [Sign up consumer](https://app.luca-app.de/pay/api/v1/swagger/#/Consumers/post_consumers_signup)
     */
    private fun createConsumerSignUpRequestData(): Single<ConsumerSignUpRequestData> {
        return getConsumerData()
            .map { consumerData ->
                ConsumerSignUpRequestData(
                    username = consumerData.username,
                    name = consumerData.name,
                    password = consumerData.password
                )
            }
    }

    /**
     * Generate data required for signing in using the persisted [ConsumerData].
     *
     * @see [Sign in consumer](https://app.luca-app.de/pay/api/v1/swagger/#/Consumers/post_consumers_signin)
     */
    private fun createConsumerSignInRequestData(): Single<ConsumerSignInRequestData> {
        return getConsumerData()
            .map { consumerData ->
                ConsumerSignInRequestData(
                    username = consumerData.username,
                    password = consumerData.password
                )
            }
    }

    private fun getConsumerData(): Single<ConsumerData> {
        return restoreConsumerDataIfAvailable()
            .switchIfEmpty(createConsumerData())
    }

    /**
     * Creates and persists consumer data to use for signing up. The name is used as saved in [RegistrationData.fullName]. The username is randomly
     * generated with [UUID]. The password is an UUID constructed from the secure random bytes generated by the [CryptoManager].
     */
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun createConsumerData(): Single<ConsumerData> {
        return cryptoManager.initialize(context)
            .andThen(cryptoManager.generateSecureRandomData(CONSUMER_CREDENTIALS_LENGTH))
            .map { randomBytes ->
                val id = UUID.randomUUID().toString()
                val password = UUID.nameUUIDFromBytes(randomBytes).toString()
                ConsumerData(username = id, name = id, password = password)
            }
            .flatMap {
                persistConsumerData(it)
                    .andThen(Single.just(it))
            }
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun persistConsumerData(newConsumerData: ConsumerData): Completable {
        return preferencesManager.persist(KEY_CONSUMER_DATA, newConsumerData)
            .doOnComplete { Timber.v("Persisted consumer data: $newConsumerData") }
    }

    private fun restoreConsumerDataIfAvailable(): Maybe<ConsumerData> {
        return preferencesManager.restoreIfAvailable(KEY_CONSUMER_DATA, ConsumerData::class.java)
    }

    private fun deleteConsumerData(): Completable {
        return preferencesManager.delete(KEY_CONSUMER_DATA)
    }

    /*
        Sign-in data
     */

    /**
     * Retrieves cached sign in data if access token has not expired or tries to sign in again and then returns cached sign in data
     */
    private fun getSignInData(): Single<SignInData> {
        return getSignInDataIfAvailable()
            .flatMap { signInData ->
                try {
                    if (LucaApplication.isRunningUnitTests()) {
                        // don't use actual JWT, as it's expired already
                        require(signInData.expirationTimestamp > TimeUtil.getCurrentMillis())
                    } else {
                        signInData.accessToken.parseSignedJwt().run {
                            require(body["exp"] as Int > TimeUtil.getCurrentMillis().toUnixTimestamp())
                        }
                    }
                    Maybe.just(signInData)
                } catch (error: Throwable) {
                    Maybe.empty()
                }
            }
            .switchIfEmpty(
                signIn().andThen(getSignInDataIfAvailable().toSingle())
            )
    }

    private fun getSignInDataIfAvailable(): Maybe<SignInData> {
        return Maybe.defer {
            if (cachedSignInData == null) {
                cachedSignInData = restoreSignInDataIfAvailable().cache()
            }
            cachedSignInData!!
        }
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun persistSignInData(newSignInData: SignInData): Completable {
        return preferencesManager.persist(KEY_CONSUMER_SIGN_IN_DATA, newSignInData)
            .doOnSubscribe { cachedSignInData = Maybe.just(newSignInData) }
            .doOnComplete { Timber.v("Persisted sign in data: $newSignInData") }
    }

    private fun restoreSignInDataIfAvailable(): Maybe<SignInData> {
        return preferencesManager.restoreIfAvailable(KEY_CONSUMER_SIGN_IN_DATA, SignInData::class.java)
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun deleteSignInData(): Completable {
        return preferencesManager.delete(KEY_CONSUMER_SIGN_IN_DATA)
            .doOnSubscribe { cachedSignInData = Maybe.empty() }
            .doOnComplete { Timber.v("Deleted sign in data") }
    }

    /*
        Config
     */

    fun fetchPaymentLimits(): Single<PaymentLimitsResponseData> {
        return networkManager.getLucaPayEndpoints()
            .flatMap { it.getPaymentLimits() }
    }

    /*
        Campaigns
     */

    private fun fetchCampaigns(locationGroupId: String): Observable<CampaignResponseData> {
        return networkManager.getLucaPayEndpoints()
            .flatMap { it.getCampaigns(locationGroupId) }
            .flatMapObservable { Observable.fromIterable(it) }
    }

    private fun submitCampaignParticipation(paymentId: String, campaign: String, email: String): Single<CampaignSubmissionResponseData> {
        return getSignInData()
            .flatMap { signInData ->
                networkManager.getLucaPayEndpoints()
                    .flatMap {
                        it.submitCampaignParticipation(
                            signInData.accessToken,
                            paymentId,
                            CampaignSubmissionRequestData(campaign, email, getLanguageCodeForCampaign())
                        )
                    }
            }
            .onErrorResumeNext {
                signInIfUnauthorizedOrError(it)
                    .andThen(submitCampaignParticipation(paymentId, campaign, email))
            }
    }

    fun submitRaffleCampaignParticipation(paymentId: String, email: String): Completable {
        return submitCampaignParticipation(paymentId, CAMPAIGN_PAY_RAFFLE, email)
            .flatMapCompletable { persistPaymentCampaignRevocationSecret(it.revocationSecret, paymentId) }
    }

    private fun getLanguageCodeForCampaign(): String {
        return if (ConfigurationCompat.getLocales(application.resources.configuration)[0]?.language == Locale.GERMANY.language) {
            Locale.GERMANY.language
        } else {
            Locale.ENGLISH.language
        }
    }

    fun restorePaymentCampaignRevocationSecretIfAvailable(paymentId: String): Maybe<String> {
        val key = "${KEY_CAMPAIGN_REVOCATION_CODE}_${CAMPAIGN_PAY_RAFFLE}_$paymentId"
        return preferencesManager.restoreIfAvailable(key, String::class.java)
    }

    private fun persistPaymentCampaignRevocationSecret(revocationSecret: String, paymentId: String): Completable {
        val key = "${KEY_CAMPAIGN_REVOCATION_CODE}_${CAMPAIGN_PAY_RAFFLE}_$paymentId"
        return preferencesManager.persist(key, revocationSecret)
    }

    fun isRaffleCampaignActive(locationId: String, timestamp: Long = TimeUtil.getCurrentMillis()): Single<Boolean> {
        return fetchCampaigns(locationId)
            .map { it.paymentCampaign }
            .any { it.name == CAMPAIGN_PAY_RAFFLE && it.wasActiveAt(timestamp) }
    }

    fun getActiveDiscountCampaignIfAvailable(locationGroupId: String): Maybe<CampaignResponseData.PaymentCampaign> {
        return fetchCampaigns(locationGroupId)
            .map { it.paymentCampaign }
            .filter { it.isDiscountCampaign && it.isCurrentlyActive }
            .sorted(Comparator.comparingInt { it.discountPercentage!! }) // if multiple are available, use largest discount
            .lastElement()
    }

    /*
        Support
     */

    @Throws(ActivityNotFoundException::class)
    fun openLucaPaySupportMailIntent(paymentData: PaymentData? = null) {
        val subject = context.getString(R.string.pay_support_mail_subject)
        val deviceInfo = application.createDeviceInformationForSupportMailText()
        val paymentInfo = createPaymentInformationForSupportMailText(paymentData)
        val supportText = getPlaceholderString(context, R.string.pay_support_mail_body, "deviceInfo" to deviceInfo, "paymentInfo" to paymentInfo)

        application.openSupportMailIntent(subject, supportText)
    }

    private fun createPaymentInformationForSupportMailText(paymentData: PaymentData?): String {
        val paymentCode = paymentData?.paymentVerifier ?: ""
        val paymentDate = paymentData?.let { context.getReadableDateTime(it.timestamp) } ?: ""
        val locationName = paymentData?.locationName ?: ""
        return getPlaceholderString(
            context,
            R.string.pay_support_mail_body_payment_info,
            "paymentCode" to paymentCode,
            "paymentDate" to paymentDate,
            "locationName" to locationName
        )
    }

    /*
        Revocation
     */

    fun getRevocationCode(): Single<String> {
        return preferencesManager.restore(KEY_REVOCATION_CODE, String::class.java)
    }

    private fun persistRevocationCode(revocationCode: String): Completable {
        return preferencesManager.persist(KEY_REVOCATION_CODE, revocationCode)
            .doOnComplete { Timber.v("Persisted revocation code: $revocationCode") }
    }

    private fun persistRevocationCodeAndShowMessage(revocationCode: String): Completable {
        return persistRevocationCode(revocationCode).andThen(
            whatIsNewManager.updateMessage(ID_LUCA_PAY_REVOCATION_CODE_MESSAGE) {
                copy(enabled = true, seen = false, notified = true, timestamp = TimeUtil.getCurrentMillis())
            }
        )
    }

    private fun hideRevocationCodeMessage(): Completable {
        return whatIsNewManager.updateMessage(ID_LUCA_PAY_REVOCATION_CODE_MESSAGE) { copy(enabled = false) }
    }

    /*
        Stuff
     */

    fun parsePaymentUrl(url: String): Single<PaymentLocationData> {
        return getLocationData(url)
            .flatMap { locationResponseData -> createPaymentLocationData(url, locationResponseData) }
    }

    private fun createPaymentLocationData(url: String, locationResponseData: LocationResponseData): Single<PaymentLocationData> {
        return getIsPaymentActive(locationResponseData.locationId)
            .map { isPaymentActive ->
                val table = getTable(url).blockingGet()
                val locationName = locationResponseData.locationDisplayName ?: context.getString(R.string.unknown)
                PaymentLocationData(locationResponseData.locationId, locationName, table, isPaymentActive)
            }
    }

    private fun getIsPaymentActive(locationId: String): Single<Boolean> {
        return networkManager.getLucaPayEndpoints()
            .flatMap { it.getPaymentActiveStatus(locationId) }
            .map { it.isPaymentActive }
    }

    private fun getScannerIdFromUrl(url: String): Single<UUID> {
        return Single.fromCallable {
            UUID.fromString(Uri.parse(url).lastPathSegment)
        }
    }

    private fun getLocationData(url: String): Single<LocationResponseData> {
        return getScannerIdFromUrl(url).flatMap { uuid: UUID ->
            getLocationDataFromScannerId(uuid.toString())
        }
    }

    private fun getLocationDataFromScannerId(scannerId: String): Single<LocationResponseData> {
        return networkManager.getLucaEndpointsV3()
            .flatMap { lucaEndpointsV3: LucaEndpointsV3 ->
                lucaEndpointsV3.getScanner(scannerId)
                    .map { jsonObject -> jsonObject["locationId"].asString }
                    .flatMap { locationId -> lucaEndpointsV3.getLocation(locationId) }
            }
    }

    private fun getTable(url: String): Maybe<String> {
        return LucaUrlUtil.getAdditionalDataFromVenueUrlIfAvailable(url)
            .map(::AdditionalCheckInData)
            .flatMapMaybe { Maybe.fromCallable<String> { it.table } }
            .onErrorComplete()
    }

    fun fetchTableName(tableId: String): Maybe<String> {
        return networkManager.getLucaEndpointsV4()
            .flatMap { it.getTableData(tableId) }
            .flatMapMaybe { tableData ->
                Maybe.fromCallable {
                    if (!tableData.customName.isNullOrEmpty()) {
                        tableData.customName
                    } else {
                        tableData.internalNumber.toString()
                    }
                }
            }
    }

    fun restoreDefaultTipPercentage(): Single<Int> {
        return preferencesManager.restoreOrDefault(KEY_LAST_TIP_PERCENTAGE, MINIMUM_DEFAULT_TIP_PERCENTAGE)
            .map { max(it, MINIMUM_DEFAULT_TIP_PERCENTAGE) }
    }

    fun persistLastTipPercentage(percentage: Int): Completable {
        return preferencesManager.persist(KEY_LAST_TIP_PERCENTAGE, percentage)
    }

    fun onPaymentResultReceived(paymentData: PaymentData) {
        paymentResultSubject.onNext(paymentData)
    }

    fun getPaymentResults(): Observable<PaymentData> {
        return paymentResultSubject.distinctUntilChanged()
    }

    companion object {
        private const val KEY_CAMPAIGN_REVOCATION_CODE = "pay_campaign_revocation_code"
        private const val KEY_REVOCATION_CODE = "pay_revocation_code"
        private const val KEY_CONSUMER_DATA = "pay_consumer_data"
        private const val KEY_CONSUMER_SIGN_IN_DATA = "pay_consumer_sign_in_data"
        private const val KEY_LAST_TIP_PERCENTAGE = "last_tip_percentage"
        private const val KEY_LAST_CHECKOUT_ID = "last_checkout_id"
        private const val MINIMUM_DEFAULT_TIP_PERCENTAGE = 10
        private const val CONSUMER_CREDENTIALS_LENGTH = 32
        const val CAMPAIGN_PAY_RAFFLE = "PayRaffleAndTipTopUp"
    }
}
