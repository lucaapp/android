package de.culture4life.luca.ui.compound

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.setPadding
import de.culture4life.luca.R

abstract class AccountPageActionItemBaseView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defaultStyle: Int = 0
) : ConstraintLayout(context, attributeSet, defaultStyle) {

    protected val inflater: LayoutInflater = LayoutInflater.from(context)

    init {
        isClickable = true
        background = ContextCompat.getDrawable(context, R.drawable.background_account_page_action_item)
        setPadding(context.resources.getDimensionPixelSize(R.dimen.spacing_default))
    }
}
