package de.culture4life.luca.testtools.preconditions

import androidx.test.core.app.ApplicationProvider
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.testtools.FixRobolectricIdlingResource

class WhatIsNewPreconditions {

    private val application = ApplicationProvider.getApplicationContext<LucaApplication>()
    private val whatIsNewManager by lazy {
        application.getInitializedManager(application.whatIsNewManager).blockingGet().also {
            // ensure that invoked initalization steps are done, does not wait for invokeDelayed
            FixRobolectricIdlingResource.waitForIdle()
        }
    }

    fun givenAllNewsSeen() {
        whatIsNewManager.disableWhatIsNewScreenForCurrentVersion().blockingAwait()
    }

    fun givenMessageEnabled(messageId: String, isEnabled: Boolean) {
        whatIsNewManager.updateMessage(messageId) { copy(enabled = isEnabled) }.blockingAwait()
    }

    fun givenMessageSeen(messageId: String, seen: Boolean) {
        whatIsNewManager.updateMessage(messageId) { copy(seen = seen) }.blockingAwait()
    }

    fun assertMessageMarkedAsSeen(id: String, isSeen: Boolean) {
        whatIsNewManager.getMessage(id)
            .test()
            .assertValue { it.seen == isSeen }
    }
}
