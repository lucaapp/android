package de.culture4life.luca.testtools.rules

import de.culture4life.luca.LucaApplication
import timber.log.Timber

/**
 * Setup and cleanup of printing log statements.
 */
class LoggingRule : BaseHookingTestRule() {

    override fun beforeTest() {
        // Currently the Timber "tree" is already added through LucaApplication always.

        printLogsForRobolectricTests()
    }

    override fun afterTest() {
        if (LucaApplication.isRunningUnitTests()) {
            // Robolectric creates a new Application instance for every test method which adds an additional "tree" every time.
            //  Because of singleton style it adds up multiple times. Without the removal we get same log statements printed multiple times.
            //  Instrumentation tests usually have one Application instance for all test methods.
            Timber.uprootAll()
        }
    }

    private fun printLogsForRobolectricTests() {
        if (LucaApplication.isRunningUnitTests()) {
            // Done through reflection to keep it compilable for androidTest target.
            val shadowLog = javaClass.classLoader!!.loadClass("org.robolectric.shadows.ShadowLog")
            shadowLog.getDeclaredField("stream").set(shadowLog, System.out)
        }
    }
}
