package de.culture4life.luca.testtools.pages.dialogs.base

import android.content.Context
import androidx.test.platform.app.InstrumentationRegistry
import io.github.kakaocup.kakao.dialog.KAlertDialog

abstract class DefaultOkDialog : DefaultDialog {

    protected val context: Context = InstrumentationRegistry.getInstrumentation().targetContext
    protected val baseDialog = KAlertDialog()

    val okButton = baseDialog.positiveButton
}
