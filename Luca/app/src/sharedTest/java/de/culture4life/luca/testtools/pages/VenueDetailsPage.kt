package de.culture4life.luca.testtools.pages

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.R
import de.culture4life.luca.consent.ConsentManager
import de.culture4life.luca.testtools.pages.dialogs.ConsentDialog
import de.culture4life.luca.testtools.samples.LucaPaySamples
import de.culture4life.luca.util.NumberUtil
import de.culture4life.luca.util.TextUtil
import de.culture4life.luca.util.toCurrencyAmountString
import io.github.kakaocup.kakao.common.views.KView
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView

class VenueDetailsPage {

    private val application = ApplicationProvider.getApplicationContext<LucaApplication>()

    val title = KTextView { withId(R.id.titleTextView) }
    val subTitle = KTextView { withId(R.id.subTitleTextView) }
    val emptyTitle = KTextView { withId(R.id.emptyTitleTextView) }
    val tableNameTitle = KTextView { withId(R.id.tableNameTextView) }
    val tableNameValue = KTextView { withId(R.id.tableNameValueTextView) }
    val tableNameSeparator = KView { withId(R.id.tableSeparatorView) }
    val amountTitle = KTextView { withId(R.id.amountTextView) }
    val amountValue = KTextView { withId(R.id.amountValueTextView) }
    val originAmountValue = KTextView { withId(R.id.originAmountValueTextView) }
    val checkOut = KButton { withId(R.id.actionBarBackButtonImageView) }
    val startPaymentButton = KButton { withId(R.id.startPaymentButton) }
    val reportButton = KButton { withId(R.id.reportMisuseButton) }
    val paymentFlow = PaymentHistoryPage.PaymentFlow()

    val termsOfServiceUpdateDialog = ConsentDialog(ConsentManager.ID_TERMS_OF_SERVICE_LUCA_ID)
    val termsOfServiceUpdateInfoDialog = ConsentDialog(ConsentManager.ID_TERMS_OF_SERVICE_INFO)
    val lucaPayConsentDialog = ConsentDialog(ConsentManager.ID_ACTIVATE_LUCA_PAY)

    val campaignInfoImageView = KTextView { withId(R.id.campaignInfoImageView) }
    val campaignInfoTextView = KTextView { withId(R.id.campaignInfoTextView) }

    private val context: Context
        get() = ApplicationProvider.getApplicationContext()

    fun tableIsDisplayed(tableName: String) {
        tableNameSeparator.isDisplayed()
        tableNameTitle.isDisplayed()
        tableNameValue.isDisplayed()
        tableNameValue.hasText(tableName)
    }

    fun tableIsHidden() {
        tableNameSeparator.isNotDisplayed()
        tableNameTitle.isNotDisplayed()
        tableNameValue.isNotDisplayed()
    }

    fun amountIsHidden() {
        amountValue.isNotDisplayed()
        amountTitle.isNotDisplayed()
        // not sure if [isNotDisplayed()] is right because it would also be "true" when visible but needs a "scrollTo" first
        originAmountValue.isGone()
    }

    fun amountIsDisplayed(amount: Int) {
        amountValue.isDisplayed()
        amountValue.hasText(context.getString(R.string.euro_amount, NumberUtil.toCurrencyAmountString(amount)))
        amountValue.hasTextColor(R.color.white)
        amountTitle.isDisplayed()

        originAmountValue.isGone()
    }

    fun amountWithDiscountIsDisplayed(amount: Int, originAmount: Int) {
        amountValue.isDisplayed()
        amountValue.hasText(context.getString(R.string.euro_amount, NumberUtil.toCurrencyAmountString(amount)))
        amountValue.hasTextColor(R.color.payment_promotion)
        amountTitle.isDisplayed()

        originAmountValue.hasText(context.getString(R.string.euro_amount, NumberUtil.toCurrencyAmountString(originAmount)))
        originAmountValue.isDisplayed()
    }

    fun campaignInfoIsHidden() {
        campaignInfoImageView.isGone()
        campaignInfoTextView.isGone()
    }

    fun campaignInfoIsDisplayed(campaign: LucaPaySamples.Campaign) {
        require(campaign.discountPercentage!! > 0) { "no other cases implemented yet" }
        require(campaign.maximumDiscountAmount!! > 0) { "otherwise discount percentage makes no sense" }

        val campaignInfoString = TextUtil.getPlaceholderString(
            application,
            R.string.pay_details_discount_hint,
            "discountPercentage" to "${campaign.discountPercentage}",
            "maximumDiscountAmount" to campaign.maximumDiscountAmount!!.toCurrencyAmountString()
        )

        campaignInfoImageView.isVisible()
        campaignInfoTextView.isVisible()
        campaignInfoTextView.hasText(campaignInfoString)
    }
}
