package de.culture4life.luca.testtools.actions

import io.github.kakaocup.kakao.common.actions.BaseActions

inline fun <T : BaseActions> T.scrollToAndPerform(action: T.() -> Unit) {
    scrollTo()
    action()
}
