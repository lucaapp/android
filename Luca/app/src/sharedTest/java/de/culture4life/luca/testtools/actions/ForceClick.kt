package de.culture4life.luca.testtools.actions

import android.view.View
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.matcher.ViewMatchers
import org.hamcrest.Matcher

/**
 * Workaround to click on buttons that are not fully visible.
 *
 * Needed for example in [PaymentRaffleBottomSheetFragmentTest.onSuccessResultIsSetAndFragmentDismissed()] because device height cannot be configured
 * for instrumentation test with our shared test setup and scrolling does not seem to work for bottom sheets in Robolectric tests.
 */
fun forceClick(): ViewAction {
    return object : ViewAction {
        override fun getConstraints(): Matcher<View> {
            return ViewMatchers.isEnabled()
        }

        override fun getDescription(): String {
            return "Force click without checking visibility"
        }

        override fun perform(uiController: UiController, view: View) {
            view.performClick()
        }
    }
}
