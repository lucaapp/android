package de.culture4life.luca.ui.payment.history

import androidx.test.espresso.assertion.ViewAssertions.matches
import de.culture4life.luca.R
import de.culture4life.luca.network.pojo.payment.CheckoutResponseData
import de.culture4life.luca.testtools.LucaFragmentTest
import de.culture4life.luca.testtools.actions.scrollToAndPerform
import de.culture4life.luca.testtools.matcher.RecyclerViewMatchers.Companion.canScrollUp
import de.culture4life.luca.testtools.mocks.IntentMocks
import de.culture4life.luca.testtools.pages.PaymentHistoryPage
import de.culture4life.luca.testtools.pages.PaymentHistoryPage.PaymentFlow.PaymentFlowScenario
import de.culture4life.luca.testtools.preconditions.ConsentPreconditions
import de.culture4life.luca.testtools.preconditions.MockServerPreconditions
import de.culture4life.luca.testtools.preconditions.PaymentPreconditions
import de.culture4life.luca.testtools.rules.FixedTimeRule
import de.culture4life.luca.testtools.rules.LucaFragmentScenarioRule
import de.culture4life.luca.testtools.samples.LocationSamples
import de.culture4life.luca.testtools.samples.PaymentHistorySamples
import de.culture4life.luca.testtools.samples.PaymentSamples
import de.culture4life.luca.ui.payment.children.PaymentAmountViewModel
import org.hamcrest.Matchers.not
import org.junit.Before
import org.junit.Ignore
import org.junit.Rule
import org.junit.Test
import java.net.HttpURLConnection.HTTP_BAD_REQUEST
import java.net.HttpURLConnection.HTTP_NOT_FOUND

class PaymentHistoryFragmentTest : LucaFragmentTest<PaymentHistoryFragment>(LucaFragmentScenarioRule.create()) {

    @get:Rule
    val fixedTimeRule = FixedTimeRule()

    private val paymentPreconditions = PaymentPreconditions()

    @Before
    fun setup() {
        setupDefaultWebServerMockResponses()
        ConsentPreconditions().givenConsentLucaPay(true)
    }

    @Test
    fun showEmptyViewIfNoResultsAreReturnedAndNoErrorHappened() {
        mockServerPreconditions.givenPaymentHistory()
        paymentPreconditions.givenSignedUpAndSignedIn()
        fragmentScenarioRule.launch()
        PaymentHistoryPage().run {
            emptyIcon.isDisplayed()
        }
    }

    @Test
    fun onFirstLoadFailedShowRetryButton() {
        mockServerPreconditions.givenHttpError(MockServerPreconditions.Route.PaymentHistory, HTTP_BAD_REQUEST)
        paymentPreconditions.givenSignedUpAndSignedIn()
        fragmentScenarioRule.launch()
        PaymentHistoryPage().run {
            paymentsList.run {
                childAt<PaymentHistoryPage.LoadingItem>(0) {
                    retryButton.isDisplayed()
                }
            }
        }
    }

    @Test
    fun onScrollToEndNewContentIsLoaded() {
        val history = PaymentHistorySamples.Paged()
        mockServerPreconditions.givenPaymentHistory(history)
        paymentPreconditions.givenSignedUpAndSignedIn()
        fragmentScenarioRule.launch()
        PaymentHistoryPage().run {
            paymentsList.run {
                assertSignIn()
                assertHistoryLoaded()
                scrollToEnd()
                assertPagingHistoryLoaded(history.cursors.first())
                scrollToEnd()
                hasSize(40)
            }
        }
    }

    @Test
    fun onScrollToEndNewContentIsLoadedButFailsShowsRetryButton() {
        mockServerPreconditions.givenFailingPagedPaymentHistory()
        paymentPreconditions.givenSignedUpAndSignedIn()
        fragmentScenarioRule.launch()
        PaymentHistoryPage().run {
            paymentsList.run {
                assertSignIn()
                assertHistoryLoaded()
                scrollToEnd()
                assertPagingHistoryLoaded()
                lastChild<PaymentHistoryPage.LoadingItem> {
                    retryButton.isDisplayed()
                    retryButton.click()
                    assertPagingHistoryLoaded()
                }
            }
        }
    }

    @Ignore("Flaky")
    @Test
    fun successfulCustomerPaymentFlow() {
        val location = LocationSamples.Restaurant()
        val payment = PaymentSamples.LocationPayment(location)
        val paymentFlowScenario = PaymentFlowScenario(this, payment)

        paymentPreconditions.givenSignedUpAndSignedIn()
        paymentPreconditions.givenStoredLastCheckoutId(payment.id)
        with(mockServerPreconditions) {
            givenPaymentHistory()
            givenLocation(location)
            givenPaymentCreateCheckout(payment)
        }

        fragmentScenarioRule.launch()

        PaymentHistoryPage().run {
            paymentsList.run {
                hasSize(0)
            }
            newPaymentButton.click()
            qrCodeScanner.scanner.scan(location.qrCodeContent)
            paymentFlow.runPaymentFlow(paymentFlowScenario)
            isDisplayed(fragmentScenarioRule.scenario)
            paymentsList.run {
                childAt<PaymentHistoryPage.PaymentItem>(0) {
                    isDisplayed()
                }
            }
        }
    }

    @Ignore("Flaky")
    @Test
    fun successfulOperatorPaymentFlow() {
        val location = LocationSamples.RestaurantWithTable()
        val payment = PaymentSamples.LocationPayment(location)
        val paymentFlowScenario = PaymentFlowScenario(this, payment, initiator = PaymentFlowScenario.INITIATOR.OPERATOR)

        paymentPreconditions.givenSignedUpAndSignedIn()
        paymentPreconditions.givenStoredLastCheckoutId(payment.id)
        with(mockServerPreconditions) {
            givenPaymentHistory()
            givenLocation(location)
            givenPaymentOpenRequest(payment)
            givenPaymentOpenRequestCreateCheckout(payment)
        }

        fragmentScenarioRule.launch()

        PaymentHistoryPage().run {
            paymentsList.run {
                hasSize(0)
            }
            newPaymentButton.click()
            qrCodeScanner.scanner.scan(location.qrCodeContent)
            paymentFlow.runPaymentFlow(paymentFlowScenario)
            isDisplayed(fragmentScenarioRule.scenario)
            paymentsList.run {
                childAt<PaymentHistoryPage.PaymentItem>(0) {
                    isDisplayed()
                }
            }
        }
    }

    @Test
    fun noPaymentExpiredErrorAfterSuccessfulPayment_operatorInitiatedPayment() {
        // LUCA-9698: Main reason was that we don't stop polling for open operator initiated payments to get updates (e.g. amount correction).
        //  But after successful payment we get 404 which should usually mean expired, failed, or just successful closed.

        val location = LocationSamples.RestaurantWithTable()
        val payment = PaymentSamples.LocationPayment(location)
        val paymentFlowScenario = PaymentFlowScenario(this, payment, initiator = PaymentFlowScenario.INITIATOR.OPERATOR)

        givenPaymentResultPage(payment, location, paymentFlowScenario)

        // After payment is done, no open payment available anymore.
        mockServerPreconditions.givenPaymentOpenRequest(payment, HTTP_NOT_FOUND)
        waitFor(PaymentAmountViewModel.PAYMENT_REQUEST_POLLING_INTERVAL)

        PaymentHistoryPage().run {
            // Before fix applied the flow was dismissed and error was displayed.
            paymentFlow.resultPage.run {
                successLayout.isDisplayed()
            }
        }
    }

    @Ignore("Flaky")
    @Test
    fun listScrolledToTopAfterAddingNewPayment() {
        // After a successful payment the list gets refreshed and should also scroll to the top after the refresh is done
        // Given
        val payments = PaymentHistorySamples.generatePaymentList(11)
        val history1 = PaymentHistorySamples.PreDefined(payments.takeLast(10))
        val history2 = PaymentHistorySamples.PreDefined(payments)
        val location = LocationSamples.RestaurantWithTable()
        val payment = PaymentSamples.LocationPayment(location)
        val paymentFlowScenario = PaymentFlowScenario(this@PaymentHistoryFragmentTest, payment, addHistory = history2)

        paymentPreconditions.givenSignedUpAndSignedIn()
        with(mockServerPreconditions) {
            givenPaymentHistory(history1)
            givenLocation(location)
            givenPaymentCreateCheckout(payment)
        }

        // When/ Then
        fragmentScenarioRule.launch()
        PaymentHistoryPage().run {
            paymentsList.run {
                // Assert initial state
                childAt<PaymentHistoryPage.PaymentItem>(0) { isDisplayed(payments[1]) }
                scrollToEnd()
                view.check(matches(canScrollUp()))

                // Add new payment
                newPaymentButton.click()
                qrCodeScanner.scanner.scan(location.qrCodeContent)
                paymentFlow.runPaymentFlow(paymentFlowScenario)
                isDisplayed(fragmentScenarioRule.scenario)

                // Assert refreshed/scrolled-to-top state
                view.check(matches(not(canScrollUp())))
                childAt<PaymentHistoryPage.PaymentItem>(0) { isDisplayed(payments[0]) }
            }
        }
    }

    private fun givenPaymentResultPage(
        payment: PaymentSamples.LocationPayment,
        location: LocationSamples.RestaurantWithTable,
        paymentFlowScenario: PaymentFlowScenario
    ) {
        paymentPreconditions.givenSignedUpAndSignedIn()
        paymentPreconditions.givenStoredLastCheckoutId(payment.id)
        with(mockServerPreconditions) {
            givenPaymentHistory()
            givenLocation(location)
            givenPaymentOpenRequest(payment)
            givenPaymentOpenRequestCreateCheckout(payment)
        }

        fragmentScenarioRule.launch()

        PaymentHistoryPage().run {
            newPaymentButton.click()
            qrCodeScanner.scanner.scan(location.qrCodeContent)
            paymentFlow.runPaymentFlowUntilResult(paymentFlowScenario)
        }
    }

    @Test
    fun paymentFlowOnRecreate() {
        val location = LocationSamples.Restaurant()
        val payment = PaymentSamples.LocationPayment(location)

        paymentPreconditions.givenSignedUpAndSignedIn()
        paymentPreconditions.givenStoredLastCheckoutId(payment.id)
        with(mockServerPreconditions) {
            givenPaymentHistory()
            givenLocation(location)
            givenPaymentCreateCheckout(payment)
        }

        fragmentScenarioRule.launch()
        PaymentHistoryPage().run {
            newPaymentButton.click()
            qrCodeScanner.scanner.scan(location.qrCodeContent)
            paymentFlow.run {
                amountPage.isDisplayed()
                amountPage.amountEditText.replaceText("100")
            }
        }

        fragmentScenarioRule.scenario.recreate()
        PaymentHistoryPage().run {
            paymentFlow.run {
                amountPage.isDisplayed()
                amountPage.continueButton.scrollToAndPerform { click() }
                tipPage.isDisplayed()
            }
        }

        fragmentScenarioRule.scenario.recreate()
        PaymentHistoryPage().run {
            paymentFlow.tipPage.isDisplayed()
        }
    }

    @Ignore("Flaky")
    @Test
    fun canceledPaymentFlow() {
        val location = LocationSamples.Restaurant()
        val payment = PaymentSamples.LocationPayment(location)

        paymentPreconditions.givenSignedUpAndSignedIn()
        paymentPreconditions.givenStoredLastCheckoutId(payment.id)
        with(mockServerPreconditions) {
            givenLocation(location)
            givenPaymentCreateCheckout(payment)
        }

        fragmentScenarioRule.launch()

        PaymentHistoryPage().run {
            newPaymentButton.click()
            qrCodeScanner.scanner.scan(location.qrCodeContent)
            paymentFlow.run {
                amountPage.run {
                    amountEditText.replaceText("1234")
                    amountEditText.pressImeAction()
                }
                tipPage.run {
                    isDisplayed()
                    totalAmount.hasText("13,57 €")
                    selectPercentage(15)
                    totalAmount.hasText("14,19 €")
                    IntentMocks.givenWebAppResponse(payment.checkoutUrl, scenario = fragmentScenarioRule) {
                        mockServerPreconditions.givenPaymentCheckout(payment, CheckoutResponseData.Payment.Status.UNKNOWN)
                    }
                    primaryActionButton.scrollToAndPerform { click() }
                }
                resultPage.run {
                    errorLayout.isDisplayed()
                    IntentMocks.givenWebAppResponse(payment.checkoutUrl, scenario = fragmentScenarioRule) {
                        mockServerPreconditions.givenPaymentCheckout(payment)
                    }
                    retryButton.scrollToAndPerform { click() }
                }
                resultPage.run {
                    successLayout.isDisplayed()
                    nextButton.scrollToAndPerform { click() }
                }
            }
            isDisplayed(fragmentScenarioRule.scenario)
        }
    }

    @Test
    fun navigateToPaymentDetails() {
        paymentPreconditions.givenSignedUpAndSignedIn()
        mockServerPreconditions.givenPaymentHistory(PaymentHistorySamples.Paged())
        launchScenario(R.id.paymentHistoryFragment)
        PaymentHistoryPage().run {
            paymentsList.run {
                assertSignIn()
                assertHistoryLoaded()
                childAt<PaymentHistoryPage.PaymentItem>(0) {
                    isDisplayed()
                    click()
                    assertNavigationDestination(R.id.paymentHistoryDetailFragment)
                }
            }
        }
    }

    @Test
    fun navigateBackToPaymentWhenNotSignedUp() {
        launchScenario(R.id.paymentHistoryFragment)
        assertNavigationDestination(R.id.paymentFragment)
    }

    private fun assertSignIn() {
        mockServerPreconditions.assert(MockServerPreconditions.Route.PaymentSignUp)
        mockServerPreconditions.assert(MockServerPreconditions.Route.PaymentSignIn)
    }

    private fun assertHistoryLoaded() {
        mockServerPreconditions.assert(MockServerPreconditions.Route.PaymentHistory)
    }

    private fun assertPagingHistoryLoaded(cursor: String = "cursor123") {
        mockServerPreconditions.assert(MockServerPreconditions.Route.PaymentHistoryPaging, mapOf("cursor" to cursor))
    }

    private fun setupDefaultWebServerMockResponses() {
        with(mockServerPreconditions) {
            givenTimeSync()
            givenPaymentSignUp()
            givenPaymentSignIn()
            givenNoOpenPaymentRequest()
        }
    }
}
