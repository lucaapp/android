package de.culture4life.luca.testtools.pages.dialogs

import de.culture4life.luca.R
import de.culture4life.luca.testtools.pages.dialogs.base.DefaultOkContinueDialog
import de.culture4life.luca.testtools.pages.dialogs.base.SafeDialogInteraction

class UserAuthenticationNotActivatedDialog : DefaultOkContinueDialog() {

    init {
        SafeDialogInteraction.apply(this)
    }

    override fun isDisplayed() {
        baseDialog.title.hasText(R.string.authentication_error_not_activated_title)
        baseDialog.message.hasText(R.string.authentication_error_not_activated_description)
    }
}
