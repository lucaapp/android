package de.culture4life.luca.testtools.pages.dialogs

import de.culture4life.luca.R
import de.culture4life.luca.consent.ConsentManager
import de.culture4life.luca.testtools.pages.dialogs.base.DefaultConsentDialog
import de.culture4life.luca.testtools.pages.dialogs.base.SafeDialogInteraction

class ConsentDialog(val id: String) : DefaultConsentDialog() {

    init {
        SafeDialogInteraction.apply(this)
    }

    override fun isDisplayed() {
        when (id) {
            ConsentManager.ID_ACTIVATE_LUCA_PAY -> lucaPayIsDisplayed()
            ConsentManager.ID_IMPORT_DOCUMENT -> importDocumentIsDisplayed()
            ConsentManager.ID_TERMS_OF_SERVICE_INFO -> termsOfServiceInfoIsDisplayed()
            ConsentManager.ID_TERMS_OF_SERVICE_LUCA_ID -> termsOfServiceLucaIdIsDisplayed() // same as ID_TERMS_OF_SERVICE_LUCA_PAY
            else -> throw NotImplementedError("ConsentDialog: $id is not defined")
        }
    }

    private fun lucaPayIsDisplayed() {
        title.hasText(R.string.consent_title)
        info.hasText(getStringFromHtml(R.string.pay_activation_consent))
        acceptButton.hasText(R.string.updated_terms_button_agree)
        acceptButton.isDisplayed()
        cancelButton.isNotDisplayed()
    }

    private fun importDocumentIsDisplayed() {
        title.hasText(R.string.consent_title)
        info.hasText(R.string.consent_import_document_description)
        acceptButton.hasText(R.string.document_import_action)
        acceptButton.isDisplayed()
        cancelButton.isNotDisplayed()
    }

    private fun termsOfServiceInfoIsDisplayed() {
        title.hasText(R.string.consent_terms_of_service_info_title)
        info.hasText(getStringFromHtml(R.string.consent_terms_of_service_info_description))
        acceptButton.hasText(R.string.action_continue)
        acceptButton.isDisplayed()
        cancelButton.isNotDisplayed()
    }

    private fun termsOfServiceLucaIdIsDisplayed() {
        title.hasText(R.string.consent_terms_of_service_title)
        info.hasText(getStringFromHtml(R.string.consent_terms_of_service_description))
        acceptButton.hasText(R.string.updated_terms_button_agree)
        acceptButton.isDisplayed()
        cancelButton.isNotDisplayed()
    }
}
