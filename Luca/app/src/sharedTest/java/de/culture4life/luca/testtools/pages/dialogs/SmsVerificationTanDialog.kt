package de.culture4life.luca.testtools.pages.dialogs

import de.culture4life.luca.R
import de.culture4life.luca.testtools.pages.dialogs.base.DefaultOkCancelDialog
import de.culture4life.luca.testtools.pages.dialogs.base.SafeDialogInteraction
import io.github.kakaocup.kakao.edit.KEditText
import io.github.kakaocup.kakao.text.KButton

class SmsVerificationTanDialog : DefaultOkCancelDialog() {

    val tanInput = KEditText { withId(R.id.tanInputEditText) }
    val infoButton = KButton { withId(R.id.infoImageView) }

    init {
        SafeDialogInteraction.apply(this)
    }

    override fun isDisplayed() {
        baseDialog.title.containsText(context.getString(R.string.verification_enter_tan_title))
        baseDialog.positiveButton.containsText(context.getString(R.string.action_ok))
    }
}
