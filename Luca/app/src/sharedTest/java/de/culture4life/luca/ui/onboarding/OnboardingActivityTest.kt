package de.culture4life.luca.ui.onboarding

import android.app.Activity.RESULT_OK
import android.app.Instrumentation
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.Intents.intending
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import de.culture4life.luca.consent.ConsentManager
import de.culture4life.luca.testtools.LucaActivityTest
import de.culture4life.luca.testtools.pages.OnboardingPage
import de.culture4life.luca.testtools.preconditions.ConsentPreconditions
import de.culture4life.luca.testtools.rules.LucaActivityScenarioRule
import de.culture4life.luca.ui.registration.RegistrationActivity
import org.junit.Before
import org.junit.Test
import java.util.*

class OnboardingActivityTest : LucaActivityTest<OnboardingActivity>(LucaActivityScenarioRule.create()) {

    private val onboardingPage = OnboardingPage()

    @Before
    fun before() {
        ConsentPreconditions().givenConsent(ConsentManager.ID_TERMS_OF_SERVICE_LUCA_ID, true)
    }

    @Test
    fun acceptingTermsResultsInRegistrationScreen() {
        // Given
        activityScenarioRule.launch()
        intending(hasComponent(RegistrationActivity::class.java.name)).respondWith(Instrumentation.ActivityResult(RESULT_OK, null))

        // When
        onboardingPage.run {
            termsCheckBox.click()
            privacyPolicyCheckBox.click()
            finishButton.click()
        }

        // Then
        intended(hasComponent(RegistrationActivity::class.java.name))
    }
}
