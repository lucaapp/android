package de.culture4life.luca.testtools.pages

import de.culture4life.luca.R
import io.github.kakaocup.kakao.text.KButton

class DiscoveryPage {

    val discoverButton = KButton { withId((R.id.discoverButton)) }

}
