package de.culture4life.luca.testtools

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelStoreOwner
import de.culture4life.luca.ui.SharedViewModelScopeProvider

/**
 * Container that provides a shared [ViewModelStoreOwner] so that a [BaseFlowChildFragment] can be tested in isolation.
 * Once started use [createActualChildFragment] to instantiate the actual fragment that should be tested. Arguments that this fragment
 * receive will be passed to fragment that is created via [createActualChildFragment]
 *
 * Example usage:
 *
 * ```
 * class MyTest: LucaFragmentTest<DummySharedViewModelScopeProvider>(LucaFragmentScenarioRule.create())
 * ...
 * fragmentScenarioRule.launch()
 * fragmentScenarioRule.scenario.onFragment {
 *      it.createActualChildFragment(PaymentResultFragment::class.java)
 *      val sharedViewModel = ViewModelProvider(it.sharedViewModelStoreOwner).get(PaymentFlowViewModel::class.java)
 * }
 * ...
 * ```
 */
class DummySharedViewModelScopeProvider : Fragment(), SharedViewModelScopeProvider {

    override val sharedViewModelStoreOwner: ViewModelStoreOwner
        get() = this

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return FrameLayout(requireContext()).apply {
            id = CONTAINER_ID
            layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT)
        }
    }

    fun createActualChildFragment(fragmentClass: Class<out Fragment>) {
        childFragmentManager.beginTransaction().replace(CONTAINER_ID, fragmentClass, arguments).commitNow()
    }

    companion object {
        private const val CONTAINER_ID = 123
    }
}
