package de.culture4life.luca.testtools.pages.dialogs

import de.culture4life.luca.R
import de.culture4life.luca.testtools.pages.dialogs.base.DefaultOkCancelDialog
import de.culture4life.luca.testtools.pages.dialogs.base.SafeDialogInteraction

class SmsVerificationTanInfoDialog : DefaultOkCancelDialog() {

    init {
        SafeDialogInteraction.apply(this)
    }

    override fun isDisplayed() {
        baseDialog.title.containsText(context.getString(R.string.verification_tan_delayed_title))
        baseDialog.positiveButton.containsText(context.getString(R.string.action_ok))
    }
}
