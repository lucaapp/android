package de.culture4life.luca.testtools.pages.dialogs

import de.culture4life.luca.R
import de.culture4life.luca.testtools.pages.dialogs.base.DefaultOkDialog
import de.culture4life.luca.testtools.pages.dialogs.base.SafeDialogInteraction

class UpdateDialog : DefaultOkDialog() {

    init {
        SafeDialogInteraction.apply(this)
    }

    override fun isDisplayed() {
        // TODO: add isCancelable=False logic if required
        baseDialog.title.hasText(R.string.update_required_title)
        baseDialog.message.hasText(R.string.update_required_description)
        okButton.hasText(R.string.action_update)
    }
}
