package de.culture4life.luca.ui

import de.culture4life.luca.consent.ConsentManager
import de.culture4life.luca.testtools.LucaActivityTest
import de.culture4life.luca.testtools.pages.CheckInPage
import de.culture4life.luca.testtools.pages.MainPage
import de.culture4life.luca.testtools.pages.MyLucaPage
import de.culture4life.luca.testtools.pages.VenueDetailsPage
import de.culture4life.luca.testtools.preconditions.*
import de.culture4life.luca.testtools.rules.FixedTimeRule
import de.culture4life.luca.testtools.rules.LucaActivityScenarioRule
import de.culture4life.luca.testtools.samples.CovidDocumentSamples
import de.culture4life.luca.testtools.samples.LocationSamples
import de.culture4life.luca.ui.splash.SplashActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class NavigationTest : LucaActivityTest<SplashActivity>(LucaActivityScenarioRule.create()) {

    @get:Rule
    val fixedTimeRule = FixedTimeRule()

    private val registrationPreconditions = RegistrationPreconditions()
    private val dailyKeyPreconditions = DailyKeyPreconditions()
    private val consentPreconditions = ConsentPreconditions()

    @Before
    fun setup() {
        TermsPreconditions().givenTermsAccepted()
        OnboardingPreconditions().givenOnboardingSeen()
        WhatIsNewPreconditions().givenAllNewsSeen()
        consentPreconditions.givenConsent(ConsentManager.ID_TERMS_OF_SERVICE_LUCA_ID, true)

        mockServerPreconditions.givenTimeSync()
    }

    @Test
    fun checkInDeeplink_navigateToMyLuca() {
        launchAppWithLocationDeeplink()

        VenueDetailsPage().title.isDisplayed()
        assertMyLucaIsDisplayedWhenSelected()
    }

    @Test
    fun documentDeeplink_navigateToCheckIn() {
        launchAppWithTestDeeplink()

        MyLucaPage().run {
            consentsDialog.acceptButton.click()
            isDisplayed()
        }

        assertCheckInIsDisplayedWhenSelected()
    }

    private fun launchAppWithLocationDeeplink() {
        val location = LocationSamples.Restaurant()
        val deepLink = location.deeplink
        registrationPreconditions.givenRegisteredUser()
        dailyKeyPreconditions.givenStoredDailyKey()
        mockServerPreconditions.givenLocation(location)

        activityScenarioRule.launchDeepLink(deepLink)
    }

    private fun launchAppWithTestDeeplink() {
        val document = CovidDocumentSamples.ErikaMustermann.EudccFastNegative()
        val deeplink = document.asDeepLink()
        registrationPreconditions.givenRegisteredUser(document.person)
        mockServerPreconditions.givenRedeemDocument()

        activityScenarioRule.launchDeepLink(deeplink)
    }

    private fun assertMyLucaIsDisplayedWhenSelected() {
        MainPage().navigation.myLuca.click()
        MyLucaPage().isDisplayed()
    }

    private fun assertCheckInIsDisplayedWhenSelected() {
        MainPage().navigation.checkIn.click()
        CheckInPage().isDisplayed()
    }
}
