package de.culture4life.luca.testtools.pages.dialogs

import de.culture4life.luca.R
import de.culture4life.luca.testtools.pages.dialogs.base.DefaultDialog
import de.culture4life.luca.testtools.pages.dialogs.base.SafeDialogInteraction
import de.culture4life.luca.testtools.pages.elements.QrCodeScannerElement
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView

class QrCodeScannerBottomSheet : DefaultDialog {

    val title = KTextView { withId(R.id.scanQrCodeTitleTextView) }
    val description = KTextView { withId(R.id.scanQrCodeDescriptionTextView) }
    val cancelButton = KButton { withId(R.id.cancelButton) }

    val scanner = QrCodeScannerElement()

    init {
        SafeDialogInteraction.apply(this)
    }

    override fun isDisplayed() {
        title.isGone()
        description.isGone()
        cancelButton.isDisplayed()
        cancelButton.hasText(R.string.action_cancel)
    }
}
