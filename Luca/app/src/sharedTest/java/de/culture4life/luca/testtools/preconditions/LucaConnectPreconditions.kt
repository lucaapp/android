package de.culture4life.luca.testtools.preconditions

import androidx.test.core.app.ApplicationProvider
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.consent.ConsentManager
import de.culture4life.luca.health.ResponsibleHealthDepartment
import de.culture4life.luca.testtools.samples.CovidDocumentSamples
import de.culture4life.luca.testtools.samples.HealthDepartmentSamples

class LucaConnectPreconditions {

    private val application = ApplicationProvider.getApplicationContext<LucaApplication>()
    private val healthDepartment by lazy { application.getInitializedManager(application.healthDepartmentManager).blockingGet() }

    fun givenLucaConnect(zipCode: String = CovidDocumentSamples.ErikaMustermann().registrationData().postalCode!!) {
        ConsentPreconditions().givenConsent(ConsentManager.ID_POSTAL_CODE_MATCHING, true)
        val department = ResponsibleHealthDepartment(HealthDepartmentSamples.healthDepartment(zipCode), zipCode)
        healthDepartment.persistResponsibleHealthDepartment(department).blockingAwait()
    }
}
