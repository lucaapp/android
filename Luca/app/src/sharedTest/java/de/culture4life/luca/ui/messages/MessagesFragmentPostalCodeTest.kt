package de.culture4life.luca.ui.messages

import de.culture4life.luca.consent.ConsentManager
import de.culture4life.luca.testtools.LucaFragmentTest
import de.culture4life.luca.testtools.pages.MessagesPage
import de.culture4life.luca.testtools.preconditions.ConsentPreconditions
import de.culture4life.luca.testtools.preconditions.RegistrationPreconditions
import de.culture4life.luca.testtools.rules.LucaFragmentScenarioRule
import de.culture4life.luca.testtools.samples.CovidDocumentSamples
import org.junit.Before
import org.junit.Test

class MessagesFragmentPostalCodeTest : LucaFragmentTest<MessagesFragment>(LucaFragmentScenarioRule.create()) {

    private val personWithPostalCode = CovidDocumentSamples.ErikaMustermann().registrationData()
    private val personWithoutPostalCode = personWithPostalCode.copy(postalCode = "")

    private val registrationPreconditions = RegistrationPreconditions()
    private val messagesPage = MessagesPage()

    @Before
    fun setup() {
        // hide messages not relevant for this test class
        ConsentPreconditions().givenConsent(ConsentManager.ID_TERMS_OF_SERVICE_LUCA_ID, true)
    }

    @Test
    fun postalCodeAvailable() {
        registrationPreconditions.givenRegisteredUser(personWithPostalCode)
        fragmentScenarioRule.launch()

        messagesPage.messageList.childAt<MessagesPage.MessageItem>(0) { assertIsPostalCodeMessage() }
    }

    @Test
    fun postalCodeNotGiven() {
        registrationPreconditions.givenRegisteredUser(personWithoutPostalCode)
        fragmentScenarioRule.launch()

        MessagesPage().run {
            messageList.hasSize(0)
        }
    }

    @Test
    fun postalCodeUpdate() {
        postalCodeNotGiven()

        registrationPreconditions.updateRegisteredUser { personWithPostalCode }
        waitFor(MessagesViewModel.WHAT_IS_NEW_MESSAGE_UPDATE_DELAY)
        messagesPage.messageList.childAt<MessagesPage.MessageItem>(0) { assertIsPostalCodeMessage() }

        registrationPreconditions.updateRegisteredUser { personWithoutPostalCode }
        waitFor(MessagesViewModel.WHAT_IS_NEW_MESSAGE_UPDATE_DELAY)
        messagesPage.messageList.hasSize(0)
    }
}
