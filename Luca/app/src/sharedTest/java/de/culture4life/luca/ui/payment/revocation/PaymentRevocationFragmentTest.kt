package de.culture4life.luca.ui.payment.revocation

import android.content.ClipboardManager
import android.content.Context
import de.culture4life.luca.testtools.LucaFragmentTest
import de.culture4life.luca.testtools.mocks.IntentMocks
import de.culture4life.luca.testtools.pages.account.PaymentRevocationPage
import de.culture4life.luca.testtools.preconditions.ConsentPreconditions
import de.culture4life.luca.testtools.preconditions.MockServerPreconditions.Companion.REVOCATION_CODE
import de.culture4life.luca.testtools.preconditions.WhatIsNewPreconditions
import de.culture4life.luca.testtools.rules.LucaFragmentScenarioRule
import de.culture4life.luca.whatisnew.WhatIsNewManager.Companion.ID_LUCA_PAY_REVOCATION_CODE_MESSAGE
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class PaymentRevocationFragmentTest : LucaFragmentTest<PaymentRevocationFragment>(LucaFragmentScenarioRule.create()) {

    private val whatIsNewPreconditions = WhatIsNewPreconditions()

    @Before
    fun setup() {
        ConsentPreconditions().givenConsentLucaPay(true)
    }

    @Test
    fun markAsSeen() {
        // Given
        whatIsNewPreconditions.givenMessageEnabled(ID_LUCA_PAY_REVOCATION_CODE_MESSAGE, true)
        getInitializedManager(application.whatIsNewManager)

        // When
        whatIsNewPreconditions.assertMessageMarkedAsSeen(ID_LUCA_PAY_REVOCATION_CODE_MESSAGE, false)
        fragmentScenarioRule.launch()

        // Then
        whatIsNewPreconditions.assertMessageMarkedAsSeen(ID_LUCA_PAY_REVOCATION_CODE_MESSAGE, true)
    }

    @Test
    fun showRecoveryToken() {
        // Given
        givenSignedUp()

        // When
        fragmentScenarioRule.launch()

        // Then
        PaymentRevocationPage().run {
            revocationCode.hasText(REVOCATION_CODE)
        }
    }

    @Test
    fun copyToClipboard() {
        // Given
        givenSignedUp()

        // When
        fragmentScenarioRule.launch()

        // Then
        PaymentRevocationPage().run {
            copyButton.click()
            val clipboardManager = (application.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager)
            val clipboardText = clipboardManager.primaryClip!!.getItemAt(0).text.toString()
            assertEquals(REVOCATION_CODE, clipboardText)
        }
    }

    @Test
    fun shareFile() {
        // Given
        givenSignedUp()

        // When
        fragmentScenarioRule.launch()

        // Then
        PaymentRevocationPage().run {
            shareButton.click()
            IntentMocks.assertShareFileChooserIntent()
        }
    }

    private fun givenSignedUp() {
        mockServerPreconditions.givenPaymentSignIn()
        mockServerPreconditions.givenPaymentSignUp()
        getInitializedManager(application.paymentManager).signUp().blockingAwait()
    }
}
