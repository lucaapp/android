package de.culture4life.luca.testtools.pages.dialogs.base

import android.view.InputDevice
import android.view.MotionEvent
import android.view.View
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.GeneralClickAction
import androidx.test.espresso.action.GeneralLocation
import androidx.test.espresso.action.Press
import androidx.test.espresso.action.Tap
import de.culture4life.luca.KakaoConfiguration
import de.culture4life.luca.LucaApplication
import io.github.kakaocup.kakao.common.views.KBaseView
import org.hamcrest.Matcher
import java.lang.reflect.Field

object SafeDialogInteraction {

    fun apply(dialog: DefaultDialog) {
        apply(dialog) { _, _ ->
            // self check the content of the expected dialog
            dialog.isDisplayed()
        }
    }

    fun apply(
        obj: Any,
        isEmbeddedInDialog: Boolean = true,
        additionalInterception: (viewInteraction: ViewInteraction, viewAction: ViewAction) -> Unit = { _, _ -> }
    ) {
        if (!isEmbeddedInDialog) {
            return
        }

        obj.javaClass.getAllDeclaredFields().forEach { field ->
            val fieldValue = getFieldValue(field, obj)
            requireNotNull(fieldValue) {
                """
                    Declared field was null, make sure that apply is called after fields are created.

                    For example if SafeDialogInteraction2.apply() is used in an init block put the
                    init block after the declaration of the page fields.
                """.trimIndent()
            }

            addIsDialogRootMatcher(fieldValue)
            addRobolectricClickWorkaround(fieldValue, additionalInterception)
        }
    }

    /**
     * Returns an array of Field objects reflecting all the fields declared by the class or interface represented by this Class object.
     * This includes public, protected, default (package) access, and private fields including inherited fields.
     */
    private fun Class<*>.getAllDeclaredFields(): List<Field> {
        var tClass: Class<*>? = this
        val fields: MutableList<Field> = mutableListOf()
        while (tClass != null) {
            fields.addAll(tClass.declaredFields)
            tClass = tClass.superclass
        }
        return fields
    }

    private fun addRobolectricClickWorkaround(
        fieldValue: Any,
        additionalInterception: (viewInteraction: ViewInteraction, viewAction: ViewAction) -> Unit = { _, _ -> }
    ) {
        if (fieldValue is KBaseView<*>) {
            fieldValue.also { interceptOnPerform(it, additionalInterception) }
        }
    }

    private fun addIsDialogRootMatcher(fieldValue: Any) {
        if (fieldValue is KBaseView<*>) {
            fieldValue.also { it.inRoot { isDialog() } }
        }
    }

    private fun getFieldValue(field: Field, obj: Any): Any? {
        field.isAccessible = true
        return field.get(obj)
    }

    private fun interceptOnPerform(
        builder: KBaseView<*>,
        additionalInterception: (viewInteraction: ViewInteraction, viewAction: ViewAction) -> Unit = { _, _ -> }
    ) {
        builder.intercept {
            onPerform(true) { viewInteraction, viewAction ->
                KakaoConfiguration.onPerformDefaultInterceptors(viewInteraction, viewAction)
                additionalInterception(viewInteraction, viewAction)
                if (DirectClickAction.isSingleClickWithRobolectric(viewAction)) {
                    // Sometimes the default click does not trigger the click lister.
                    // Root cause could be when multiple "isDialog" roots exists (e.g. BottomSheet + AlertDialog)
                    viewInteraction.perform(DirectClickAction())
                } else {
                    viewInteraction.perform(viewAction)
                }
            }
        }
    }

    private class DirectClickAction : ViewAction {

        override fun getConstraints(): Matcher<View> {
            return generalClickAction.constraints
        }

        override fun getDescription(): String {
            return "${generalClickAction.description} directly"
        }

        override fun perform(uiController: UiController, view: View) {
            view.performClick()
            uiController.loopMainThreadUntilIdle()
        }

        companion object {
            private val generalClickAction = GeneralClickAction(
                Tap.SINGLE,
                GeneralLocation.VISIBLE_CENTER,
                Press.FINGER,
                InputDevice.SOURCE_UNKNOWN,
                MotionEvent.BUTTON_PRIMARY
            )

            fun isSingleClickWithRobolectric(viewAction: ViewAction): Boolean {
                return LucaApplication.isRunningUnitTests() &&
                    viewAction is GeneralClickAction &&
                    viewAction.description == generalClickAction.description
            }
        }
    }
}
