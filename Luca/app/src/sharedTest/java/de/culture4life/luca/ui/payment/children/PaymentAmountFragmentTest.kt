package de.culture4life.luca.ui.payment.children

import androidx.core.os.bundleOf
import de.culture4life.luca.payment.PaymentAmounts
import de.culture4life.luca.testtools.LucaFlowFragmentTest
import de.culture4life.luca.testtools.pages.PaymentAmountPage
import de.culture4life.luca.testtools.samples.LocationSamples
import de.culture4life.luca.testtools.samples.LucaPaySamples
import de.culture4life.luca.testtools.samples.PaymentSamples
import de.culture4life.luca.testtools.util.FragmentFinder
import de.culture4life.luca.ui.payment.PaymentFlowViewModel
import org.junit.Test

class PaymentAmountFragmentTest : LucaFlowFragmentTest<PaymentAmountFragment, PaymentFlowViewModel>(
    PaymentAmountFragment::class.java,
    PaymentFlowViewModel::class.java
) {

    private val page = PaymentAmountPage(isEmbeddedInDialog = false)

    @Test
    fun enteredAmountIsDisplayed() {
        launchScenario()
        val amount = 1234
        givenEnteredAmount(amount)
        page.assertAmountDisplayed(amount)
    }

    @Test
    fun adjustingToValidAmountHidesError() {
        launchScenario()

        givenInvalidAmount()
        page.continueButton.click()

        givenValidAmount()
        page.errorTextView.isInvisible()
    }

    @Test
    fun selectingInvalidAmountShowsError() {
        launchScenario()
        givenInvalidAmount()
        page.continueButton.click()
        page.errorTextView.isVisible()
    }

    @Test
    fun noErrorShownByDefault() {
        launchScenario()
        page.errorTextView.isInvisible()
        givenInvalidAmount()
        page.errorTextView.isInvisible() // still invisible because amount has not been selected
    }

    @Test
    fun onRecreate() {
        launchScenario()

        givenEnteredAmount(1234)
        recreateScenario()
        page.assertAmountDisplayed(1234)

        givenEnteredAmount(4321)
        recreateScenario()
        page.assertAmountDisplayed(4321)
    }

    @Test
    fun discountCampaign_customerInitiated() {
        val location = LocationSamples.Restaurant()
        val discountCampaign = LucaPaySamples.Campaign.discount50(location.locationId)

        mockServerPreconditions.givenLocation(location)
        mockServerPreconditions.givenCampaigns(discountCampaign.locationId, discountCampaign)

        launchScenario(bundleOf(PaymentFlowViewModel.ARGUMENT_PAYMENT_LOCATION_DATA_KEY to location.locationData))

        givenEnteredAmount(1234)
        page.assertAmountDisplayed(1234)
    }

    @Test
    fun discountCampaign_operatorInitiated() {
        val location = LocationSamples.RestaurantWithTable()
        val openPayment = PaymentSamples.LocationPayment(location)
        val discountCampaign = LucaPaySamples.Campaign.discount50(location.locationId)
        val expectedAmounts = PaymentAmounts(
            invoiceAmount = openPayment.invoiceAmount,
            tipPercentage = 0,
            discountPercentage = discountCampaign.discountPercentage!!,
            maximumDiscountAmount = discountCampaign.maximumDiscountAmount!!
        )

        mockServerPreconditions.givenLocation(location)
        mockServerPreconditions.givenPaymentOpenRequest(openPayment)
        mockServerPreconditions.givenCampaigns(discountCampaign.locationId, discountCampaign)

        launchScenario(bundleOf(PaymentFlowViewModel.ARGUMENT_PAYMENT_LOCATION_DATA_KEY to location.locationData))

        page.assertAmountDisplayedWithDiscount(expectedAmounts)

        // reset discount campaign (like ended just now)
        FragmentFinder.find(PaymentAmountFragment::class.java).viewModel.paymentAmounts
            .postValue(PaymentAmounts(openPayment.invoiceAmount))
        // expect discount ui changes are reverted
        page.assertAmountDisplayed(openPayment.invoiceAmount)
    }

    @Test
    fun operatorInitiated_withoutTable() {
        val location = LocationSamples.RestaurantWithoutTables()
        val openPayment = PaymentSamples.LocationPayment(location)

        mockServerPreconditions.givenLocation(location)
        mockServerPreconditions.givenPaymentOpenRequest(openPayment)

        launchScenario(bundleOf(PaymentFlowViewModel.ARGUMENT_PAYMENT_LOCATION_DATA_KEY to location.locationData))

        page.assertAmountDisplayed(openPayment.invoiceAmount)
    }

    @Test
    fun operatorInitiated_partialPayment() {
        val location = LocationSamples.RestaurantWithoutTables()
        val openPayment = PaymentSamples.LocationPayment(location)

        mockServerPreconditions.givenLocation(location)
        mockServerPreconditions.givenPaymentOpenRequest(openPayment)

        launchScenario(bundleOf(PaymentFlowViewModel.ARGUMENT_PAYMENT_LOCATION_DATA_KEY to location.locationData))

        page.partialContinueButton.isDisplayed()
    }

    @Test
    fun operatorInitiated_noPartialPayment() {
        val location = LocationSamples.RestaurantWithoutTables()
        val openPayment = PaymentSamples.LocationPayment(location, partialAmountsSupported = false)

        mockServerPreconditions.givenLocation(location)
        mockServerPreconditions.givenPaymentOpenRequest(openPayment)

        launchScenario(bundleOf(PaymentFlowViewModel.ARGUMENT_PAYMENT_LOCATION_DATA_KEY to location.locationData))

        page.partialContinueButton.isNotDisplayed()
    }

    @Test
    fun partialPaymentButtonNotDisplayedByDefault() {
        val location = LocationSamples.RestaurantWithoutTables()
        launchScenario(bundleOf(PaymentFlowViewModel.ARGUMENT_PAYMENT_LOCATION_DATA_KEY to location.locationData))
        page.partialContinueButton.isNotDisplayed()
    }

    private fun givenInvalidAmount() {
        givenEnteredAmount(12)
    }

    private fun givenValidAmount() {
        givenEnteredAmount(1234)
    }

    private fun givenEnteredAmount(amount: Int) {
        page.amountEditText.run {
            replaceText(amount.toString())
        }
    }
}
