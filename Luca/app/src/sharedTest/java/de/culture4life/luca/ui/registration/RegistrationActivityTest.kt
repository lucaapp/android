package de.culture4life.luca.ui.registration

import de.culture4life.luca.R
import de.culture4life.luca.testtools.LucaActivityTest
import de.culture4life.luca.testtools.actions.scrollToAndPerform
import de.culture4life.luca.testtools.pages.RegistrationPage
import de.culture4life.luca.testtools.pages.dialogs.RequestErrorDialog
import de.culture4life.luca.testtools.pages.dialogs.SmsVerificationExplanationDialog
import de.culture4life.luca.testtools.preconditions.MockServerPreconditions
import de.culture4life.luca.testtools.preconditions.RegistrationPreconditions
import de.culture4life.luca.testtools.rules.LucaActivityScenarioRule
import de.culture4life.luca.testtools.samples.CovidDocumentSamples
import de.culture4life.luca.ui.registration.RegistrationActivity.REGISTRATION_COMPLETED_SCREEN_SEEN_KEY
import org.junit.Assert.assertEquals
import org.junit.Ignore
import org.junit.Test
import java.net.HttpURLConnection.HTTP_BAD_GATEWAY
import java.net.HttpURLConnection.HTTP_BAD_REQUEST

class RegistrationActivityTest : LucaActivityTest<RegistrationActivity>(LucaActivityScenarioRule.create()) {

    private val person = CovidDocumentSamples.ErikaMustermann()
    private val registrationData = person.registrationData()
    private val editedUserData = registrationData.copy(
        firstName = "Max",
        lastName = "Mustermann",
        phoneNumber = "12345",
        email = "max@mustermann.de",
        street = "Blumenweg",
        houseNumber = "12",
        city = "Berlin",
        postalCode = "12345"
    )
    private val registrationPage = RegistrationPage()

    @Test
    fun onboardUserWithNameOnly() {
        // Given
        mockServerPreconditions.run { givenRegisterUser() }

        // When
        activityScenarioRule.launch()
        registrationPage.run {
            enterRegistrationNames(registrationData)
            finishButton.scrollToAndPerform { click() }
        }

        // Then
        registrationPage.assertSuccess()
        mockServerPreconditions.assertNot(MockServerPreconditions.Route.RegisterUser)
    }

    @Test
    fun smsVerificationFailedRateLimit() {
        // Given
        RegistrationPreconditions().givenRegisteredUser(person)
        mockServerPreconditions.run {
            givenSmsRequest(responseCode = 429)
            givenUpdateUser(person.registrationData().id!!.toString())
        }

        // When
        activityScenarioRule.launch()
        registrationPage.run {
            enterRegistrationNames(registrationData)
            enterContactData(registrationData)
            updateButton.scrollToAndPerform { click() }
            SmsVerificationExplanationDialog().okButton.click()
        }

        // Then
        RequestErrorDialog(R.string.verification_rate_limit_title).isDisplayed()
    }

    @Test
    fun smsVerificationFailedBadGateway() {
        // Given
        RegistrationPreconditions().givenRegisteredUser(person)
        mockServerPreconditions.run {
            givenSmsRequest(responseCode = HTTP_BAD_GATEWAY)
            givenUpdateUser(person.registrationData().id!!.toString())
        }

        // When
        activityScenarioRule.launch()
        registrationPage.run {
            enterRegistrationNames(registrationData)
            enterContactData(registrationData)
            updateButton.scrollToAndPerform { click() }
            SmsVerificationExplanationDialog().okButton.click()
        }

        // Then
        RequestErrorDialog(R.string.error_http_server_error_title).isDisplayed()
    }

    @Test
    fun smsVerificationFailedBadRequest() {
        // Given
        RegistrationPreconditions().givenRegisteredUser(person)
        mockServerPreconditions.run {
            givenSmsRequest(responseCode = HTTP_BAD_REQUEST)
            givenUpdateUser(person.registrationData().id!!.toString())
        }

        // When
        activityScenarioRule.launch()
        registrationPage.run {
            enterRegistrationNames(registrationData)
            enterContactData(registrationData)
            updateButton.scrollToAndPerform { click() }
            SmsVerificationExplanationDialog().okButton.click()
        }

        // Then
        RequestErrorDialog().isDisplayed()
    }

    @Test
    fun editUserData() {
        // Given
        RegistrationPreconditions().givenRegisteredUser(person)
        mockServerPreconditions.run {
            givenSmsRequest()
            givenSmsBulkVerification()
            givenUpdateUser(person.registrationData().id!!.toString())
        }
        application.preferencesManager.persist(REGISTRATION_COMPLETED_SCREEN_SEEN_KEY, true).blockingAwait()

        // When
        activityScenarioRule.launch()
        registrationPage.run {
            editUserData(editedUserData)
            performSmsTanVerification()
        }

        // Then
        waitForIdle()
        val savedRegistrationData = application.registrationManager.getRegistrationData().blockingGet()
        assertEquals(editedUserData, savedRegistrationData)
    }

    @Test
    @Ignore("No actual network request made at the moment, therefore no error can happen")
    fun editUserDataFailure() {
        // Given
        RegistrationPreconditions().givenRegisteredUser(person)
        mockServerPreconditions.run {
            givenSmsRequest()
            givenSmsBulkVerification()
            givenUpdateUser(person.registrationData().id!!.toString(), HTTP_BAD_REQUEST)
        }
        application.preferencesManager.persist(REGISTRATION_COMPLETED_SCREEN_SEEN_KEY, true).blockingAwait()

        // When
        activityScenarioRule.launch()
        registrationPage.run {
            editUserData(editedUserData)
            performSmsTanVerification()
        }

        // Then
        RequestErrorDialog().isDisplayed()
    }
}
