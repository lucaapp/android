package de.culture4life.luca.testtools.pages.dialogs.base

abstract class DefaultOkCancelDialog : DefaultOkDialog() {

    val cancelButton = baseDialog.negativeButton
}
