package de.culture4life.luca.testtools.pages

import de.culture4life.luca.R
import de.culture4life.luca.testtools.pages.dialogs.base.SafeDialogInteraction
import io.github.kakaocup.kakao.check.KCheckBox
import io.github.kakaocup.kakao.edit.KEditText
import io.github.kakaocup.kakao.screen.Screen
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView

class PaymentRafflePage(isEmbeddedInDialog: Boolean = true) : Screen<PaymentRafflePage>() {

    val title = KTextView {
        withId(R.id.titleTextView)
        withText(R.string.pay_raffle_consent_title)
    }

    val emailInput = KEditText { withId(R.id.emailEditText) }

    val checkbox = KCheckBox { withId(R.id.consentCheckBox) }

    val primaryActionButton = KButton { withId(R.id.primaryActionButton) }

    init {
        SafeDialogInteraction.apply(this, isEmbeddedInDialog)
    }

    fun isDisplayed() {
        title.isDisplayed()
    }
}
