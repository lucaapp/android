package de.culture4life.luca.testtools.pages

import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.testing.FragmentScenario
import de.culture4life.luca.R
import de.culture4life.luca.network.pojo.payment.ConsumerPaymentsResponseData
import de.culture4life.luca.payment.PaymentAmounts
import de.culture4life.luca.payment.PaymentData
import de.culture4life.luca.testtools.LucaFragmentTest
import de.culture4life.luca.testtools.actions.scrollToAndPerform
import de.culture4life.luca.testtools.mocks.IntentMocks
import de.culture4life.luca.testtools.pages.dialogs.QrCodeScannerBottomSheet
import de.culture4life.luca.testtools.samples.PaymentHistorySamples
import de.culture4life.luca.testtools.samples.PaymentSamples
import de.culture4life.luca.ui.payment.history.PaymentHistoryFragment
import de.culture4life.luca.util.toCurrencyAmountString
import io.github.kakaocup.kakao.common.views.KView
import io.github.kakaocup.kakao.image.KImageView
import io.github.kakaocup.kakao.recycler.KRecyclerItem
import io.github.kakaocup.kakao.recycler.KRecyclerView
import io.github.kakaocup.kakao.swiperefresh.KSwipeRefreshLayout
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView
import org.hamcrest.Matcher
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.empty
import org.hamcrest.Matchers.`is`

class PaymentHistoryPage {
    val paymentsList = KRecyclerView(
        { withId(R.id.paymentsList) },
        {
            itemType(PaymentHistoryPage::LoadingItem)
            itemType(PaymentHistoryPage::PaymentItem)
        }
    )
    val refreshView = KSwipeRefreshLayout { withId(R.id.refreshLayout) }
    val emptyIcon = KView { withId(R.id.emptyIconImageView) }
    val newPaymentButton = KButton { withId(R.id.newPaymentButton) }
    val rootLayout = KView { withId(R.id.paymentHistoryRootLayout) }
    val qrCodeScanner = QrCodeScannerBottomSheet()
    val paymentFlow = PaymentFlow()

    fun isDisplayed(scenario: FragmentScenario<PaymentHistoryFragment>) {
        scenario.onFragment {
            // Check that no dialogs are overlaying the page
            assertThat(it.childFragmentManager.fragments.filterIsInstance<DialogFragment>(), `is`(empty()))
        }
        isDisplayed()
    }

    fun isDisplayed() {
        rootLayout.isCompletelyDisplayed()
    }

    class PaymentItem(parent: Matcher<View>) : KRecyclerItem<Any>(parent) {
        val name = KTextView(parent) { withId(R.id.titleTextView) }
        val amount = KTextView(parent) { withId(R.id.amountTextView) }

        fun isDisplayed(payment: ConsumerPaymentsResponseData.Payment) {
            name.hasText(payment.locationName)
        }
    }

    class LoadingItem(parent: Matcher<View>) : KRecyclerItem<Any>(parent) {
        val loadingIndicator = KView(parent) { withId(R.id.loading_indicator) }
        val retryButton = KImageView(parent) { withId(R.id.retryButton) }
    }

    class PaymentFlow {

        val cancelButton = KButton { withId(R.id.cancelButton) }

        val amountPage = PaymentAmountPage()
        val tipPage = PaymentTipPage()
        val webAppPage = PaymentWepAppPage()
        val resultPage = PaymentResultPage()

        data class PaymentFlowScenario(
            val testBase: LucaFragmentTest<*>,
            val payment: PaymentSamples.Payment,
            val addHistory: PaymentHistorySamples = PaymentHistorySamples.Single,
            val initiator: INITIATOR = INITIATOR.CUSTOMER
        ) {

            val initialAmount: Int = if (initiator == INITIATOR.CUSTOMER) 0 else payment.invoiceAmount
            val enterAmount: Int? = if (initiator == INITIATOR.CUSTOMER) payment.invoiceAmount else null
            val initialTipPercentage: Int = 10
            val selectTipPercentage: Int = payment.tipPercentage
            val invoiceAmount: Int = if (initiator == INITIATOR.CUSTOMER && enterAmount != null) enterAmount else initialAmount

            enum class INITIATOR {
                CUSTOMER,
                OPERATOR
            }
        }

        fun runPaymentFlow(scenario: PaymentFlowScenario) {
            runPaymentFlowUntilResult(scenario)

            resultPage.run {
                successLayout.isDisplayed()
                isDisplayingPayment(PaymentData.fromPayment(scenario.payment.checkoutPaymentResponseData(), scenario.payment.locationId))
                nextButton.scrollToAndPerform { click() }
            }
        }

        fun runPaymentFlowUntilResult(scenario: PaymentFlowScenario) {
            amountPage.run {
                isDisplayed()
                amountTextView.hasText(scenario.initialAmount.toCurrencyAmountString())
                if (scenario.initiator == PaymentFlowScenario.INITIATOR.CUSTOMER && scenario.enterAmount != null) {
                    amountEditText.replaceText("${scenario.enterAmount}")
                    amountTextView.hasText(scenario.enterAmount.toCurrencyAmountString())
                    amountEditText.pressImeAction()
                } else {
                    continueButton.scrollToAndPerform { click() }
                }
            }

            tipPage.run {
                isDisplayed()
                val expectedInitialAmount = PaymentAmounts(
                    invoiceAmount = scenario.invoiceAmount,
                    tipPercentage = scenario.initialTipPercentage
                ).totalAmount
                totalAmount.hasText("${expectedInitialAmount.toCurrencyAmountString()} €")
                selectPercentage(scenario.selectTipPercentage)
                val expectedSelectedAmount = PaymentAmounts(
                    invoiceAmount = scenario.invoiceAmount,
                    tipPercentage = scenario.selectTipPercentage
                ).totalAmount
                totalAmount.hasText("${expectedSelectedAmount.toCurrencyAmountString()} €")
            }

            IntentMocks.givenWebAppResponse(scenario.payment.checkoutUrl, scenario = scenario.testBase.fragmentScenarioRule) {
                scenario.testBase.mockServerPreconditions.run {
                    givenPaymentCheckout(scenario.payment)
                    givenPaymentHistory(scenario.addHistory)
                }
            }
            tipPage.primaryActionButton.click()
        }
    }
}
