package de.culture4life.luca.testtools.mocks

import android.app.Activity
import android.app.Instrumentation
import android.content.Intent
import android.net.Uri
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.VerificationModes.times
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.espresso.intent.matcher.UriMatchers
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.testtools.rules.LucaFragmentScenarioRule
import org.hamcrest.Matchers

object IntentMocks {

    /**
     * Avoid the "No Activity found to handle Intent" for "market" scheme (aka PlayStore).
     */
    fun givenMarketIntentResponse(resultCode: Int = 12345, resultData: Intent = Intent()) {
        Intents.intending(
            Matchers.allOf(
                IntentMatchers.hasAction(Intent.ACTION_VIEW),
                IntentMatchers.hasData(UriMatchers.hasHost("play.google.com"))
            )
        ).respondWith(Instrumentation.ActivityResult(resultCode, resultData))
    }

    fun assertShareFileChooserIntent(type: String = "text/*") {
        Intents.intended(
            Matchers.allOf(
                IntentMatchers.hasAction(Intent.ACTION_CHOOSER),
                IntentMatchers.hasExtra(
                    Intent.EXTRA_INTENT,
                    Matchers.allOf(
                        IntentMatchers.hasAction(Intent.ACTION_SEND),
                        IntentMatchers.hasExtra(Intent.EXTRA_STREAM, UriMatchers.hasScheme("content")),
                        IntentMatchers.hasType(type)
                    )
                )
            )
        )
    }

    inline fun <reified T : Activity> givenActivityNavigationResponseOk() {
        Intents.intending(IntentMatchers.hasComponent(T::class.java.name))
            .respondWith(Instrumentation.ActivityResult(Activity.RESULT_OK, Intent()))
    }

    inline fun <reified T : Activity> assertActivityNavigation() {
        Intents.intended(IntentMatchers.hasComponent(T::class.java.name))
    }

    fun givenWebAppResponse(
        url: String,
        resultCode: Int = Activity.RESULT_OK,
        resultData: Intent = Intent(),
        scenario: LucaFragmentScenarioRule<*>? = null,
        onResult: () -> Unit = {},
    ) {
        Intents.intending(
            Matchers.allOf(
                IntentMatchers.hasAction(Intent.ACTION_VIEW),
                IntentMatchers.hasData(Uri.parse(url))
            )
        ).respondWithFunction {
            onResult.invoke()
            if (LucaApplication.isRunningUnitTests()) {
                // simulate WebApp finish as intents don't change the state of the fragment when run with robolectric
                scenario?.leaveAndReturnToApp()
            }
            Instrumentation.ActivityResult(resultCode, resultData)
        }
    }

    fun assertWebAppIntent(url: String, times: Int = 1) {
        Intents.intended(
            Matchers.allOf(
                IntentMatchers.hasAction(Intent.ACTION_VIEW),
                IntentMatchers.hasData(Uri.parse(url))
            ),
            times(times)
        )
    }
}
