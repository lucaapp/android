package de.culture4life.luca.testtools.samples

import android.net.Uri
import de.culture4life.luca.network.pojo.LocationResponseData
import de.culture4life.luca.network.pojo.LocationUrlsResponseData
import de.culture4life.luca.payment.PaymentLocationData
import java.util.*

interface LocationSamples {

    val scannerId: String
    val locationId: String
    val table: String?
    val qrCodeContent: String
    val deeplink: Uri
    val isPaymentActive: Boolean
    val locationResponseData: LocationResponseData
    val urlsResponseData: LocationUrlsResponseData

    val locationData: PaymentLocationData
        get() {
            return PaymentLocationData(locationId, locationResponseData.locationDisplayName ?: "", table, isPaymentActive)
        }

    open class Restaurant : LocationSamples {
        final override val scannerId = UUID.randomUUID().toString()
        final override val locationId = UUID.randomUUID().toString()
        override val table: String? = null
        override val qrCodeContent
            get() = UrlSamples.location(scannerId)
        override val deeplink: Uri
            get() = Uri.parse(qrCodeContent)
        override val isPaymentActive = true
        val qrCodeLucaData = "e30"
        val qrCodeContentWithLucaData
            get() = "$qrCodeContent#$qrCodeLucaData"
        val qrCodeContentWithLucaDataAndCWA =
            "$qrCodeContentWithLucaData/CWA1/CAESLAgBEhNEb2xvcmVzIGN1bHBhIHV0IHNpGhNOb3N0cnVkIE5hbSBpZCBlbGlnGnYIARJggwLMzE153tQwAOf2MZoUXXfzWTdlSpfS99iZffmcmxOG9njSK4RTimFOFwDh6t0Tyw8XR01ugDYjtuKwjjuK49Oh83FWct6XpefPi9Skjxvvz53i9gaMmUEc96pbtoaAGhDL1rYQOi3Bh_YYps7XagWYIgcIARAIGIQF"
        val qrCodeContentWithEmptyCWA = "${UrlSamples.webApp}CWA1/#"

        override var locationResponseData = LocationResponseData(
            locationId = locationId,
            groupName = "Restaurant Light Blue"
        ).also { it.isPaymentActive = isPaymentActive }

        private val menuUrl = "https://thingsforrestaurants.com/qr_menu/example-menu/"
        override val urlsResponseData = LocationUrlsResponseData(
            menu = menuUrl
        )
    }

    class RestaurantWithTable : Restaurant() {
        override val table = "11"
        override val qrCodeContent = "${UrlSamples.location(scannerId)}#eyJ0YWJsZUlkIjoiMTEifQ==" // {"tableId":"11"}
    }

    class RestaurantWithoutTables : Restaurant() {
        override val table: String? = null
    }

    class RestaurantWithoutLucaPay : Restaurant() {
        override val isPaymentActive = false
    }

    class Meeting : LocationSamples {
        override val scannerId = UUID.randomUUID().toString()
        override val locationId = UUID.randomUUID().toString()
        override val table: String? = null
        override val qrCodeContent = UrlSamples.meeting(scannerId)
        override val deeplink: Uri = Uri.parse(qrCodeContent)
        override val isPaymentActive = false
        override val urlsResponseData = LocationUrlsResponseData()
        override val locationResponseData = LocationResponseData(locationId)
    }
}
