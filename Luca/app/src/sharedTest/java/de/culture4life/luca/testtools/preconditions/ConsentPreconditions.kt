package de.culture4life.luca.testtools.preconditions

import androidx.test.core.app.ApplicationProvider
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.consent.ConsentManager

class ConsentPreconditions {

    private val application = ApplicationProvider.getApplicationContext<LucaApplication>()
    private val consentManager by lazy { application.getInitializedManager(application.consentManager).blockingGet() }

    fun givenConsent(consent: String, isGiven: Boolean) {
        with(consentManager) {
            getConsent(consent)
                .flatMapCompletable { persistConsent(it.copy(approved = isGiven)) }
                .blockingAwait()
        }
    }

    fun givenConsentLucaPay(isGiven: Boolean) {
        for (id in ConsentManager.lucaPayConsentBundle) {
            givenConsent(id, isGiven)
        }
    }
}
