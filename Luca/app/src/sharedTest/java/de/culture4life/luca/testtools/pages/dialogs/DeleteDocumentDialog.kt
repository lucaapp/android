package de.culture4life.luca.testtools.pages.dialogs

import de.culture4life.luca.R
import de.culture4life.luca.testtools.pages.dialogs.base.DefaultOkContinueDialog
import de.culture4life.luca.testtools.pages.dialogs.base.SafeDialogInteraction

class DeleteDocumentDialog : DefaultOkContinueDialog() {

    init {
        SafeDialogInteraction.apply(this)
    }

    override fun isDisplayed() {
        baseDialog.message.hasText(R.string.document_delete_confirmation_message)
    }
}
