package de.culture4life.luca.testtools.samples

import de.culture4life.luca.network.pojo.payment.ConsumerPaymentsResponseData
import de.culture4life.luca.util.TimeUtil
import de.culture4life.luca.util.toUnixTimestamp
import java.math.BigDecimal
import java.util.*
import kotlin.random.Random

interface PaymentHistorySamples {

    fun getNextPage(cursor: String? = null): ConsumerPaymentsResponseData

    object Empty : PaymentHistorySamples {

        override fun getNextPage(cursor: String?): ConsumerPaymentsResponseData {
            return ConsumerPaymentsResponseData(
                cursor = null,
                payments = listOf()
            )
        }
    }

    object Single : PaymentHistorySamples {

        override fun getNextPage(cursor: String?): ConsumerPaymentsResponseData {
            return ConsumerPaymentsResponseData(
                cursor = null,
                payments = generatePaymentList(1)
            )
        }
    }

    data class Paged(val pageSize: Int = 20, val pages: Int = 2) : PaymentHistorySamples {

        val cursors = (0 until pages - 1).map { UUID.randomUUID().toString() }

        override fun getNextPage(cursor: String?): ConsumerPaymentsResponseData {
            val nextCursor = when {
                cursor == null -> cursors.first()
                cursors.indexOf(cursor) == cursors.lastIndex -> null
                else -> cursors[cursors.indexOf(cursor) + 1]
            }

            return ConsumerPaymentsResponseData(
                cursor = nextCursor,
                payments = generatePaymentList(pageSize)
            )
        }
    }

    data class PreDefined(val payments: List<ConsumerPaymentsResponseData.Payment>) : PaymentHistorySamples {

        override fun getNextPage(cursor: String?): ConsumerPaymentsResponseData {
            return ConsumerPaymentsResponseData(
                cursor = null,
                payments = payments
            )
        }
    }

    companion object {
        fun generatePaymentList(size: Int) = (0 until size).map {
            ConsumerPaymentsResponseData.Payment(
                id = UUID.randomUUID().toString(),
                locationName = Random.nextInt().toString(),
                locationId = UUID.randomUUID().toString(),
                totalAmount = BigDecimal("50.0"),
                invoiceAmount = BigDecimal("40.0"),
                discountAmounts = null,
                tipAmount = BigDecimal("10.0"),
                timestamp = TimeUtil.getCurrentMillis().toUnixTimestamp(),
                table = "23",
                status = ConsumerPaymentsResponseData.Payment.Status.CLOSED,
                refunded = false,
                refundedAmount = BigDecimal(0.0),
                paymentVerifier = "verifier"
            )
        }
    }
}
