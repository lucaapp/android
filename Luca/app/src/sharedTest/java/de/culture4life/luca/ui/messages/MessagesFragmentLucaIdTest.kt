package de.culture4life.luca.ui.messages

import de.culture4life.luca.R
import de.culture4life.luca.consent.ConsentManager
import de.culture4life.luca.testtools.LucaFragmentTest
import de.culture4life.luca.testtools.pages.MessagesPage
import de.culture4life.luca.testtools.preconditions.ConsentPreconditions
import de.culture4life.luca.testtools.preconditions.MockServerPreconditions
import de.culture4life.luca.testtools.rules.LucaFragmentScenarioRule
import de.culture4life.luca.util.isHttpException
import org.junit.Before
import org.junit.Test

class MessagesFragmentLucaIdTest : LucaFragmentTest<MessagesFragment>(LucaFragmentScenarioRule.create()) {

    private val anyHttpError = 400

    @Before
    fun setup() {
        // hide messages not relevant for this test class
        ConsentPreconditions().givenConsent(ConsentManager.ID_TERMS_OF_SERVICE_LUCA_ID, true)

        with(mockServerPreconditions) {
            givenTimeSync()
            givenPowChallenge()
            givenLucaIdCreateEnrollment()
            givenLucaIdEnrollmentStatusQueued()
            givenLucaIdDelete()
            givenLucaIdDeleteIdent()
            givenAttestationNonce()
            givenAttestationRegister()
            givenAttestationAssert()
        }
    }

    @Test
    fun showEnrollmentTokenMessage() {
        launchScenario(R.id.messagesFragment)

        MessagesPage().run {
            messageList.run {
                hasSize(0)
                mockServerPreconditions.givenLucaIdEnrollmentStatusPending()
                whenEnrolled()
                waitFor(MessagesViewModel.WHAT_IS_NEW_MESSAGE_UPDATE_DELAY)
                hasSize(1)
                childAt<MessagesPage.MessageItem>(0) {
                    assertIsEnrollmentTokenMessage()
                    click()
                }
            }
        }
        assertNavigationDestination(R.id.lucaIdEnrollmentTokenFragment)
    }

    @Test
    fun showEnrollErrorMessage_fromEnrollError() {
        launchScenario(R.id.messagesFragment)

        // show message when failed
        MessagesPage().run {
            messageList.run {
                hasSize(0)
                whenEnrolled(anyHttpError)
                hasSize(1)
                childAt<MessagesPage.MessageItem>(0) {
                    assertIsEnrollmentErrorMessage()
                    click()
                }
            }
        }
        assertNavigationDestination(R.id.lucaIdEnrollmentErrorFragment)

        // hide message after success
        MessagesPage().run {
            messageList.run {
                mockServerPreconditions.givenLucaIdCreateEnrollment()
                mockServerPreconditions.givenLucaIdEnrollmentStatusPending()
                whenEnrolled()
                hasSize(1)
                childAt<MessagesPage.MessageItem>(0) {
                    assertIsEnrollmentTokenMessage()
                }
            }
        }
    }

    @Test
    fun showEnrollErrorMessage_fromStatusUpdate() {
        launchScenario(R.id.messagesFragment)

        // show message when failed
        MessagesPage().run {
            messageList.run {
                hasSize(0)
                mockServerPreconditions.givenLucaIdEnrollmentStatusFailed()
                whenEnrolled()
                hasSize(1)
                childAt<MessagesPage.MessageItem>(0) {
                    assertIsEnrollmentErrorMessage()
                    click()
                }
            }
        }
        assertNavigationDestination(R.id.lucaIdEnrollmentErrorFragment)
    }

    @Test
    fun resetEnrollmentMessagesAfterDelete() {
        launchScenario(R.id.messagesFragment)

        MessagesPage().messageList.hasSize(0)

        mockServerPreconditions.givenLucaIdEnrollmentStatusSuccess()
        whenEnrolled()
        MessagesPage().messageList.run {
            hasSize(1)
            childAt<MessagesPage.MessageItem>(0) { assertIsLucaIdVerificationSuccessMessage() }
        }

        whenDeleted()
        MessagesPage().messageList.hasSize(0)
    }

    @Test
    fun showVerificationSuccessMessage() {
        mockServerPreconditions.givenLucaIdEnrollmentStatusPending()
        launchScenario(R.id.messagesFragment)

        MessagesPage().run {
            messageList.run {
                hasSize(0)
                whenEnrolled()
                childAt<MessagesPage.MessageItem>(0) { assertIsEnrollmentTokenMessage() }

                mockServerPreconditions.givenLucaIdEnrollmentStatusSuccess()
                whenVerifyVerificationStatus()
                waitFor(MessagesViewModel.WHAT_IS_NEW_MESSAGE_UPDATE_DELAY)
                hasSize(2)
                childAt<MessagesPage.MessageItem>(1) { assertIsEnrollmentTokenMessage() }
                childAt<MessagesPage.MessageItem>(0) {
                    assertIsLucaIdVerificationSuccessMessage()
                    click()
                }
            }
        }
        assertNavigationDestination(R.id.lucaIdVerificationFragment)
    }

    private fun whenVerifyVerificationStatus() {
        application.idNowManager.updateEnrollmentStatus().blockingAwait()
    }

    private fun whenEnrolled(expectError: Int? = null) {
        var enroll = getInitializedManager(application.idNowManager)
            .initiateEnrollment()

        if (expectError != null) {
            mockServerPreconditions.givenHttpError(MockServerPreconditions.Route.LucaIdCreateEnrollment, expectError)
            enroll = enroll.onErrorComplete { it.isHttpException(expectError) }
        }

        enroll.blockingAwait()
        waitFor(MessagesViewModel.WHAT_IS_NEW_MESSAGE_UPDATE_DELAY)
    }

    private fun whenDeleted() {
        application.idNowManager.unEnroll().blockingAwait()
        waitFor(MessagesViewModel.WHAT_IS_NEW_MESSAGE_UPDATE_DELAY)
    }
}
