package de.culture4life.luca.testtools.pages

import androidx.test.core.app.ApplicationProvider
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.R
import de.culture4life.luca.payment.PaymentAmounts
import de.culture4life.luca.testtools.pages.dialogs.base.SafeDialogInteraction
import de.culture4life.luca.util.TextUtil
import de.culture4life.luca.util.toCurrencyAmountString
import io.github.kakaocup.kakao.common.views.KView
import io.github.kakaocup.kakao.edit.KEditText
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView

class PaymentAmountPage(isEmbeddedInDialog: Boolean = true) {

    private val application = ApplicationProvider.getApplicationContext<LucaApplication>()
    private val rootLayoutId = R.id.payAmountRoot
    private val rootView = KView { withId(rootLayoutId) }

    val titleTextView = KTextView {
        withId(R.id.titleTextView)
        isDescendantOfA { withId(rootLayoutId) }
    }
    val descriptionTextView = KTextView {
        withId(R.id.descriptionTextView)
        isDescendantOfA { withId(rootLayoutId) }
    }
    val amountTextView = KTextView {
        withId(R.id.amountTextView)
        isDescendantOfA { withId(rootLayoutId) }
    }
    val amountEditText = KEditText {
        withId(R.id.amountEditText)
        isDescendantOfA { withId(rootLayoutId) }
    }
    val originAmountTextView = KTextView {
        withId(R.id.originAmountTextView)
        isDescendantOfA { withId(rootLayoutId) }
    }
    val campaignInfoImageView = KTextView {
        withId(R.id.campaignInfoImageView)
        isDescendantOfA { withId(rootLayoutId) }
    }
    val campaignInfoTextView = KTextView {
        withId(R.id.campaignInfoTextView)
        isDescendantOfA { withId(rootLayoutId) }
    }
    val errorTextView = KTextView {
        withId(R.id.errorTextView)
        isDescendantOfA { withId(rootLayoutId) }
    }
    val continueButton = KButton {
        withId(R.id.continueWithAmountButton)
        isDescendantOfA { withId(rootLayoutId) }
    }
    val partialContinueButton = KButton {
        withId(R.id.continueWithPartialAmountButton)
        isDescendantOfA { withId(rootLayoutId) }
    }

    init {
        SafeDialogInteraction.apply(this, isEmbeddedInDialog)
    }

    fun isDisplayed() {
        rootView.isDisplayed()
    }

    fun assertAmountDisplayed(amount: Int) {
        val invoiceAmountString = amount.toCurrencyAmountString()
        val continueAmountString = TextUtil.getPlaceholderString(
            application,
            R.string.pay_continue_button,
            "amount" to invoiceAmountString
        )

        amountTextView.hasText(invoiceAmountString)
        amountTextView.hasTextColor(R.color.white)
        continueButton.hasText(continueAmountString)

        originAmountTextView.isGone()
        campaignInfoImageView.isGone()
        campaignInfoTextView.isGone()
    }

    fun assertAmountDisplayedWithDiscount(payment: PaymentAmounts) {
        val invoiceAmountString = application.getString(R.string.euro_amount, payment.invoiceAmount.toCurrencyAmountString())
        val remainingInvoiceAmountString = payment.remainingInvoiceAmount.toCurrencyAmountString()
        val campaignInfoString = TextUtil.getPlaceholderString(
            application,
            R.string.pay_details_discount_hint,
            "discountPercentage" to "${payment.discountPercentage}",
            "maximumDiscountAmount" to payment.maximumDiscountAmount.toCurrencyAmountString()
        )
        val continueAmountString = TextUtil.getPlaceholderString(
            application,
            R.string.pay_continue_button,
            "amount" to remainingInvoiceAmountString
        )

        amountTextView.hasText(remainingInvoiceAmountString)
        amountTextView.hasTextColor(R.color.payment_promotion)
        continueButton.hasText(continueAmountString)

        originAmountTextView.isVisible()
        originAmountTextView.hasText(invoiceAmountString)

        campaignInfoImageView.isVisible()
        campaignInfoTextView.isVisible()
        campaignInfoTextView.hasText(campaignInfoString)
    }
}
