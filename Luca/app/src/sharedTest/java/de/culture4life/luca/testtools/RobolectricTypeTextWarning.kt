package de.culture4life.luca.testtools

import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.TypeTextAction
import de.culture4life.luca.LucaApplication

object RobolectricTypeTextWarning {

    fun apply(interaction: ViewAction) {
        if (!LucaApplication.isRunningUnitTests()) {
            return
        }

        if (interaction is TypeTextAction) {
            throw UnsupportedOperationException("use replaceText instead, see https://github.com/robolectric/robolectric/issues/5110")
        }
    }
}
