package de.culture4life.luca.testtools.pages.dialogs

import de.culture4life.luca.R
import de.culture4life.luca.testtools.pages.dialogs.base.DefaultOkDialog
import de.culture4life.luca.testtools.pages.dialogs.base.SafeDialogInteraction

class ContactInfoMissingDialog : DefaultOkDialog() {

    init {
        SafeDialogInteraction.apply(this)
    }

    override fun isDisplayed() {
        baseDialog.title.hasText(R.string.registration_missing_info)
        baseDialog.message.hasText(R.string.registration_address_mandatory)
        okButton.hasText(R.string.action_change)
    }
}
