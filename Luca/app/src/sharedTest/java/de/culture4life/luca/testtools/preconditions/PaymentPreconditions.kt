package de.culture4life.luca.testtools.preconditions

import androidx.test.core.app.ApplicationProvider
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.testtools.FixRobolectricIdlingResource

class PaymentPreconditions {

    private val application = ApplicationProvider.getApplicationContext<LucaApplication>()
    private val paymentManager by lazy { application.getInitializedManager(application.paymentManager).blockingGet() }

    fun givenSignedUpAndSignedIn() {
        paymentManager.signUp().blockingAwait()
    }

    fun givenStoredDefaultTipChoice(percentage: Int) {
        paymentManager.persistLastTipPercentage(percentage).blockingAwait()
    }

    fun assertStoredDefaultTipChoice(percentage: Int) {
        FixRobolectricIdlingResource.waitForIdle()
        paymentManager.restoreDefaultTipPercentage()
            .test()
            .assertValue(percentage)
    }

    fun givenStoredLastCheckoutId(id: String) {
        paymentManager.persistLastCheckoutId(id).blockingAwait()
    }
}
