package de.culture4life.luca.testtools.pages

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import de.culture4life.luca.LucaApplication
import de.culture4life.luca.R
import de.culture4life.luca.payment.PaymentData
import de.culture4life.luca.util.NumberUtil
import de.culture4life.luca.util.getReadableDate
import de.culture4life.luca.util.getReadableTime
import io.github.kakaocup.kakao.text.KTextView

class PaymentHistoryDetailPage {

    private val context
        get() = ApplicationProvider.getApplicationContext<LucaApplication>()

    private val titleTextView = KTextView { withId(R.id.titleTextView) }
    private val dateValueTextView = KTextView { withId(R.id.dateValueTextView) }
    private val timeValueTextView = KTextView { withId(R.id.timeValueTextView) }
    private val tableTextView = KTextView { withId(R.id.tableTextView) }
    private val tableValueTextView = KTextView { withId(R.id.tableValueTextView) }
    private val amountValueTextView = KTextView { withId(R.id.amountValueTextView) }
    private val invoiceAmountValueTextView = KTextView { withId(R.id.invoiceAmountValueTextView) }
    private val originInvoiceAmountValueTextView = KTextView { withId(R.id.originInvoiceAmountValueTextView) }
    private val tipAmountValueTextView = KTextView { withId(R.id.tipAmountValueTextView) }
    private val paymentCodeValueTextView = KTextView { withId(R.id.paymentCodeValueTextView) }

    fun assertPaymentDisplayed(payment: PaymentData) {
        assertIsDisplayedWithText(titleTextView, payment.locationName)
        assertIsDisplayedWithText(dateValueTextView, context.getReadableDate(payment.timestamp))
        assertIsDisplayedWithText(
            timeValueTextView,
            context.getString(R.string.history_time, context.getReadableTime(payment.timestamp))
        )
        assertIsDisplayedWithText(amountValueTextView, getAmountInEuro(context, payment.totalAmount))
        if (payment.isDiscounted) {
            assertIsDisplayedWithText(originInvoiceAmountValueTextView, getAmountInEuro(context, payment.originalInvoiceAmount))
        } else {
            originInvoiceAmountValueTextView.isGone()
        }
        assertIsDisplayedWithText(invoiceAmountValueTextView, getAmountInEuro(context, payment.invoiceAmount))
        assertIsDisplayedWithText(
            tipAmountValueTextView,
            context.getString(
                R.string.pay_details_tip_amount_value,
                payment.tipPercent,
                NumberUtil.toCurrencyAmountString(payment.tipAmount)
            )
        )
        assertIsDisplayedWithText(paymentCodeValueTextView, payment.paymentVerifier)
    }

    fun assertTableDisplayed(tableName: String) {
        tableTextView.isDisplayed()
        assertIsDisplayedWithText(tableValueTextView, tableName)
    }

    fun assertTableNotDisplayed() {
        tableTextView.isNotDisplayed()
        tableValueTextView.isNotDisplayed()
    }

    private fun assertIsDisplayedWithText(view: KTextView, text: String) {
        view.isDisplayed()
        view.hasText(text)
    }

    private fun getAmountInEuro(context: Context, amount: Int) = context.getString(R.string.euro_amount, NumberUtil.toCurrencyAmountString(amount))
}
