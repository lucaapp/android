package de.culture4life.luca.testtools.pages.dialogs

import de.culture4life.luca.R
import de.culture4life.luca.testtools.pages.dialogs.base.DefaultOkCancelDialog
import de.culture4life.luca.testtools.pages.dialogs.base.SafeDialogInteraction

class UserAuthenticationRequiredLucaIdEnrollmentDialog : DefaultOkCancelDialog() {

    init {
        SafeDialogInteraction.apply(this)
    }

    override fun isDisplayed() {
        baseDialog.message.containsText(context.getString(R.string.authentication_title))
        baseDialog.message.containsText(context.getString(R.string.authentication_luca_id_enrollment_subtitle))
        baseDialog.message.containsText(context.getString(R.string.authentication_luca_id_enrollment_description))
    }
}
