package de.culture4life.luca.ui.messages

import de.culture4life.luca.R
import de.culture4life.luca.consent.ConsentManager
import de.culture4life.luca.testtools.LucaFragmentTest
import de.culture4life.luca.testtools.pages.MessagesPage
import de.culture4life.luca.testtools.preconditions.ConsentPreconditions
import de.culture4life.luca.testtools.preconditions.RegistrationPreconditions
import de.culture4life.luca.testtools.preconditions.WhatIsNewPreconditions
import de.culture4life.luca.testtools.rules.LucaFragmentScenarioRule
import de.culture4life.luca.whatisnew.WhatIsNewManager
import org.junit.Before
import org.junit.Test

class MessagesFragmentPayTest : LucaFragmentTest<MessagesFragment>(LucaFragmentScenarioRule.create()) {

    private val registrationPreconditions = RegistrationPreconditions()
    private val consentPreconditions = ConsentPreconditions()

    @Before
    fun before() {
        registrationPreconditions.givenRegisteredUser()

        // hide messages not relevant for this test class
        consentPreconditions.givenConsent(ConsentManager.ID_TERMS_OF_SERVICE_LUCA_ID, true)
        WhatIsNewPreconditions().givenMessageEnabled(WhatIsNewManager.ID_POSTAL_CODE_MESSAGE, false)
    }

    private fun activatePayment() {
        mockServerPreconditions.run {
            givenPaymentSignIn()
            givenPaymentSignUp()
        }
        consentPreconditions.givenConsentLucaPay(true)
        getInitializedManager(application.paymentManager).signUp().blockingAwait()
    }

    private fun revokePayment() {
        mockServerPreconditions.run {
            givenPaymentConsumerDeletion()
        }
        getInitializedManager(application.paymentManager).deleteConsumer().blockingAwait()
    }

    @Test
    fun revocationMessageShownAfterPaymentActivated() {
        launchScenario(R.id.messagesFragment)
        MessagesPage().run {
            messageList.run {
                hasSize(0)
                activatePayment()
                waitFor(MessagesViewModel.WHAT_IS_NEW_MESSAGE_UPDATE_DELAY)
                hasSize(1)
                childAt<MessagesPage.MessageItem>(0) {
                    assertIsPayRevocationMessage()
                    click()
                }
            }
        }
        assertNavigationDestination(R.id.paymentRevocationCodeFragment)
    }

    @Test
    fun revocationMessageHiddenAfterPaymentRevoked() {
        launchScenario(R.id.messagesFragment)
        MessagesPage().run {
            messageList.run {
                activatePayment()
                waitFor(MessagesViewModel.WHAT_IS_NEW_MESSAGE_UPDATE_DELAY)
                hasSize(1)
                childAt<MessagesPage.MessageItem>(0) { assertIsPayRevocationMessage() }
                revokePayment()
                waitFor(MessagesViewModel.WHAT_IS_NEW_MESSAGE_UPDATE_DELAY)
                hasSize(0)
            }
        }
    }
}
