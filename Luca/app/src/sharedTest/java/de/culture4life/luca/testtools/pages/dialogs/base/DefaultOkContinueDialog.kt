package de.culture4life.luca.testtools.pages.dialogs.base

abstract class DefaultOkContinueDialog : DefaultOkDialog() {

    val continueButton = baseDialog.neutralButton
}
