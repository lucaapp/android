package de.culture4life.luca.ui.payment.children

import de.culture4life.luca.payment.PaymentAmounts
import de.culture4life.luca.testtools.LucaFlowFragmentTest
import de.culture4life.luca.testtools.pages.PaymentTipPage
import de.culture4life.luca.testtools.preconditions.PaymentPreconditions
import de.culture4life.luca.ui.payment.PaymentFlowViewModel
import de.culture4life.luca.util.toCurrencyAmountString
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class PaymentTipFragmentTest : LucaFlowFragmentTest<PaymentTipFragment, PaymentFlowViewModel>(
    PaymentTipFragment::class.java,
    PaymentFlowViewModel::class.java
) {

    private val paymentPreconditions = PaymentPreconditions()
    private val defaultPercentage = 10
    private val page = PaymentTipPage(isEmbeddedInDialog = false)

    @Test
    fun preselectDefaultTipChoice() {
        launchScenario()
        page.isSelectedPercentage(defaultPercentage)
    }

    @Test
    fun rememberTipChoice() {
        val higherPercentage = 20

        assertThat(higherPercentage).isGreaterThan(defaultPercentage)

        fixtureRememberTipChoice(
            initialDefaultPercentage = defaultPercentage,
            choosePercentage = higherPercentage,
            nextDefaultPercentage = higherPercentage
        )
    }

    @Test
    fun rememberTipChoiceLowerThanDefault() {
        val initialPercentage = 20
        val lowerPercentage = 0

        assertThat(initialPercentage).isGreaterThan(defaultPercentage)
        assertThat(lowerPercentage).isLessThan(defaultPercentage)

        fixtureRememberTipChoice(
            initialDefaultPercentage = initialPercentage,
            choosePercentage = lowerPercentage,
            nextDefaultPercentage = defaultPercentage
        )
    }

    @Test
    fun rememberTipChoiceLowerThanLastChoice() {
        val lastPercentage = 20
        val lowerPercentage = 15

        assertThat(lastPercentage).isGreaterThan(defaultPercentage)
        assertThat(lowerPercentage).isGreaterThan(defaultPercentage).isLessThan(lastPercentage)

        fixtureRememberTipChoice(
            initialDefaultPercentage = lastPercentage,
            choosePercentage = lowerPercentage,
            nextDefaultPercentage = lowerPercentage
        )
    }

    @Test
    fun showTipChoiceSelected() {
        launchScenario()

        page.isSelectedPercentage(defaultPercentage)
        page.selectPercentage(defaultPercentage)
        page.isSelectedPercentage(defaultPercentage)

        page.selectPercentage(0)
        page.isSelectedPercentage(0)

        page.selectPercentage(10)
        page.isSelectedPercentage(10)

        page.selectPercentage(15)
        page.isSelectedPercentage(15)

        page.selectPercentage(20)
        page.isSelectedPercentage(20)
    }

    @Test
    fun storeTipForNextSteps() {
        val differentPercentage = 20
        assertThat(differentPercentage).isNotEqualTo(defaultPercentage)

        launchScenario()
        assertThat(sharedViewModel.paymentAmounts.value!!.tipPercentage).isEqualTo(0)

        page.primaryActionButton.click()
        assertThat(sharedViewModel.paymentAmounts.value!!.tipPercentage).isEqualTo(defaultPercentage)

        page.selectPercentage(differentPercentage)
        page.primaryActionButton.click()
        assertThat(sharedViewModel.paymentAmounts.value!!.tipPercentage).isEqualTo(differentPercentage)
    }

    @Test
    fun onRecreate() {
        launchScenario()
        sharedViewModel.paymentAmounts.postValue(PaymentAmounts(100))

        page.selectPercentage(20)
        recreateScenario()
        page.isSelectedPercentage(20)
        page.totalAmount.containsText(120.toCurrencyAmountString())

        page.selectPercentage(0)
        recreateScenario()
        page.isSelectedPercentage(0)
        page.totalAmount.containsText(100.toCurrencyAmountString())
    }

    @Test
    fun calculateAmountWithTip() {
        launchScenario()
        val payment = PaymentAmounts(invoiceAmount = 100, tipPercentage = 10)
        sharedViewModel.paymentAmounts.postValue(payment)

        page.assertTipAmountDisplayed(payment)

        page.selectPercentage(20)
        page.assertTipAmountDisplayed(payment.copy(tipPercentage = 20))
    }

    @Test
    fun discountCampaign() {
        launchScenario()
        val paymentWithoutDiscount = PaymentAmounts(
            invoiceAmount = 100,
            tipPercentage = defaultPercentage
        )
        val paymentWithDiscount = paymentWithoutDiscount.copy(
            discountPercentage = 50,
            maximumDiscountAmount = 10000
        )
        sharedViewModel.paymentAmounts.postValue(paymentWithDiscount)

        page.assertAmountDisplayedWithDiscount(paymentWithDiscount)

        // reset discount campaign (like ended just now)
        sharedViewModel.paymentAmounts.postValue(paymentWithoutDiscount)
        // expect discount ui changes are reverted
        page.assertTipAmountDisplayed(paymentWithoutDiscount)
    }

    private fun fixtureRememberTipChoice(initialDefaultPercentage: Int, choosePercentage: Int, nextDefaultPercentage: Int) {
        // initial tip choice
        paymentPreconditions.givenStoredDefaultTipChoice(initialDefaultPercentage)
        launchScenario()
        page.isSelectedPercentage(initialDefaultPercentage)

        // choose different percentage
        page.selectPercentage(choosePercentage)

        // store chosen percentage for next time
        paymentPreconditions.assertStoredDefaultTipChoice(initialDefaultPercentage)
        page.primaryActionButton.click()
        paymentPreconditions.assertStoredDefaultTipChoice(nextDefaultPercentage)

        // preselect stored default percentage
        restartScenario()
        page.isSelectedPercentage(nextDefaultPercentage)
    }
}
