package de.culture4life.luca.testtools

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.ext.junit.runners.AndroidJUnit4
import de.culture4life.luca.R
import de.culture4life.luca.authentication.AuthenticationUiExtension
import de.culture4life.luca.testtools.rules.LucaFragmentScenarioRule
import de.culture4life.luca.ui.consent.ConsentUiExtension
import org.junit.Assert
import org.junit.Rule
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
abstract class LucaFragmentTest<FRAGMENT : Fragment>(
    @get:Rule
    val fragmentScenarioRule: LucaFragmentScenarioRule<FRAGMENT>
) : LucaScenarioTest() {

    lateinit var testNavigationController: TestNavHostController

    /**
     * Mock [Navigation.findNavController] result and the following [androidx.navigation.NavController.navigate] calls.
     *
     * Has to be called after fragment instance is available and before onViewCreated.
     *
     * https://developer.android.com/guide/navigation/navigation-testing#test_navigationui_with_fragmentscenario
     */
    fun setupTestNavigationController(
        fragment: Fragment,
        @IdRes
        currentDestination: Int
    ) {
        testNavigationController = TestNavHostController(application)
        testNavigationController.navigatorProvider = TestNavigatorProvider()
        fragment.viewLifecycleOwnerLiveData.observeForever { viewLifecycleOwner ->
            if (viewLifecycleOwner != null) {
                testNavigationController.setGraph(R.navigation.mobile_navigation)
                testNavigationController.setCurrentDestination(currentDestination)
                Navigation.setViewNavController(fragment.requireView(), testNavigationController)
            }
        }
    }

    fun assertNavigationDestination(
        @IdRes
        navigationId: Int
    ) {
        Assert.assertEquals(
            application.resources.getResourceName(navigationId),
            testNavigationController.currentDestination!!.displayName
        )
    }

    /**
     * Only by default available when we use our [de.culture4life.luca.ui.BaseActivity]. But for this
     * test type we try to isolate as much as possible to keep tests simple. For that Espresso enforce
     * us to use [androidx.fragment.app.testing.FragmentScenario.EmptyFragmentActivity].
     */
    fun initializeUiExtensions() {
        fragmentScenarioRule.scenario.onFragment {
            ConsentUiExtension(it.childFragmentManager, application.consentManager, testDisposable)
            AuthenticationUiExtension(it.parentFragmentManager, application.authenticationManager, testDisposable)
        }
    }

    fun launchScenario(
        @IdRes
        navigationId: Int,
        bundleBlock: Bundle.() -> Unit = {}
    ) {
        val bundle = Bundle().apply(bundleBlock)
        fragmentScenarioRule.launch(onCreated = { setupTestNavigationController(it, navigationId) }, bundle = bundle)
    }
}
