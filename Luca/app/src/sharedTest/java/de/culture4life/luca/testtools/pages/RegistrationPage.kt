package de.culture4life.luca.testtools.pages

import de.culture4life.luca.R
import de.culture4life.luca.registration.RegistrationData
import de.culture4life.luca.testtools.actions.scrollToAndPerform
import de.culture4life.luca.testtools.pages.dialogs.SmsVerificationExplanationDialog
import de.culture4life.luca.testtools.pages.dialogs.SmsVerificationTanDialog
import de.culture4life.luca.testtools.pages.dialogs.SmsVerificationTanInfoDialog
import de.culture4life.luca.testtools.pages.dialogs.UpdateDialog
import io.github.kakaocup.kakao.edit.KEditText
import io.github.kakaocup.kakao.edit.KTextInputLayout
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView

class RegistrationPage {

    val updateDialog = UpdateDialog()

    val successTitle = KTextView { withId(R.id.onboardingTitleTextView) }
    val successSubTitle = KTextView { withId(R.id.onboardingSubTitleTextView) }

    val phoneNumberInput = KEditText { withId(R.id.phoneNumberEditText) }
    val emailInput = KEditText { withId(R.id.emailEditText) }
    val streetInput = KEditText { withId(R.id.streetEditText) }
    val houseNumberInput = KEditText { withId(R.id.houseNumberEditText) }
    val postalCodeInput = KEditText { withId(R.id.postalCodeEditText) }
    val cityNameInput = KEditText { withId(R.id.cityNameEditText) }

    val firstNameInputLayout = KTextInputLayout { withId(R.id.firstNameLayout) }
    val lastNameInputLayout = KTextInputLayout { withId(R.id.lastNameLayout) }

    val continueButton = KButton {
        withId(R.id.registrationActionButton)
        withText(R.string.action_continue)
    }

    val updateButton = KButton {
        withId(R.id.registrationActionButton)
        withText(R.string.action_update)
    }

    val finishButton = KButton {
        withId(R.id.registrationActionButton)
        withText(R.string.action_finish)
    }

    fun assertNamesEmptyError() {
        firstNameInputLayout.hasError(R.string.registration_empty_but_required_field_error)
        lastNameInputLayout.hasError(R.string.registration_empty_but_required_field_error)
    }

    fun assertSuccess() {
        successTitle.isDisplayed()
        successSubTitle.isDisplayed()
    }

    fun enterContactData(registrationData: RegistrationData) {
        phoneNumberInput.scrollToAndPerform { replaceText(registrationData.phoneNumber.orEmpty()) }
        emailInput.scrollToAndPerform { replaceText(registrationData.email.orEmpty()) }
    }

    fun enterRegistrationNames(registrationData: RegistrationData) {
        firstNameInputLayout.edit.scrollToAndPerform { replaceText(registrationData.firstName.orEmpty()) }
        lastNameInputLayout.edit.scrollToAndPerform { replaceText(registrationData.lastName.orEmpty()) }
    }

    fun enterOtherData(registrationData: RegistrationData) {
        streetInput.scrollToAndPerform { replaceText(registrationData.street.orEmpty()) }
        houseNumberInput.scrollToAndPerform { replaceText(registrationData.houseNumber.orEmpty()) }
        postalCodeInput.scrollToAndPerform { replaceText(registrationData.postalCode.orEmpty()) }
        cityNameInput.scrollToAndPerform { replaceText(registrationData.city.orEmpty()) }
    }

    fun editUserData(registrationData: RegistrationData) {
        enterContactData(registrationData)
        enterRegistrationNames(registrationData)
        enterOtherData(registrationData)
        updateButton.scrollToAndPerform { click() }
    }

    fun performSmsTanVerification(showInfoDialog: Boolean = false) {
        SmsVerificationExplanationDialog().run { okButton.click() }
        SmsVerificationTanDialog().run {
            if (showInfoDialog) {
                infoButton.scrollToAndPerform { click() }
                SmsVerificationTanInfoDialog().run {
                    isDisplayed()
                    okButton.click()
                }
            }
            tanInput.replaceText("123456")
            okButton.click()
        }
    }
}
