package de.culture4life.luca.ui.myluca

import androidx.core.os.bundleOf
import androidx.lifecycle.Lifecycle
import de.culture4life.luca.R
import de.culture4life.luca.children.Child
import de.culture4life.luca.testtools.LucaFragmentTest
import de.culture4life.luca.testtools.pages.MyLucaPage
import de.culture4life.luca.testtools.preconditions.DocumentPreconditions
import de.culture4life.luca.testtools.preconditions.MockServerPreconditions
import de.culture4life.luca.testtools.preconditions.RegistrationPreconditions
import de.culture4life.luca.testtools.rules.FixedTimeRule
import de.culture4life.luca.testtools.rules.LucaFragmentScenarioRule
import de.culture4life.luca.testtools.samples.CovidDocumentSamples
import de.culture4life.luca.testtools.samples.LocationSamples
import de.culture4life.luca.testtools.samples.TicketSampleEvents
import de.culture4life.luca.ui.BaseQrCodeViewModel
import io.github.kakaocup.kakao.recycler.KRecyclerView
import org.joda.time.DateTime
import org.junit.Before
import org.junit.Ignore
import org.junit.Rule
import org.junit.Test
import java.util.*

class MyLucaFragmentTest : LucaFragmentTest<MyLucaFragment>(LucaFragmentScenarioRule.create()) {

    @get:Rule
    val fixedTimeRule = FixedTimeRule()

    private val documentPreconditions = DocumentPreconditions()
    private val registrationPreconditions = RegistrationPreconditions()

    @Before
    fun setup() {
        setupDefaultWebServerMockResponses()

        // Transitive used manager needs to be initialized before start.
        // A bug or just not possible, because usually initialization is already done before reaching MyLucaFragment?
        getInitializedManager(application.consentManager)
        // NotificationManager is used to give feedback as vibration.
        // Vibration is not added to AddCertificateFlowPage scan process yet but will be.
        getInitializedManager(application.notificationManager)
    }

    @Test
    fun scanEudccFullyVaccinated() {
        val document = CovidDocumentSamples.ErikaMustermann.EudccFullyVaccinated()
        registrationPreconditions.givenRegisteredUser(document.person)
        launchFragment()

        MyLucaPage().run {
            documentList.assertShowsOnlyDefaultEntries()
            stepsScanValidDocument(document)
            documentList.run {
                hasSize(2)
                childAt<MyLucaPage.DocumentItem>(0) {
                    title.hasText(application.getString(R.string.certificate_type_vaccination, "(2/2)"))
                    assertIsMarkedAsValid()
                }
            }
        }

        assertDocumentRedeemRequest()

        givenFarFuture(document.vaccinationDate)
        MyLucaPage().documentList.childAt<MyLucaPage.DocumentItem>(0) {
            title.hasText(application.getString(R.string.certificate_type_vaccination, "(2/2)"))
            assertIsMarkedAsExpired()
        }
    }

    @Test
    fun scanEudccPartiallyVaccinated() {
        val document = CovidDocumentSamples.ErikaMustermann.EudccPartiallyVaccinated()
        registrationPreconditions.givenRegisteredUser(document.person)
        launchFragment()

        MyLucaPage().run {
            documentList.assertShowsOnlyDefaultEntries()
            stepsScanValidDocument(document)
            documentList.run {
                hasSize(2)
                childAt<MyLucaPage.DocumentItem>(0) {
                    title.hasText(application.getString(R.string.certificate_type_vaccination, "(1/2)"))
                    assertIsMarkedAsPartially()
                }
            }
        }

        assertDocumentRedeemRequest()

        givenFarFuture(document.vaccinationDate)
        MyLucaPage().documentList.childAt<MyLucaPage.DocumentItem>(0) {
            title.hasText(application.getString(R.string.certificate_type_vaccination, "(1/2)"))
            assertIsMarkedAsExpired()
        }
    }

    @Test
    fun scanEudccRecovered() {
        val document = CovidDocumentSamples.ErikaMustermann.EudccRecovered()
        registrationPreconditions.givenRegisteredUser(document.person)
        launchFragment()

        MyLucaPage().run {
            documentList.assertShowsOnlyDefaultEntries()
            stepsScanValidDocument(document)
            documentList.run {
                hasSize(2)
                childAt<MyLucaPage.DocumentItem>(0) {
                    title.hasText(R.string.certificate_type_recovery)
                    assertIsMarkedAsValid()
                }
            }
        }

        assertDocumentRedeemRequest()

        givenFarFuture(document.startDate)
        MyLucaPage().documentList.childAt<MyLucaPage.DocumentItem>(0) {
            title.hasText(R.string.certificate_type_recovery)
            assertIsMarkedAsExpired()
        }
    }

    @Test
    fun scanEudccTestFastNegative() {
        val document = CovidDocumentSamples.ErikaMustermann.EudccFastNegative()
        registrationPreconditions.givenRegisteredUser(document.person)
        launchFragment()

        MyLucaPage().run {
            documentList.assertShowsOnlyDefaultEntries()
            stepsScanValidDocument(document)
            documentList.run {
                hasSize(2)
                childAt<MyLucaPage.DocumentItem>(0) {
                    title.containsText(application.getString(R.string.certificate_type_test_fast) + ": " + application.getString(R.string.certificate_test_outcome_negative))
                    assertIsMarkedAsNegative()
                }
            }
        }

        assertDocumentRedeemRequest()

        givenFarFuture(document.testingDateTime)
        MyLucaPage().documentList.assertShowsOnlyDefaultEntries()
    }

    @Test
    fun scanEudccTestPcrNegative() {
        val document = CovidDocumentSamples.ErikaMustermann.EudccPcrNegative()
        registrationPreconditions.givenRegisteredUser(document.person)
        launchFragment()

        MyLucaPage().run {
            documentList.assertShowsOnlyDefaultEntries()
            stepsScanValidDocument(document)
            documentList.run {
                hasSize(2)
                childAt<MyLucaPage.DocumentItem>(0) {
                    title.containsText(application.getString(R.string.certificate_type_test_pcr) + ": " + application.getString(R.string.certificate_test_outcome_negative))
                    assertIsMarkedAsNegative()
                }
            }
        }

        assertDocumentRedeemRequest()

        givenFarFuture(document.testingDateTime)
        MyLucaPage().documentList.assertShowsOnlyDefaultEntries()
    }

    @Test
    fun scanEudccTestPcrPositive() {
        val document = CovidDocumentSamples.ErikaMustermann.EudccPcrPositive()
        registrationPreconditions.givenRegisteredUser(document.person)
        launchFragment()

        MyLucaPage().run {
            documentList.assertShowsOnlyDefaultEntries()
            stepsScanValidDocument(document)
            documentList.run {
                hasSize(2)
                childAt<MyLucaPage.DocumentItem>(0) {
                    title.containsText(application.getString(R.string.certificate_type_test_pcr) + ": " + application.getString(R.string.certificate_test_outcome_positive))
                    assertIsMarkedAsPositive()
                }
            }
        }

        assertDocumentRedeemRequest()

        givenFarFuture(document.testingDateTime)
        MyLucaPage().documentList.childAt<MyLucaPage.DocumentItem>(0) {
            title.containsText(application.getString(R.string.certificate_type_test_pcr) + ": " + application.getString(R.string.certificate_test_outcome_positive))
            assertIsMarkedAsExpired()
        }
    }

    @Test
    fun scanCheckIn() {
        registrationPreconditions.givenRegisteredUser()
        launchFragment()

        MyLucaPage().run {
            navigateToScanner()
            addCertificateFlowPage.qrCodeScannerPage.run {
                scanner.scan(LocationSamples.Restaurant().qrCodeContent)
                checkInNotSupportedDialog.okButton.click()
                isDisplayed() // Should just stay on qr code scanner now.
            }
        }
    }

    @Test
    fun scanMeeting() {
        registrationPreconditions.givenRegisteredUser()
        launchFragment()

        MyLucaPage().run {
            navigateToScanner()
            addCertificateFlowPage.qrCodeScannerPage.run {
                scanner.scan(LocationSamples.Meeting().qrCodeContent)
                checkInNotSupportedDialog.okButton.click()
                isDisplayed() // Should just stay on qr code scanner now.
            }
        }
    }

    @Test
    fun redirectWithScannedDocument() {
        val document = CovidDocumentSamples.ErikaMustermann.EudccFullyVaccinated()
        registrationPreconditions.givenRegisteredUser(document.person)

        launchFragmentSimulatedRedirect(document)

        MyLucaPage().run {
            consentsDialog.acceptButton.click()
            documentList.run {
                hasSize(2)
                childAt<MyLucaPage.DocumentItem>(0) {
                    title.hasText(application.getString(R.string.certificate_type_vaccination, "(2/2)"))
                    assertIsMarkedAsValid()
                }
            }
        }

        assertDocumentRedeemRequest()

        givenFarFuture(document.vaccinationDate)
        MyLucaPage().documentList.childAt<MyLucaPage.DocumentItem>(0) {
            title.hasText(application.getString(R.string.certificate_type_vaccination, "(2/2)"))
            assertIsMarkedAsExpired()
        }
    }

    private fun KRecyclerView.assertShowsOnlyDefaultEntries() {
        hasSize(2)
        childAt<MyLucaPage.NoDocumentsItem>(0) {
            // TODO no documents info item
        }
        childAt<MyLucaPage.CreateIdentityItem>(1) {
            // TODO create luca id info item
        }
    }

    @Test
    fun scanFullyVaccinatedPersonWithFullyVaccinatedChild() {
        val parentDocument = CovidDocumentSamples.ErikaMustermann.EudccFullyVaccinated()
        val childDocument = CovidDocumentSamples.JulianMusterkind.EudccFullyVaccinated()
        registrationPreconditions.givenRegisteredUser(parentDocument.person)
        givenAddedChild(childDocument.person)

        launchFragment()

        MyLucaPage().run {
            documentList.assertShowsOnlyDefaultEntries()
            stepsScanValidDocument(parentDocument)
            stepsScanValidDocument(childDocument)
            documentList.run {
                hasSize(4)
                childAt<MyLucaPage.DocumentItem>(0) {
                    title.hasText(application.getString(R.string.certificate_type_vaccination, "(2/2)"))
                    assertIsMarkedAsValid()
                }
                // pos 1 is id card
                // pos 2 is child title
                childAt<MyLucaPage.DocumentItem>(3) {
                    title.hasText(application.getString(R.string.certificate_type_vaccination, "(2/2)"))
                    assertIsMarkedAsValid()
                }
            }
        }

        givenFarFuture(parentDocument.vaccinationDate)
        MyLucaPage().documentList.run {
            childAt<MyLucaPage.DocumentItem>(0) {
                title.hasText(application.getString(R.string.certificate_type_vaccination, "(2/2)"))
                assertIsMarkedAsExpired()
            }
            childAt<MyLucaPage.DocumentItem>(3) {
                title.hasText(application.getString(R.string.certificate_type_vaccination, "(2/2)"))
                assertIsMarkedAsExpired()
            }
        }
    }

    @Test
    fun scanMultipleVaccinations() {
        val documentFirst = CovidDocumentSamples.ErikaMustermann.EudccPartiallyVaccinated()
        val documentSecond = CovidDocumentSamples.ErikaMustermann.EudccFullyVaccinated()
        val documentThird = CovidDocumentSamples.ErikaMustermann.EudccBoosteredVaccinated()
        registrationPreconditions.givenRegisteredUser(documentFirst.person)
        launchFragment()

        MyLucaPage().run {
            documentList.assertShowsOnlyDefaultEntries()
            stepsScanValidDocument(documentFirst)
            stepsScanValidDocument(documentSecond)
            stepsScanValidDocument(documentThird)
            documentList.run {
                hasSize(2)
                // TODO check it shows the most recent scanned document
                // TODO check item contains all documents
            }
        }

        // Once for every document
        assertDocumentRedeemRequest()
        assertDocumentRedeemRequest()
        assertDocumentRedeemRequest()
    }

    @Test
    fun redirectBirthdayNotMatch() {
        val documentFirst = CovidDocumentSamples.ErikaMustermann.EudccPartiallyVaccinated()
        val documentSecond = CovidDocumentSamples.ErikaMustermannDifferentBirthday.EudccFullyVaccinated()
        registrationPreconditions.givenRegisteredUser(documentFirst.person)
        documentPreconditions.givenAddedDocument(documentFirst)

        launchFragmentSimulatedRedirect(documentSecond)

        MyLucaPage().run {
            consentsDialog.acceptButton.click()
            birthdayNotMatchDialog.okButton.click()
            documentList.hasSize(2)
        }
    }

    @Test
    fun scanBirthdayNotMatch() {
        val documentFirst = CovidDocumentSamples.ErikaMustermann.EudccPartiallyVaccinated()
        val documentSecond = CovidDocumentSamples.ErikaMustermannDifferentBirthday.EudccFullyVaccinated()
        registrationPreconditions.givenRegisteredUser(documentFirst.person)
        launchFragment()

        MyLucaPage().run {
            documentList.assertShowsOnlyDefaultEntries()
            stepsScanValidDocument(documentFirst)
            documentList.hasSize(2)
            navigateToScanner()
            addCertificateFlowPage.qrCodeScannerPage.run {
                scanner.scan(documentSecond.qrCodeContent)
                consentsDialog.acceptButton.click()
                birthdayNotMatchDialog.okButton.click()
                cancelButton.click()
            }
            documentList.hasSize(2)
        }
    }

    @Test
    fun cardStaysExpandedOnPauseAndResume() {
        val document = CovidDocumentSamples.ErikaMustermann.EudccFullyVaccinated()
        registrationPreconditions.givenRegisteredUser(document.person)
        documentPreconditions.givenAddedDocument(document)
        launchFragment()

        MyLucaPage().documentList.childAt<MyLucaPage.DocumentItem>(0) {
            assertIsExpectedItemType()

            assertIsCollapsed()
            click()
            assertIsExpanded()

            fragmentScenarioRule.scenario.moveToState(Lifecycle.State.STARTED)
            fragmentScenarioRule.scenario.moveToState(Lifecycle.State.RESUMED)

            assertIsExpanded()
        }
    }

    @Test
    fun scanEventTicket() {
        val document = TicketSampleEvents.Exhibition()
        registrationPreconditions.givenRegisteredUser(CovidDocumentSamples.ErikaMustermann())

        launchFragment()
        MyLucaPage().run {
            documentList.assertShowsOnlyDefaultEntries()
            stepsScanValidDocument(document)
            documentList.run { hasSize(2) }
            documentList.childAt<MyLucaPage.EventItem>(0) {
                assertIsExpectedItemType()
                icon.hasDrawable(R.drawable.ic_eye)
                title.hasText(document.eventName)
                assertIsCollapsed()
                click()
                assertIsExpanded()
            }
        }

        // Once for every document
        assertDocumentRedeemRequest()
    }

    @Test
    @Ignore("Not useful to run in pipeline")
    fun addOneThousandTickets() {
        val tickets = (0..1000).map {
            val id = UUID.randomUUID().toString()
            TicketSampleEvents.Exhibition(id = id)
        }
        registrationPreconditions.givenRegisteredUser(CovidDocumentSamples.ErikaMustermann())
        tickets.forEach { documentPreconditions.givenAddedDocument(it) }

        launchFragment()
        MyLucaPage().run {
            documentList.run {
                hasSize(1002)
                scrollTo(1000)
            }
            documentList.childAt<MyLucaPage.EventItem>(0) {
                assertIsExpectedItemType()
                assertIsCollapsed()
                click()
                assertIsExpanded()
            }
        }
    }

    @Test
    fun deleteAndUnredeemDocument() {
        val document = CovidDocumentSamples.ErikaMustermann.EudccFullyVaccinated()
        registrationPreconditions.givenRegisteredUser(document.person)
        documentPreconditions.givenAddedDocument(document)
        launchFragment()

        MyLucaPage().run {
            documentList.childAt<MyLucaPage.DocumentItem>(0) {
                assertIsExpectedItemType()
                assertIsCollapsed()
                click()
                assertIsExpanded()
                documentList.scrollToEnd()
                deleteButton.click()
            }
            deleteDocumentDialog.okButton.click()
            documentList.childAt<MyLucaPage.NoDocumentsItem>(0) {
                assertIsExpectedItemType()
            }
        }

        assertDocumentUnRedeemRequest()
    }

    private fun launchFragment() {
        fragmentScenarioRule.launch()
        initializeUiExtensions()
    }

    private fun launchFragmentSimulatedRedirect(document: CovidDocumentSamples) {
        fragmentScenarioRule.launchSimulateRedirect(
            bundleOf(BaseQrCodeViewModel.BARCODE_DATA_KEY to document.qrCodeContent)
        ) {
            initializeUiExtensions()
        }
    }

    private fun assertDocumentUnRedeemRequest() {
        mockServerPreconditions.assert(MockServerPreconditions.Route.UnRedeemDocument)
    }

    private fun assertDocumentRedeemRequest() {
        mockServerPreconditions.assert(MockServerPreconditions.Route.RedeemDocument)
    }

    private fun setupDefaultWebServerMockResponses() {
        with(mockServerPreconditions) {
            givenTimeSync()
            givenRedeemDocument()
            givenUnRedeemDocument()
        }
    }

    private fun givenAddedChild(person: CovidDocumentSamples.Person) = givenAddedChild(person.firstName, person.lastName)
    private fun givenAddedChild(firstName: String, lastName: String) {
        val childrenManager = application.childrenManager
        childrenManager.addChild(Child(firstName, lastName)).blockingAwait()
    }

    private fun givenFarFuture(start: DateTime) {
        fixedTimeRule.setCurrentDateTime(start.plusYears(100))
        // Ensure we don't test entries which becomes deleted and triggers list update.
        application.documentManager.deleteExpiredDocuments().blockingAwait()
        fragmentScenarioRule.scenario.recreate()
    }
}
