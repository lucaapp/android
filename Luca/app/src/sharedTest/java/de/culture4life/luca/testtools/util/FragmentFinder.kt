package de.culture4life.luca.testtools.util

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.test.internal.runner.junit4.statement.UiThreadStatement
import androidx.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import androidx.test.runner.lifecycle.Stage
import de.culture4life.luca.LucaApplication

object FragmentFinder {

    fun <T : Fragment> find(expected: Class<T>): T {
        return findTransitive(getCurrentActivity(), expected)
    }

    private fun getCurrentActivity(): FragmentActivity {
        fun getCurrentActivity(): FragmentActivity {
            return ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(Stage.RESUMED).elementAt(0) as FragmentActivity
        }

        return if (LucaApplication.isRunningInstrumentationTests()) {
            lateinit var activity: FragmentActivity
            UiThreadStatement.runOnUiThread {
                activity = getCurrentActivity()
            }
            activity
        } else {
            getCurrentActivity()
        }
    }

    private fun <T> findTransitive(activity: FragmentActivity, expected: Class<T>): T {
        return findTransitive(activity.supportFragmentManager.fragments, expected)!!
    }

    private fun <T> findTransitive(fragments: List<Fragment>, expected: Class<T>): T? {
        fragments.forEach { fragment ->
            if (fragment.javaClass == expected) {
                @Suppress("UNCHECKED_CAST")
                return fragment as T
            } else {
                val result = findTransitive(fragment.childFragmentManager.fragments, expected)
                if (result != null) return result
            }
        }

        return null
    }
}
