package de.culture4life.luca.testtools.samples

import de.culture4life.luca.payment.PaymentData
import org.joda.time.DateTime
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

object UrlSamples {

    const val webApp = "https://app.luca-app.de/webapp/"
    const val payment = "https://app.luca-app.de/pay/"

    const val covidTestResult = "${webApp}testresult/"
    const val meeting = "${webApp}meeting/"
    const val appointment = "${webApp}appointment/"
    const val tickets = "${webApp}tickets/"

    fun location(scannerId: String) = "$webApp$scannerId"

    fun meeting(scannerId: String) = "$meeting$scannerId"

    fun covidTestResult(qrCodeContent: String): String {
        val encodedQrCode = URLEncoder.encode(qrCodeContent, StandardCharsets.UTF_8.name())
        return "$covidTestResult#$encodedQrCode"
    }

    fun appointment(testingDateTime: DateTime, type: String, lab: String, address: String, qrCodeContent: String): String {
        val encodedType = URLEncoder.encode(type, StandardCharsets.UTF_8.name())
        val encodedLab = URLEncoder.encode(lab, StandardCharsets.UTF_8.name())
        val encodedAddress = URLEncoder.encode(address, StandardCharsets.UTF_8.name())
        val encodedQrCode = URLEncoder.encode(qrCodeContent, StandardCharsets.UTF_8.name())
        return "$appointment?timestamp=${testingDateTime.millis}&type=$encodedType&lab=$encodedLab&address=$encodedAddress&qrCode=$encodedQrCode"
    }

    fun tickets(jwt: String) = "$tickets#$jwt"

    fun checkoutResult(checkoutId: String, checkoutState: PaymentData.Status = PaymentData.Status.CLOSED): String {
        return when (checkoutState) {
            PaymentData.Status.CLOSED -> "${payment}complete_checkout?checkoutId=$checkoutId"
            PaymentData.Status.UNKNOWN -> "${payment}cancel_checkout?checkoutId=$checkoutId"
            PaymentData.Status.ERROR -> "${payment}error_payment?checkoutId=$checkoutId"
        }
    }
}
