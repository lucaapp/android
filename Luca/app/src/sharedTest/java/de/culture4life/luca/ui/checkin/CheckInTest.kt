package de.culture4life.luca.ui.checkin

import de.culture4life.luca.testtools.LucaActivityTest
import de.culture4life.luca.testtools.pages.CheckInPage
import de.culture4life.luca.testtools.pages.VenueDetailsPage
import de.culture4life.luca.testtools.preconditions.*
import de.culture4life.luca.testtools.rules.FixedTimeRule
import de.culture4life.luca.testtools.rules.LucaActivityScenarioRule
import de.culture4life.luca.testtools.samples.LocationSamples
import de.culture4life.luca.ui.MainActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class CheckInTest : LucaActivityTest<MainActivity>(LucaActivityScenarioRule.create()) {

    @get:Rule
    val fixedTimeRule = FixedTimeRule()

    @Before
    fun setup() {
        RegistrationPreconditions().givenRegisteredUser()
        DailyKeyPreconditions().givenStoredDailyKey()

        TermsPreconditions().givenTermsAccepted()
        OnboardingPreconditions().givenOnboardingSeen()
        WhatIsNewPreconditions().givenAllNewsSeen()

        mockServerPreconditions.givenTimeSync()
        mockServerPreconditions.givenSupportedVersion()
    }

    @Test
    fun checkInMultipleTimes_tracingDisabled() {
        ContactTracingPreconditions().givenContactTracingEnabled(false)

        val location = LocationSamples.Restaurant()
        mockServerPreconditions.givenLocation(location)
        launchScenario()

        CheckInPage().run {
            isDisplayed()
            scanner.scan(location.qrCodeContent)
        }

        VenueDetailsPage().run {
            waitForIdle()
            title.isDisplayed()
            checkOut.click()
        }

        CheckInPage().run {
            isDisplayed()
            scanner.scan(location.qrCodeContent)
        }

        VenueDetailsPage().run {
            title.isDisplayed()
            checkOut.click()
        }

        CheckInPage().run {
            isDisplayed()
        }
    }

    private fun launchScenario() {
        activityScenarioRule.launch()
    }
}
