package de.culture4life.luca

import androidx.test.espresso.ViewAction
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import de.culture4life.luca.testtools.FixRobolectricIdlingResource
import de.culture4life.luca.testtools.RobolectricTypeTextWarning
import io.github.kakaocup.kakao.Kakao
import io.github.kakaocup.kakao.common.actions.NestedScrollToAction

object KakaoConfiguration {
    fun apply() {
        Kakao.intercept {
            onDataInteraction { onAll { FixRobolectricIdlingResource.waitForIdle() } }
            onViewInteraction {
                onAll { FixRobolectricIdlingResource.waitForIdle() }
                onPerform { viewInteraction, viewAction -> onPerformDefaultInterceptors(viewInteraction, viewAction) }
            }
            onWebInteraction { onAll { FixRobolectricIdlingResource.waitForIdle() } }
        }
    }

    fun onPerformDefaultInterceptors(viewInteraction: ViewInteraction, viewAction: ViewAction) {
        FixRobolectricIdlingResource.waitForIdle()
        RobolectricTypeTextWarning.apply(viewAction)

        // ensure we perform only on full displayed views (otherwise the user can't reach them)
        requireViewIsAccessibleForUser(viewAction, viewInteraction)
    }

    private fun requireViewIsAccessibleForUser(viewAction: ViewAction, viewInteraction: ViewInteraction) {
        // skip check for actions to reach the view
        if (viewAction is NestedScrollToAction) {
            return
        }

        viewInteraction.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }
}
