package de.culture4life.luca.poeditor

import de.culture4life.luca.poeditor.network.PoEditorApiFactory
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import java.io.File

/**
 * Download translations how POEditor provides them for [type = "android_strings"]
 */
open class PoEditorDownloadTask : DefaultTask() {

    open class Setting(
        val apiToken: String,
        val projectId: Int,
        val language: String,
        val quotedStrings: Boolean = false,

        val targetResourceFolder: String,
        val targetResourceFile: String
    ) {
        val targetResource = "src/main/res/${targetResourceFolder}/${targetResourceFile}"
    }

    @Input
    var settings: List<Setting> = emptyList()

    private val poEditorApi by lazy {
        PoEditorApiFactory.create { logger.lifecycle(it) }
    }

    @TaskAction
    fun importTranslations() {
        logger.lifecycle("importing ${settings.size} translation(s)")

        settings.forEach { setting ->
            val translation = downloadTranslation(setting)
            writeFile(setting.targetResource, translation)
        }
    }

    private fun downloadTranslation(setting: Setting): String {
        val translationFileUrl = getTranslationFileDownloadUrl(setting)
        return downloadContent(translationFileUrl)
    }

    private fun getTranslationFileDownloadUrl(setting: Setting): String {
        val translationExportCall = poEditorApi.getExportFileInfo(
            apiToken = setting.apiToken,
            id = setting.projectId,
            language = setting.language,
            type = "android_strings",
            filters = null,
            order = "terms",
            tags = null,
            options = "[{\"unquoted\":${if (setting.quotedStrings) 0 else 1}}]"
        )

        val body = translationExportCall.execute().body()!!
        require(body.response.status != "fail") { "Unable to download translation file: ${body.response.message}" }
        return body.result.url
    }

    private fun writeFile(target: String, content: String) {
        val targetFile = File(project.projectDir, target)
        targetFile.parentFile.mkdirs()
        targetFile.writeText(content)
    }

    private fun downloadContent(fileUrl: String): String {
        return poEditorApi.downloadFile(fileUrl).execute().body()!!
    }
}
