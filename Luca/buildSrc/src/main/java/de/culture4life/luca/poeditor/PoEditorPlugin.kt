package de.culture4life.luca.poeditor

import org.gradle.api.Plugin
import org.gradle.api.Project

class PoEditorPlugin : Plugin<Project> {

    private val pluginTaskGroup = "localization"
    private val pluginExtension = "poEditor"

    override fun apply(target: Project) {
        val userConfiguration = target.extensions.create(pluginExtension, PoEditorPluginExtension::class.java)

        val downloadTask = target.tasks.register("poEditorDownload", PoEditorDownloadTask::class.java) {
            settings = userConfiguration.translations
        }

        val unifyTask = target.tasks.register("poEditorUnify", PoEditorUnifyTask::class.java) {
            files = userConfiguration.unifies
        }

        target.tasks.register("poEditorImport") {
            group = pluginTaskGroup
            dependsOn(downloadTask, unifyTask)
        }
    }
}