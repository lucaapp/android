package de.culture4life.luca.poeditor

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import java.io.File

/**
 * Modify the POEditor exports to the state how they have been imported into POEditor.
 */
open class PoEditorUnifyTask : DefaultTask() {

    @Input
    var files = emptyList<String>()

    @TaskAction
    fun unifyFiles() {
        logger.lifecycle("unify ${files.size} translation(s)")

        files.forEach { path ->
            val content = readFile(path)
            val contentUnified = unify(content)
            writeFile(path, contentUnified)
        }
    }

    private fun unify(content: String) = content
        // decode (special) dashes (poeditor had translate – to &#x2013; during import to POEditor)
        .replace("&#x2013;", "–")
        // remove escape from quotes for html tags (poeditor translate " as \")
        .replace("=\\\"", "=\"")
        .replace("\\\">", "\">")
        // remove escape from @ (sometimes added by poeditor?)
        .replace("\\@", "@")
        // remove escape from ? (sometimes added by poeditor?)
        .replace("\\?", "?")
        // translate NBSP (poeditor translate &#160; as NBSP)
        .replace(" ", "&#160;")
        // replace Markdown links with HTML anchor tag
        .replace(Regex("\\[([^\\]]+)\\]\\(([^\\)]+)\\)"), "<a href=\"$2\">$1</a>")

    private fun readFile(target: String): String {
        val targetFile = File(project.projectDir, target)
        return targetFile.readText()
    }

    private fun writeFile(target: String, content: String) {
        val targetFile = File(project.projectDir, target)
        targetFile.parentFile.mkdirs()
        targetFile.writeText(content)
    }
}
