package de.culture4life.luca.poeditor

open class PoEditorPluginExtension {

    companion object {
        const val STRINGS_QUOTED = true
        const val STRINGS_NOT_QUOTED = false

        const val STRINGS_UNIFIED = true
        const val STRINGS_NOT_UNIFIED = false
    }

    lateinit var apiToken: String

    val translations = mutableListOf<PoEditorDownloadTask.Setting>()
    val unifies = mutableListOf<String>()

    fun add(projectId: Int, language: String, folder: String, file: String, quotedStrings: Boolean, unify: Boolean) {
        val setting = PoEditorDownloadTask.Setting(
            apiToken = apiToken,
            projectId = projectId,
            language = language,
            quotedStrings = quotedStrings,
            targetResourceFolder = folder,
            targetResourceFile = file
        )

        translations.add(setting)

        if (unify) {
            unifies.add(setting.targetResource)
        }
    }
}