package de.culture4life.luca.poeditor.network

import retrofit2.Call
import retrofit2.http.*

interface PoEditorApi {
    @FormUrlEncoded
    @POST("projects/export")
    fun getExportFileInfo(
        @Field("api_token") apiToken: String,
        @Field("id") id: Int,
        @Field("language") language: String,
        @Field("type") type: String,
        @Field("filters") filters: List<String>? = null,
        @Field("order") order: String? = null,
        @Field("tags") tags: List<String>? = null,
        @Field("options") options: String? = null
    ): Call<ExportResponse>

    @GET
    fun downloadFile(@Url url: String): Call<String>
}