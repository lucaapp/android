package de.culture4life.luca.poeditor.network

data class ExportResponse(
    val response: ResponseStatus,
    val result: ExportResult
) {
    data class ResponseStatus(
        val status: String,
        val code: String,
        val message: String
    )

    data class ExportResult(val url: String)
}


