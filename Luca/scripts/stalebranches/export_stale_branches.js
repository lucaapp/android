const axios = require('axios')
const fs = require('fs')
const os = require('os')
const outputFile = "./stale_branches.txt"
const gitlabAccessToken = process.env.LUCA_GITLAB_ACCESS_TOKEN
const gitlabProjectId = "35039811"
const monthMilliseconds = 30 * 24 * 60 * 60 * 1000
const lastCommitThreshold = 2 * monthMilliseconds
deleteFileIfExists()
const writer = fs.createWriteStream(outputFile, { flags: 'a' })

function deleteFileIfExists() {
    try {
        fs.unlinkSync(outputFile)
    } catch (error) { }
}

async function accumulateBranches(page, branches) {
    const newBranches = await getBranchesForPage(page)
    if (newBranches.length < 100) return branches.concat(newBranches)
    return accumulateBranches(page + 1, branches.concat(newBranches))
}

async function getBranchesForPage(page) {
    console.log(`Getting branches for page ${page}`)
    const branchesResponse = await axios.get(`https://gitlab.com/api/v4/projects/${gitlabProjectId}/repository/branches?page=${page}&per_page=100&private_token=${gitlabAccessToken}`)
    return branchesResponse.data
}

function isBranchStale(branch) {
    const containsForbiddenName = new RegExp("^(dev|master|release\/.*)$").test(branch["name"])
    const isProtectedBranch = branch["protected"]
    const isMerged = branch["merged"]
    const lastCommit = Date.parse(branch["commit"]["committed_date"])
    const isOlderThanTwoMonths = lastCommit < (Date.now() - lastCommitThreshold)
    return !containsForbiddenName && !isProtectedBranch && isMerged && isOlderThanTwoMonths
}

async function writeStaleBranchToFile(branchName, last) {
    writer.write(branchName)
    if (!last) writer.write(os.EOL)
}

function getStaleBranchNamesFromBranches(branches) {
    return [...new Set(
        branches.filter(branch => isBranchStale(branch))
            .sort((a, b) => new Date(b["commit"]["committed_date"]) - new Date(a["commit"]["committed_date"]))
            .map(branch => `Last commit: ${branch["commit"]["committed_date"]} Name: ${branch["name"]}`)
    )]
}

async function main() {
    const branches = await accumulateBranches(0, [])
    console.log(`Got ${branches.length} branches`)
    const staleBranchNames = getStaleBranchNamesFromBranches(branches)
    staleBranchNames.forEach((branch, index) => writeStaleBranchToFile(branch, index == staleBranchNames.length - 1))
    return staleBranchNames
}

(() => {
    console.log("Starting to export stale branches...")
    main()
        .then(staleBranchNames => console.log(`Finished exporting ${staleBranchNames.length} stale branches!`))
        .catch(error => console.log(error))
})()